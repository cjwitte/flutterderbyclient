import 'dart:collection';

import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/FinishResult.dart';
import 'package:flutter0322/autoTimer/HistoryBuffer.dart';
import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:flutter0322/autoTimer/RawDataBuffer.dart';
import 'package:flutter0322/autoTimer/SimplePinSummary.dart';
import 'package:flutter0322/autoTimer/models/Constants.dart';
import 'package:flutter0322/autoTimer/utils/CompactMap.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/RaceConfig.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/shared/TImerConfig.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:quiver/strings.dart';
import 'dart:convert';

import 'dart:async';
import 'dart:io';
import 'dart:io' show File, Platform;
import 'package:mqtt_client/mqtt_client.dart';
import 'package:cron/cron.dart';
import 'package:intl/intl.dart';

import 'package:flutter0322/autoTimer/models/data_model_plain.dart';

/// An annotated simple subscribe/publish usage example for mqtt_client. Please read in with reference
/// to the MQTT specification. The example is runnable, also refer to test/mqtt_client_broker_test...dart
/// files for separate subscribe/publish tests.
   const String nextPhaseTopic="nextPhase";

RacePhaseCOMPAT globalNextOnBlocks=null;

Future<int> main() async {
  final String mqttHost = getMqttHost();

  print("Using mqttHost: $mqttHost");
  /// First create a client, the client is constructed with a broker name, client identifier
  /// and port if needed. The client identifier (short ClientId) is an identifier of each MQTT
  /// client connecting to a MQTT broker. As the word identifier already suggests, it should be unique per broker.
  /// The broker uses it for identifying the client and the current state of the client. If you don’t need a state
  /// to be hold by the broker, in MQTT 3.1.1 you can set an empty ClientId, which results in a connection without any state.
  /// A condition is that clean session connect flag is true, otherwise the connection will be rejected.
  /// The client identifier can be a maximum length of 23 characters. If a port is not specified the standard port
  /// of 1883 is used.
  /// If you want to use websockets rather than TCP see below.
  ///
  final MqttClient client = new MqttClient(mqttHost, "");
  print("server begin main.");

  await loadRaceConfig();
  print("server continue main.");




  runSimulatedRace(client);



  var psCallback = new PropagateViaMqtt(client);
  final rawDataBuffer = new RawDataBuffer(psCallback);

  /// A websocket URL must start with ws:// or wss:// or Dart will throw an exception, consult your websocket MQTT broker
  /// for details.
  /// To use websockets add the following lines -:
  /// client.useWebSocket = true;
  /// client.port = 80;  ( or whatever your WS port is)
  /// Note do not set the secure flag if you are using wss, the secure flags is for TCP sockets only.

  /// Set logging on if needed, defaults to off
  client.logging(on: false);

  /// If you intend to use a keep alive value in your connect message that is not the default(60s)
  /// you must set it here
  client.keepAlivePeriod = 20;

  /// Add the unsolicited disconnection callback
  client.onDisconnected = onDisconnected;

  /// Add a subscribed callback, there is also an unsubscribed callback if you need it.
  /// You can add these before connection or change them dynamically after connection if
  /// you wish.
  client.onSubscribed = onSubscribed;

  /// Create a connection message to use or use the default one. The default one sets the
  /// client identifier, any supplied username/password, the default keepalive interval(60s)
  /// and clean session, an example of a specific one below.
  final MqttConnectMessage connMess = new MqttConnectMessage()
      .withClientIdentifier("Mqtt_MyClientUniqueId")
      .keepAliveFor(20) // Must agree with the keep alive set above or not set
      .withWillTopic("willtopic") // If you set this you must set a will message
      .withWillMessage("My Will message")
      .startClean() // Non persistent session for testing
      .withWillQos(MqttQos.atLeastOnce);

  augmentCredentials(connMess);
  print("mqttTimer::Mosquitto client connecting....");
  client.connectionMessage = connMess;

  /// Connect the client, any errors here are communicated by raising of the appropriate exception. Note
  /// in some circumstances the broker will just disconnect us, see the spec about this, we however eill
  /// never send malformed messages.
  try {
    await client.connect();
  } catch (e) {
    print("EXAMPLE::client exception - $e");
    client.disconnect();
  }

  /// Check we are connected
  if (client.connectionStatus.state == MqttConnectionState.connected) {
    print("EXAMPLE::Mosquitto client connected");
  } else {
    print(
        "EXAMPLE::ERROR Mosquitto client connection failed - disconnecting, state is ${client.connectionStatus.state}");
    client.disconnect();
    exit(-1);
  }

  /// Ok, lets try a subscription
  final String topic = "derby/+/rpi/#";  // derby/{TIMER_HOSTNAME/rpi/{lanePin}
  client.subscribe(topic, MqttQos.atMostOnce);
  client.subscribe(TimerConfig.desiredTimerConfigTopic, MqttQos.atMostOnce);
  client.subscribe(nextPhaseTopic, MqttQos.atMostOnce);

  /// The client has a change notifier object(see the Observable class) which we then listen to to get
  /// notifications of published updates to each subscribed topic.
  client.updates.listen((List<MqttReceivedMessage> c) {
    c.forEach((MqttReceivedMessage msg) {
      handleReceivedMessage(msg, rawDataBuffer, client);
    });
  });

  /// Lets publish to our topic
  // Use the payload builder rather than a raw buffer
  print("EXAMPLE::Publishing our topic");

  /// Our known topic to publish to
  final String pubTopic = "Dart/Mqtt_client/testtopic";
  final MqttClientPayloadBuilder builder = new MqttClientPayloadBuilder();
  builder.addString("Hello from mqtt_client");

  /// Subscribe to it
  client.subscribe(pubTopic, MqttQos.exactlyOnce);

  /// Publish it
  //client.publishMessage(pubTopic, MqttQos.exactlyOnce, builder.payload);

  /// Ok, we will now sleep a while, in this gap you will see ping request/response
  /// messages being exchanged by the keep alive mechanism.
  print("EXAMPLE::Sleeping....");

  // publish initial message to spring to avoid empty datalog.ndjson
  Map<String,String> postPrimeMap={"carNumber":"199","firstName":new DateFormat.yMMMd().format(new DateTime.now())};
  await new PostAddBlocks().postForm(postPrimeMap, "/addRacer");

  while (true) {
    await MqttUtilities.asyncSleep(5);
    rawDataBuffer.periodicCheck();
  }

  /// Finally, unsubscribe and exit gracefully
  print("EXAMPLE::Unsubscribing");
  client.unsubscribe(topic);

  /// Wait for the unsubscribe message from the broker if you wish.
  await MqttUtilities.asyncSleep(2);
  print("EXAMPLE::Disconnecting");
  client.disconnect();
  return 0;
}

void runSimulatedRace( MqttClient client,) {
  Map<String, String> envVars = Platform.environment;
  if (isEmpty(envVars['SIMULATE_TIMER'])) {
    print ("No simulated race.");
    return;
  }
  print ("Scheduling simulated race(s).");

  var cron = new Cron();
  String csched='*/2 * * * *';
  cron.schedule(new Schedule.parse(csched), () async {

    // defer execution until time is solid into new minute (sometimes fires @ 00:59 otherwise).
    new Timer(new Duration(seconds: 10), (){
      simulatedRace( client,lane1Pin, lane2Pin);
    });
    new Timer(new Duration(seconds: 40), (){
      simulatedRace( client, lane2Pin, lane1Pin);
    });
  });
}

void simulatedRace( MqttClient client, String winPin, String losePin) {

  var now = new DateTime.now();
  var f2=new DateFormat.yMd().add_jms();
  String formatted2 = f2.format(now);
  print("$formatted2 every other minute? ");int t0=now.microsecondsSinceEpoch;
  List<Map<String, String>> raw = [];
 // String base20 = (20 * CompactMap.million).toString();
  String base20 = t0.toString();
  raw.add({"base": base20, "pin": winPin, "state": "1",  "times": "60:351500"});
  raw.add({"base": base20, "pin": losePin, "state": "1", "times": "50:350102"});

  CompactMap compactMap=new CompactMap();
  List<PinChange> pcl=compactMap.buildPinChangeListFromCompactMapList(raw);
  pcl.forEach((PinChange pc){
    publishSim(pc,client);
  });
}
void publishSim(PinChange pc, MqttClient client){

  final String pubTopic = "rpi/${pc.pinNumber}";
  print("publishSim ${pubTopic} publishing message: ${json.encode(pc.toJson())}");

  final MqttClientPayloadBuilder builder = new MqttClientPayloadBuilder();
  builder.addString(json.encode(pc.toJson()));

  /// Publish it
  client.publishMessage(pubTopic, MqttQos.exactlyOnce, builder.payload);
}

void handleReceivedMessage(MqttReceivedMessage msg, RawDataBuffer rawDataBuffer,
    MqttClient mqttClient) {
  final MqttPublishMessage recMess = msg.payload as MqttPublishMessage;
  final String pt =
      MqttPublishPayload.bytesToStringAsString(recMess.payload.message);

  /// The above may seem a little convoluted for users only interested in the
  /// payload, some users however may be interested in the received publish message,
  /// lets not constrain ourselves yet until the package has been in the wild
  /// for a while.
  /// The payload is a byte buffer, this will be specific to the topic
  print(
      "Got Mqtt message: from topic: <${msg.topic}>, payload is <-- ${pt} -->");
  if (TimerConfig.desiredTimerConfigTopic == msg.topic) {
    StaticShared.getShared().timerConfig = TimerConfig.fromJsonString(pt);
    String activeTimerConfig = StaticShared.getShared().timerConfig.toString();

    // publish back to activeTimerConfig
    print(
        "republish Mqtt message: to topic: <${msg.topic}>, payload is <-- ${activeTimerConfig} -->");

    final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addString(activeTimerConfig);

    mqttClient.publishMessage(TimerConfig.activeTimerConfigTopic,
        MqttQos.atLeastOnce, builder.payload);

    return;
  }
  if (nextPhaseTopic == msg.topic) {
    print(
        "nextPhase: cars on blocks: <${msg.topic}>, payload is <-- ${pt} -->");
    //final rpc = standardSerializers.deserializeWith(json.decode(pt), new FullType(RacePhase));

    if (pt == "null") {
      print("got string with value null: [$pt]");
      return;
    }
    if (pt != null) {
      var rpc = RacePhaseCOMPAT.fromJsonMap(json.decode(pt));
      globalNextOnBlocks=rpc;
      print("deserialized: $rpc");
      lucky777(rpc);
    }
    else{
      globalNextOnBlocks=null;
    }

    return;
  }
  rawDataBuffer.addJsonData(pt);
}

void lucky777(RacePhaseCOMPAT racePhase) async {
  if (racePhase.carNumber1 == 777) {
    await applyWinner(racePhase, 777);
  }
  if (racePhase.carNumber2 == 777) {
    await applyWinner(racePhase, -777);
  }
}

void _postRaceResults(RacePhaseCOMPAT racePhase, int winMs) async {
  Map<String, String> resultMap = {};

  if (racePhase.raceStandingID == null) {
    print("Cannot apply results to RED race");
    return;
  }
  resultMap["block1"] = racePhase.carNumber1.toString();
  resultMap["block2"] = racePhase.carNumber2.toString();
  resultMap["phaseTime"] = winMs.toString();
  resultMap["racePhaseId"] = racePhase.id.toString();
  //final String resultJson = json.encode(resultMap);
  new PostAddBlocks().postForm(resultMap, "/applyWinner");
  //prefStreamController.add(timerConfig);
}

void applyWinner(RacePhaseCOMPAT racePhase, int winMs) async {

  await _postRaceResults(racePhase, winMs);
}

void augmentCredentials(MqttConnectMessage connMess) {
  Map<String, String> envVars = Platform.environment;
  if (isNotEmpty(envVars['MQTT_USER']) &&
      isNotEmpty(envVars['MQTT_PASSWORD'])) {
    print("augmentCredentials, adding user/password:");
    connMess.authenticateAs(envVars['MQTT_USER'], envVars['MQTT_PASSWORD']);
  } else {
    print("augmentCredentials, no credentials");
  }
}

String getMqttHost() {
  Map<String, String> envVars = Platform.environment;
  if (isNotEmpty(envVars['MQTT_HOST'])) {
    return envVars['MQTT_HOST'];
  }
  return "mqtt"; // let docker dns resolve it
}

/// The subscribed callback
void onSubscribed(String topic) {
  print("EXAMPLE::Subscription confirmed for topic $topic");
}

/// The unsolicited disconnect callback
void onDisconnected() {
  print("EXAMPLE::OnDisconnected client callback - Client disconnection");
  exit(-1);
}

loadRaceConfig()async  {
  Map<String, String> envVars = Platform.environment;
  if (isNotEmpty(envVars['RACE_CONFIG_FILE'])) {
    File f = new File(envVars['RACE_CONFIG_FILE']);
    String xml=f.readAsStringSync();
    RaceConfig raceConfig = RaceConfig.fromXml(xmlString: xml, raceName: "localhost");

    StaticShared.getShared().raceConfig=raceConfig;
  }
  if (isNotEmpty(envVars['MQTT_USER']) &&
      isNotEmpty(envVars['MQTT_PASSWORD'])) {
    StaticShared.getShared().loginCredentials=new LoginCredentials(user:envVars['MQTT_USER'], password:envVars['MQTT_PASSWORD']  );
    print("loadRaceConfig, priming user/password:");
    await requestFileUpload("invalidFileName"); // won't upload, but should login.
    //Login login = new Login(user: envVars['MQTT_USER'], password: envVars['MQTT_PASSWORD']);
    //await login.doLogin();
  }
}

class PropagateViaMqtt extends PropagateStatusCallbacks {
  final MqttClient client;
  PropagateViaMqtt(MqttClient this.client);

  @override
  void onFinish(FinishDetailMap finishDetailMap) {
    print("onPropagateStatus ${json.encode(finishDetailMap.toJson())}\n");
    final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addString(finishDetailMap.toString());
    client.publishMessage("fooTest", MqttQos.atLeastOnce, builder.payload);

    List<FinishResult> fr=finishDetailMap.getFinishResults();



    int winMicros=finishDetailMap.getWinTime(0);
    int winMs=SimplePinSummary.microsToMillis(winMicros);
    String winPin=fr[0].pinNumber;

    print("RAW: onFinish timeMS: $winMs pin: ${winPin}");

    if(globalNextOnBlocks!=null){

      if(winPin==lane1Pin){
        print("RAW: lane1 winner");
      }
      else if(winPin==lane2Pin){
        print("RAW: lane2 winner");
        winMs=winMs* -1;
      }
      else{
        print("RAW: skipping update, unknown pin: $winPin");
        return;
      }
      finishDetailMap.finishDetailMap[lane1Pin].carNumber=globalNextOnBlocks.carNumber1.toString();
      finishDetailMap.finishDetailMap[lane2Pin].carNumber=globalNextOnBlocks.carNumber2.toString();

      // don't apply invalid results automatically.
      if( fr[0].validDerbyRace){
        // let this run async...
        applyWinner(globalNextOnBlocks, winMs);
      }
      else{
        print("RAW: skipping update, invalid race results: ${fr[0]}");
      }
    }

    logAndUploadFinish(finishDetailMap);

  }

  @override
  void onHeartbeat(List<String> errorList) {
    final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addString(errorList.toString());
    client.publishMessage("heartbeatHi", MqttQos.atLeastOnce, builder.payload);
  }

  @override
  void onPinChange(PinChange pinChange) {
    logAndUploadPinChange(pinChange);

  }
}

void logAndUploadFinish(FinishDetailMap finishDetailMap) async{
  const String fdmFileName="fdmWithCarNumbers.ndjson";

  await finishDetailMap.historyBuffer.logFinishJson(finishDetailMap,baseName: fdmFileName);
  await requestFileUpload(fdmFileName);
}
void logAndUploadPinChange(PinChange pinChange)async{
  const String pinChangeFileName="pinChange.ndjson";

  final String logJson=json.encode(pinChange.toJson());
  await HistoryBuffer.logString(logJson,baseName: pinChangeFileName);
  queueFileUpload(pinChangeFileName);
}

// Expecting these to get received in chunks.
//   those chunks will be consolidated to (hopefully) reduce the number
//   of upload Requests.
HashSet<String > pendingUploadSet=new HashSet<String>();
void queueFileUpload(String fname){

  pendingUploadSet.add(fname);
  new Timer(new Duration(seconds: 3), (){
    print("Timer: queueFileUpload file $fname ");

    while(pendingUploadSet.length>0){
      String fname=pendingUploadSet.first;
      pendingUploadSet.remove(fname);
      requestFileUpload(fname);
    }

  });
}


void requestFileUpload(String fname)async {
  print("requestFileUpload BEGIN file $fname");

  Map<String,String> postMap={"fname":fname};
  try {
    await new PostAddBlocks().postForm(postMap, "/publishFileToS3");
  }catch(e){
    print("requestFileUpload file $fname error: $e");
  }
}
