## Expected from .env
#MQTT_HOST=test.mosquitto.org
#MQTT_USER=
#MQTT_PASSWORD=
set -o allexport; source .env; set +o allexport


env|grep MQTT
docker ps  --filter name=mqtimer >/dev/null 2>&1 && docker rm -f mqtimer
docker run -d  \
	--name mqtimer \
	-e MQTT_HOST=$MQTT_HOST \
	-e MQTT_USER=$MQTT_USER \
	-e MQTT_PASSWORD=$MQTT_PASSWORD \
	-e RACE_CONFIG_FILE=/root/RaceConfig.xml \
	-v $PWD/RaceConfig.xml:/root/RaceConfig.xml \
	-p8181:8181 \
	cjw/dart_mq_timer
