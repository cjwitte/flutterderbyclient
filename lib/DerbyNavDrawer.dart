import 'package:flutter/material.dart';
import 'package:flutter0322/appPages/RaceSelectionFuture.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/widgets/MqttStatusWidget.dart';

class DerbyNavDrawer {
  static popNavMenu(BuildContext context) {
    Navigator.of(context).canPop() && Navigator.of(context).pop();
  }

  static Drawer getDrawer(BuildContext context) {
    globals.globalDerby.mqttObserver.refreshConnectionStatus();

    String title = "Derby Menu";
    bool hasConfig = StaticShared.getShared().raceConfig != null;
    if (hasConfig) {
      title = StaticShared.getShared().raceConfig.raceName;
      title = (title == null) ? "Derby Race" : title;
    }

    List<Widget> drawerItems = [];
    drawerItems.add(new DrawerHeader(
      child: new Column(children: [new Text(title), new MqttStatusWidget()]),
      decoration: new BoxDecoration(
        color: Theme.of(context).primaryColor,

      ),
    ));

    if (hasConfig) {
      drawerItems.add(new ListTile(
        title: new Text('Races'),
        onTap: () async {
          popNavMenu(context);
          Navigator.of(context).pushReplacementNamed('/RacesTab');
        },
      ));
      drawerItems.add(new ListTile(
        title: new Text('Racers'),
        onTap: () async {
          popNavMenu(context);

          Navigator.of(context).pushReplacementNamed('/Racers');
        },
      ));

      drawerItems.add(new ListTile(
          title: new Text('Brackets'),
          onTap: () async {
            popNavMenu(context);

            Navigator.of(context).pushReplacementNamed('/Brackets');
          }));

      if (globals.globalDerby.experimental)
        drawerItems.add(new ListTile(
            title: new Text('TimerChart'),
            onTap: () async {
              popNavMenu(context);

              Navigator.of(context).pushReplacementNamed('/TimerChart');
            }));

      if (true || globals.globalDerby.experimental)
        drawerItems.add(new ListTile(
            title: new Text('Lane Status'),
            onTap: () async {
              popNavMenu(context);

              Navigator.of(context).pushReplacementNamed('/TimerLaneStatus');
            }));
      if (true || globals.globalDerby.experimental)
        drawerItems.add(new ListTile(
            title: new Text('Timer Detail'),
            onTap: () async {
              popNavMenu(context);

              Navigator.of(context).pushReplacementNamed('/TimerDetail');
            }));

      if (false)
        drawerItems.add(new ListTile(
            title: new Text('Racers2'),
            onTap: () async {
              popNavMenu(context);

              Navigator.of(context).pushReplacementNamed("/RacersG");
            }));

      drawerItems.add(new ListTile(
          title: new Text('Refresh Log'),
          onTap: () async {
            popNavMenu(context);

            Navigator.of(context).pushReplacementNamed("/RefreshLog");
          }));
    }
    //always allow new race selection.
    drawerItems.add(new ListTile(
      title: new Text('Race Selection'),
      onTap: () {
        RaceSelectionFuture.loadAndPush(context);
      },
    ));


    if (true) {
      drawerItems.add(new ListTile(
        title: new Text('Preferences'),
        onTap: () {
          Navigator.of(context).pushReplacementNamed("/Preferences");
        },
      ));
    }

    return new Drawer(
// Add a ListView to the drawer. This ensures the user can scroll
// through the options in the Drawer if there isn't enough vertical
// space to fit everything.
      child: new ListView(
// Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: drawerItems,
      ),
    );
  }
}
