import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter0322/DerbyNavDrawer.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/modelUi.dart';
import 'package:flutter0322/utils/Utils.dart';
import 'package:flutter0322/widgets/RaceResultWidget.dart';
import 'package:flutter0322/globals.dart' as globals;

import '../models.dart';

enum RoundStatus { Active, NotStarted, Finished }

class BracketDetailTabsPage extends StatelessWidget {
  int raceBracketId;
  BracketDetailTabsPage() {}
  Stream<RaceBracketDetailUi> tsc;

  static Future<RaceBracketDetailUi>getCurrentRaceBracketDetailUI(int raceBracketId) async {
    RaceBracket raceBracket = await Utils.getRaceBracket(raceBracketId);
    var pendingRaceStandingMap =
        await Utils.getRaceStandingMapForBracket(raceBracketId);
    print("getCurrentRaceBracketDetailUI: $raceBracketId  $raceBracket $pendingRaceStandingMap");
    return new RaceBracketDetailUi(raceBracket, pendingRaceStandingMap);
  }

  @override
  Widget build(BuildContext context) {
    RaceBracketDetailUi raceBracketDetailUiArg =
        ModalRoute.of(context).settings.arguments;

    raceBracketId=raceBracketDetailUiArg.raceBracket.id;
    tsc = globals.globalDerby.derbyDb.recentChangesController.stream
        .where((x) => x == "ANY")
        .asyncMap((x) {
          print("tsc looking up $raceBracketId");
      return getCurrentRaceBracketDetailUI(raceBracketId);
    });

    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Bracket Detail"),
        ),
        drawer: DerbyNavDrawer.getDrawer(context),
        body: new StreamBuilder<RaceBracketDetailUi>(
            stream: tsc,
            initialData: raceBracketDetailUiArg,
            builder: (BuildContext context,
                AsyncSnapshot<RaceBracketDetailUi> asyncSnapshot) {
              if (asyncSnapshot.hasError) {
                print("BracketDetailTabsPage: error");

                return new Text("Error!");
              } else if (asyncSnapshot.data == null) {
                print("BracketDetailTabsPage: null");
                //return new BracketDetailTabsWidget(raceBracketDetailUiArg);

                return new Text("Null Data");
              } else {
                print("BracketDetailTabsPage  : data");
                return new BracketDetailTabsWidget(asyncSnapshot.data);
              }
            }));
  }
}

class BracketDetailTabsWidget extends StatelessWidget {
  int tabCount;
  final RaceBracketDetailUi raceBracketDetailUi;
  Map<String, List<DisplayableRace>> heatDetailByRound;
  Map<String, bool> inactiveTabMap = {};
  final bool collapseBracketDisplayOLD;
  bool collapseBracketDisplay;

  BracketDetailTabsWidget(this.raceBracketDetailUi,
      {this.collapseBracketDisplayOLD: false}) {
    collapseBracketDisplay = globals.globalDerby.collapseBracketDisplay;
    print(
        "BracketDetailWidget: collapseBracketDisplay1: $collapseBracketDisplay");
    heatDetailByRound = raceBracketDetailUi.getDisplayableRaceByRound();
    //print("tab demo:" + heatDetailByRound.toString());
    removeInactiveTabs(collapseBracketDisplay);
    tabCount = heatDetailByRound.keys.length;
  }

  // constructor init is not complete yet, parameterize the boolean we need.
  void removeInactiveTabs(bool collapseBracketDisplay) {
    for (var key in raceBracketDetailUi.raceBracketDetail.heatDetailMap.keys) {
      print("hdm key: $key");
    }
    List<String> rmKeys = [];
    for (String hd in heatDetailByRound.keys) {
      print("\nHDBR key: $hd");

      Map<RaceStatus, int> recap = {};
      for (RaceStatus rs in RaceStatus.values) {
        recap[rs] = 0;
      }

      for (DisplayableRace displayableRace in heatDetailByRound[hd]) {
        print(
            "hdbr RaceStatus: ${displayableRace.getRaceStatus()} ${displayableRace.getCarNumbers()}");
        RaceStatus raceStatus = displayableRace.getRaceStatus();
        recap[raceStatus]++;
      }
      if (recap[RaceStatus.PendingProgress] > 0) {
      } else if (recap[RaceStatus.PendingSeed] > 0) {
      } // don't suppress the Pending seeds
      else if ("Places" == hd) {
      } // don't suppress the places tab...
      else {
        rmKeys.add(hd);
        inactiveTabMap[hd] = true;
      }
    }
    if (collapseBracketDisplay) {
      print(
          "BracketDetailWidget: collapseBracketDisplay2: $collapseBracketDisplay");

      for (String key in rmKeys) {
        print("removeInactiveTabs: removing $key");
        if (heatDetailByRound.length > 1) {
          // don't remove last key!
          heatDetailByRound.remove(key);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
        onRefresh: globals.globalDerby.refreshStatus.doRefresh,
        child: getBodyWithTabs(context));
  }

  Widget getBodyWithTabs(BuildContext context) {
    var tabBarWidgets = new List<Widget>();
    var tabBarViews = new List<Widget>();
    for (var hd in heatDetailByRound.keys) {
      //TextDecoration textDecoration=(inactiveTabMap.containsKey(hd))?TextDecoration.lineThrough:TextDecoration.none;
      TextDecoration textDecoration = TextDecoration.none;
      Color tabColor =
          (inactiveTabMap.containsKey(hd)) ? Colors.white : Colors.black;
      tabBarWidgets.add(new Tab(
          child: new Text(
        hd,
        style: new TextStyle(
          decoration: textDecoration,
          color: tabColor,
        ),
      )));

      tabBarViews.add(_getTabContent(heatDetailByRound[hd]));
    }
    return new DefaultTabController(
      length: tabCount,
      child: new Scaffold(
        appBar: new AppBar(
          bottom: new TabBar(
            tabs: tabBarWidgets,
            isScrollable: true,
          ),
          title: new Column(
            children: <Widget>[
              //new Text(raceBracketDetailUi.raceBracketDetail.raceTitle),

              new FlatButton(
                  onPressed: () {
                    globals.globalDerby.collapseBracketDisplay =
                        !globals.globalDerby.collapseBracketDisplay;

                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new BracketDetailTabsWidget(
                                raceBracketDetailUi,
                                collapseBracketDisplayOLD: true)));
                  },
                  child:
                      new Text(raceBracketDetailUi.raceBracketDetail.raceTitle))
            ],
          ),
        ),
        body: new TabBarView(
          children: tabBarViews,
        ),
      ),
    );
  }

  Widget _getTabContent(List<DisplayableRace> drList) {
    var rpList = new List<Widget>();
    for (var displayableRace in drList) {
      var rrw = null;
      rrw = new RaceResultWidget(
        displayableRace: displayableRace,
        zebraIndex: rpList.length,
      );

      if (rrw != null) rpList.add(rrw);
    }
    return RefreshIndicator(
        onRefresh: globals.globalDerby.refreshStatus.doRefresh,
        child: new ListView(
          children: rpList,
        ));
  }
}
