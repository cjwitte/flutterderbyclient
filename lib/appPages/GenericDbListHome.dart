import 'package:flutter/material.dart';
import 'package:flutter0322/DerbyNavDrawer.dart';
import 'package:flutter0322/appPages/DbRefreshAid.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/models.dart';
import 'package:flutter0322/widgets/RaceBracketWidget.dart';
import 'package:flutter0322/widgets/RacerWidget.dart';
import 'package:flutter0322/widgets/RefreshLogWidget.dart';

class GenericDbListHome extends StatefulWidget {
  GenericDbModel dbModel;
  GenericDbListHome(this.dbModel);

  @override
  State<StatefulWidget> createState() {
    print("GDLH Creating");
    return new GenericDbListHomeState(dbModel: dbModel);
  }
}


class GenericDbListHomeState extends State<GenericDbListHome>
    implements DbRefreshAid {
  GenericDbModel dbModel;

  List<Map<String, dynamic>> genericDbMap = [];

  bool firstTime = true;
  BuildContext lastContext;


  GenericDbListHomeState({this.dbModel}) {
    print("dbModel: $dbModel");
    print("dbModel: ${dbModel.tableName}");
    DbRefreshAid.dbAidWatchForNextChange(this, dbModel.tableName);
  }

  @override
  Widget build(BuildContext context) {
    print("GenericDbListHomeState build db length:  ${genericDbMap.length}");

    lastContext = context;
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(dbModel.title),
      ),
      drawer: DerbyNavDrawer.getDrawer(context),
      body: //new TextFormFieldDemo(),
          getGenericListBodyFromDB(),
      floatingActionButton: dbModel.getFab(context), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void requestRefresh(BuildContext context) async {
    print("requestRefresh!");
    await globals.globalDerby.refreshStatus.doRefresh();
    //Navigator.push(context,
    //  new MaterialPageRoute(builder: (context) => new RacerHome()));
  }

  Widget getGenericListBodyFromDB() {
    if (firstTime) {
      // TODO: we seem to be recurse ing w/o this!?
      queryDataFromDb();
      firstTime = false; // don't initiate query on subsequent build events.
    }

    return RefreshIndicator(
        onRefresh: globals.globalDerby.refreshStatus.doRefresh,
        child: ListView.builder(
            itemBuilder: dbModel.itemBuilder, itemCount: genericDbMap?.length));
  }

  @override
  bool queryDataFromDb() {
    dbModel.title;
    globals.globalDerby.derbyDb?.database
        ?.rawQuery(dbModel.selectSql)
        ?.then((list) {
      print("queryDataFromDb repopulate ${dbModel.tableName}! ${list.length}");

      if (mounted) {
        setState(() {
          genericDbMap = list;
          dbModel.setGenericDbMapCopy(genericDbMap);
        });
      }
    });
    return this.mounted;
  }

  @override
  bool isWidgetMounted() {
    return this.mounted;
  }
}








typedef Widget WidgetItemBuilder(BuildContext context, int index);

abstract class GenericDbModel<T extends HasRelational, HasJson> {
  String title; //Racers
  String tableName; // Racer
  String selectSql;
  WidgetItemBuilder itemBuilder;
  List<Map<String, dynamic>> genericDbMapCopy;

  Widget getFab(BuildContext context);
  // this is set prior to calling itemBuilder
  setGenericDbMapCopy(List<Map<String, dynamic>> genericDbMap){
    this.genericDbMapCopy=genericDbMap;
  }

}
class BracketDbModel extends GenericDbModel{
  BracketDbModel(){
    title="Race Brackets";
    tableName="RaceBracket";
    selectSql=RaceBracket.getSelectSql();
    itemBuilder=raceBracketItemBuilder;
  }
  RaceBracketWidget raceBracketItemBuilder(BuildContext context, int index) {

    Color bg = globals.globalDerby.myColors.stripeColor(index,context);
    RaceBracket raceBracket = new RaceBracket.fromJsonMap(genericDbMapCopy[index]);

    print("building: $raceBracket");
    return new RaceBracketWidget(
      raceBracket: raceBracket,
      bgColor: bg,
        key: new Key(raceBracket.id.toString())
    );
  }

  @override
  Widget getFab(BuildContext context) {
    if(!globals.globalDerby.loginCredentials.canEditRacers()){
      return new Container();
    }
    return new FloatingActionButton(
      onPressed: ()  {
        Navigator.of(context).pushNamed("/BracketEditWidget");
      },
      tooltip: 'Add',
      child: const Icon(Icons.add),
    );
  }
}

class RacerDbModel extends GenericDbModel{
  RacerDbModel(){
    title="Racers";
    tableName="Racer";
    selectSql=Racer.getSelectSql();
    itemBuilder=racerItemBuilder;
  }
  RacerWidget racerItemBuilder(BuildContext context, int index) {
    Color bg = globals.globalDerby.myColors.stripeColor(index,context);
    Racer racer = new Racer.fromJson(genericDbMapCopy[index]);
    return new RacerWidget(
      racer: racer,
      bgColor: bg,
      key: new Key(racer.carNumber.toString())
    );
  }

  @override
  Widget getFab(BuildContext context) {
    if(!globals.globalDerby.loginCredentials.canEditRacers()){
      return new Container();
    }
    return new FloatingActionButton(
      onPressed: ()  {
       //showRacerEditDialog( context, null) ;
       Navigator.of(context).pushNamed("/RacerEditWidget");

      },

      tooltip: 'Add',
      child: const Icon(Icons.add),
    );
  }
}
class RefreshLogDbModel extends GenericDbModel{
  RefreshLogDbModel(){
    title="RefreshLog";
    tableName="RefreshLog";
    selectSql=RefreshLog.getSelectSql();
    itemBuilder=rlItemBuilder;
  }
  Widget rlItemBuilder(BuildContext context, int index) {
    //Color bg = index % 2 == 0 ? Colors.grey : null;
    Color bg = globals.globalDerby.myColors.stripeColor(index,context);

    RefreshLog refreshLog = new RefreshLog.fromJsonMap(genericDbMapCopy[index]);
    return new RefreshLogWidget(
      refreshLog: refreshLog,
      bgColor: bg,
    );
  }
  @override
  Widget getFab(BuildContext context) {
    return new Container(); // no fab.
  }
}
