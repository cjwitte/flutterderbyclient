import 'package:flutter/material.dart';
import 'package:flutter0322/appPages/DbRefreshAid.dart';
import 'package:flutter0322/dialogs/AddPendingCarsDialog.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/modelUi.dart';
import 'package:flutter0322/models.dart';
import 'package:flutter0322/shared/HistoryType.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:flutter0322/widgets/FilterRowWidget.dart';
import 'package:flutter0322/widgets/RaceResultWidget.dart';
import 'package:flutter0322/widgets/TimerLaneStatusWidget.dart';

class RacePhasePage extends StatefulWidget implements WidgetsWithFab {
  final HistoryType historyType=HistoryType.Phase;
  @override
  State<StatefulWidget> createState() {
    return new RacePhasePageState();
  }

  @override
  Widget getFab(BuildContext conext){
    return new StreamBuilder<LoginCredentials>(
        stream: StaticShared.getShared().loginCredentialsStream ,
        initialData: StaticShared.getShared().loginCredentials,
        builder: (BuildContext context, AsyncSnapshot<LoginCredentials> asyncSnapshot) {

          if (asyncSnapshot.hasError) {
            return new Text("LoginCredentials Error!");
          } else if (asyncSnapshot.data == null) {
            return new  Text("Login Credentials Null Data");
          }else {
            return buildFab(context, asyncSnapshot.data);
          }});
  }
  Widget buildFab(BuildContext context, LoginCredentials loginCredentials){
    print ("buildFab called with $loginCredentials");
    if(! loginCredentials.canAddRacePhase()){
      return new Container();
    }

    return new FloatingActionButton(
      onPressed: () {
        onFabClicked(context);
      },
      tooltip: 'Add',
      child: const Icon(Icons.add),
    );
  }


  void onFabClicked(BuildContext context) {

    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new AddPendingCarsDialog(
            historyType: HistoryType.Phase,
          );
        },
        fullscreenDialog: true));
  }

}

class RacePhasePageState extends State<RacePhasePage> implements DbRefreshAid {
  String title;
  List<Map<String, dynamic>> racePhaseList = [];
  bool firstTime = true;

  RacePhasePageState() {
    DbRefreshAid.dbAidWatchForNextChange(this, "RacePhase");
  }
  @override
  Widget build(BuildContext context) {
    Widget bodyWidgets;

    bodyWidgets = this.getRacePhaseHistoryBodyFromDB();
    title = "Race Phase History";

    return bodyWidgets;
  }

  Widget getRacePhaseHistoryBodyFromDB() {
    if (firstTime) {
      // TODO: we seem to be recurse ing w/o this!?
      queryDataFromDb();
      firstTime = false; // don't initiate query on subsequent build events.
    }

    int listSize = racePhaseList?.length;
    listSize += 1; // artificially larger for filter.


    return RefreshIndicator(
        onRefresh: globals.globalDerby.refreshStatus.doRefresh,
        child: getListBodyWithPotentialStack(listSize),
    );
  }

  Widget getListBodyWithPotentialStack(int listSize){
    if(globals.globalDerby.showLaneStatusOnPhasePage) {
      return new Stack(
          children: [getListBody(listSize),
          new Positioned(
            left: 150.0,
            top: 5.0,
            child: new TimerLaneStatusWidget(),

          )
          ]);
    }
    else{
      return getListBody(listSize);
    }
  }
  getListBody(int listSize) {
    return ListView.builder(
        itemBuilder: racePhaseItemBuilder, itemCount: listSize);
  }
  Widget racePhaseItemBuilder(BuildContext context, int index) {
    if (index == 0) {
      return new FilterRowWidget(triggerTable: RacePhase);
    } else {
      index += -1;
    }

    RacePhase racePhase = new RacePhase.fromSqlMap(racePhaseList[index]);

    RacePhaseUi racePhaseUi = new RacePhaseUi(racePhase);
    RaceResultWidget rrw = new RaceResultWidget(displayableRace: racePhaseUi, zebraIndex: index,);


    return rrw;
  }

  @override
  bool queryDataFromDb() {
    globals.globalDerby.derbyDb?.database
        ?.rawQuery(RacePhase.getSelectSql(
            carFilter: globals.globalDerby.sqlCarNumberFilter))
        ?.then((list) {
      print("RacePhase: repopulateList! $list");

      if (this.mounted) {
        setState(() {
          racePhaseList = list;
        });
      }
    });
    return this.mounted;
  }

  @override
  bool isWidgetMounted() {
    return this.mounted;
  }


}
