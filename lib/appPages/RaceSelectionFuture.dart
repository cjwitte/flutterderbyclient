import 'package:flutter/material.dart';
import 'package:flutter0322/DerbyNavDrawer.dart';
import 'package:flutter0322/network/GetS3Object.dart';
import 'package:flutter0322/widgets/RaceSelectionWidget.dart';

class RaceSelectionFuture extends StatelessWidget {
  final String title;
  final String bucketName;
  RaceSelectionFuture({this.title = "Race Selection", this.bucketName});

  @override
  Widget build(BuildContext context) {
    print("RaceSelectionFuture: build.");
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(title),
      ),
      drawer: DerbyNavDrawer.getDrawer(context),
      body: getRaceSelectionBody(),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget getRaceSelectionBody() {
    print("RaceSelectionFuture: getRaceSelectionBody: $bucketName");

    var futureBuilder = new FutureBuilder(
        future: GetS3Utils.getS3BucketList(bucketName),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          print("RaceSelectionFuture: snapshot data: ${snapshot.data}");

          if (snapshot.hasData && snapshot.data != null) {
            return new Column(
              children: <Widget>[
                new Expanded(
                    child: new ListView(
                  children: _getData(snapshot),
                ))
              ],
            );
          } else {
            return new Dialog(child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[new CircularProgressIndicator(), new Text(" Loading Races")],
            ));
            //return new CircularProgressIndicator();
          }
        });

    return futureBuilder;
  }

  static loadAndPush(BuildContext context,
      [String s3BucketName =
          "https://s3.amazonaws.com/all.derby.rr1.us"]) async {
    //var flist = await new GetS3Object().getS3BucketList(s3BucketName);

    print("loadAndPush: " + s3BucketName.toString());
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) =>
                new RaceSelectionFuture(bucketName: s3BucketName)));
  }

  _getData(AsyncSnapshot snapshot) {
    var raceSelectionList = new List<Widget>();

    int x = 0;
    // Switched to splayTreeMap, so incoming map is sorted now.
    //List<String> keys = flist.keys.toList();
    //keys.sort((a, b) => a.compareTo(b) * -1);
    final Map<String, String> flist = snapshot.data;

    for (var displayFile in flist.keys) {
      Color bg = x % 2 == 0 ? Colors.grey : null;
      x++;
      raceSelectionList.add(new RaceSelectionWidget(
        displayFile: displayFile,
        fullPath: flist[displayFile],
        bgColor: bg,
      ));
    }

    return raceSelectionList;
  }
}
