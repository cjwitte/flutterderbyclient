import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter0322/DerbyNavDrawer.dart';
import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/widgets/TimerHistoryListWidget.dart';

class TimerHistoryPage extends StatelessWidget{
  final StreamController<List<FinishDetailMap>> fdmStreamController=new StreamController(onPause: null, onCancel: null);
  Stream<List<FinishDetailMap>> fdmStream;
  TimerHistoryPage() {
    fdmStream=fdmStreamController.stream;
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Lane Status"),
        ),
        drawer: DerbyNavDrawer.getDrawer(context),
        body: getFdmStreamBuilder(context));
  }
  Widget getFdmStreamBuilder(BuildContext context){
    return new StreamBuilder<List<FinishDetailMap>>(
        stream: fdmStream,
        initialData: [],
        builder:
            (BuildContext context, final AsyncSnapshot<List<FinishDetailMap>> asyncSnapshot) {
          print ("got async snapshot: $asyncSnapshot");
          if (asyncSnapshot.hasError) {
            return new Text("Network Error!?");
          } else {
            return new TimerHistoryListWidget( fdmStreamController,asyncSnapshot.data).getTimerListBody(context);
          }
        });
  }
}
