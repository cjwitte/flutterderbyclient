import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter0322/DerbyNavDrawer.dart';
import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/SimplePinSummary.dart';
import 'package:flutter0322/widgets/TimerHistoryListWidget.dart';
import 'package:flutter0322/widgets/TimerSpsListWidget.dart';

class TimerSpsPage extends StatelessWidget{
  final StreamController<List<SimplePinSummary>> spsStreamController=new StreamController(onPause: null, onCancel: null);
  Stream<List<SimplePinSummary>> spsStream;
  TimerSpsPage() {
    spsStream=spsStreamController.stream;
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Timer Detail"),
        ),
        drawer: DerbyNavDrawer.getDrawer(context),
        body: getFdmStreamBuilder(context));
  }
  Widget getFdmStreamBuilder(BuildContext context){
    return new StreamBuilder<List<SimplePinSummary>>(
        stream: spsStream,
        initialData: [],
        builder:
            (BuildContext context, final AsyncSnapshot<List<SimplePinSummary>> asyncSnapshot) {
          print ("got async snapshot: $asyncSnapshot");
          if (asyncSnapshot.hasError) {
            return new Text("Network Error!?");
          } else {
            return new TimerSpsListWidget( spsStreamController,asyncSnapshot.data).getTimerSpsListBody(context);
          }
        });
  }
}
