import 'package:flutter0322/autoTimer/FinishResult.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/shared/TImerConfig.dart';

class DerbyCarConstraints{
 // final int maxTime=500 * 1000; // 500ms

  //final int minTime=300 * 1000;
  //final int maxPerforationCount=1;
  //final int maxPerforationTime=15*1000; //15ms

  bool isValidLaneBlockedTime(int carLengthTime){
    TimerConfig tc=StaticShared.getShared().timerConfig;
     return (carLengthTime<=tc.maxTime.inMicroseconds && carLengthTime >=tc.minTime.inMicroseconds);
  }

  bool isTolerablePerforationList(List<int>perfList){
    TimerConfig tc=StaticShared.getShared().timerConfig;

    print ("isTolerablePerforationList $perfList");
    var perfSum=0;

    // reduce fails on empty list!?
    if(perfList.length>0){
       perfSum = perfList.reduce((curr, next) => curr + next);

    }
    return(perfList.length<=tc.maxPerforationCount && perfSum<=tc.maxPerforationTime.inMicroseconds);
  }
}
class DerbyFinishFilter {

  DerbyCarConstraints derbyCarConstraints=new DerbyCarConstraints();

  void populateValidDerbyRace(List<FinishResult> finishResults) {
    int okResults=0;
    finishResults.forEach((fr){
      int carLengthTime=fr.finishDetail.laneBlockedTime();
      List<int> perforations=fr.finishDetail.getPerforations();
      if(derbyCarConstraints.isValidLaneBlockedTime(carLengthTime)
      && derbyCarConstraints.isTolerablePerforationList(perforations)){
        okResults++;
      }
    });

    // if filter ok for all lanes, propagate callback.
    if(okResults== finishResults.length){
      // valid result is all or nothing.
      finishResults.forEach((fr){fr.validDerbyRace=true;});
    }
    else{
      print("DerbyFinishFilter: NOT passed.");
    }

  }

}