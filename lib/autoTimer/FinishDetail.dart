import 'dart:convert';

import 'package:flutter0322/autoTimer/DerbyFinishFilter.dart';
import 'package:flutter0322/autoTimer/FinishResult.dart';
import 'package:flutter0322/autoTimer/HistoryBuffer.dart';
import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:flutter0322/autoTimer/RawDataBuffer.dart';
import 'package:json_annotation/json_annotation.dart';

part 'FinishDetail.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class FinishDetailMap {
  Map<String, FinishDetail> finishDetailMap = {};

   final int finishEpoch;

  final HistoryBuffer historyBuffer;

  FinishDetailMap._(this.historyBuffer , {this.finishEpoch}) {
    _initMap();
  }

factory FinishDetailMap(HistoryBuffer historyBuffer , {int finishEpoch}) {

   if(finishEpoch==null) finishEpoch=new DateTime.now().millisecondsSinceEpoch;
   FinishDetailMap rc=new FinishDetailMap._(historyBuffer,finishEpoch: finishEpoch);

   return rc;
}

  int getValidFinishLaneCount() {
    // TODO: pass through real data
    return getValidFinishDetails().length;
  }

  List<FinishDetail> getValidFinishDetails() {
    List<FinishDetail> rc = [];
    finishDetailMap.forEach((pinName, finishDetail) {
      if (finishDetail.isValid()) rc.add(finishDetail);
    });
    print("getValidFinishLaneCount  ${rc.length} \n");
    rc.sort((a, b) => a.getNoseTime().compareTo(b.getNoseTime()));

    return rc;
  }

  // note: PinChanges are assumed to be processed in sorted order (by micros) here!
  FinishDetail finishDetailLogicPlaceholder(PinChange pc) {
    FinishDetail fd = finishDetailMap.putIfAbsent(
        pc.pinNumber, () => new FinishDetail(pinNumber: pc.pinNumber));

    if (pc.pinState) {
      fd.addNoseTime(pc.micros);
    } else {
      fd.addTailTime(pc.micros);
    }

    print("fd is valid: ${fd.isValid()}");

    return fd;
  }

  /*
  First place (index 0) Needs 2 lanes
  2nd place (index 1)   Needs 3 lanes
  3rd place (Index 2)   Needs 4 lanes
   */
  int getWinTime(int placeIndex) {
    List<FinishDetail> finishList = this.getValidFinishDetails();
    if (placeIndex < 0 || placeIndex + 2 > finishList.length) {
      return null;
    }
    FinishDetail topPlace = finishList[placeIndex];
    FinishDetail nextPlace = finishList[placeIndex + 1];

    return (nextPlace.getNoseTime() - topPlace.getNoseTime());
  }

  List<FinishResult> getFinishResults() {
    List<FinishDetail> finishDetails = getValidFinishDetails();
    List<FinishResult> rc = [];
    for (int x = 0; x < finishDetails.length; x++) {

      var fr = new FinishResult(
          pinNumber: finishDetails[x].pinNumber,
          finishDetail: finishDetails[x],
          victoryMarginMicros: getWinTime(x));

      /*
      var fr=(FinishResultBuilder()
        ..pinNumber=finishDetails[x].pinNumber..finishDetail=finishDetails[x]..victoryMarginMicros=getWinTime(x)).build();
      */
      rc.add(fr);
    }

    new DerbyFinishFilter().populateValidDerbyRace(rc);
    print("getFinishResults  ${rc} \n");

    return rc;
  }
  Map<String, dynamic> toJson() => _$FinishDetailMapToJson(this);
  factory FinishDetailMap.fromJson(Map<String, dynamic> json) => _$FinishDetailMapFromJson(json);
  String toJsonNOT() {
    return jsonEncode(finishDetailMap);
    //return jsonEncode(this);
  }
  String toJson3() {
    return jsonEncode(finishDetailMap);
    //return jsonEncode(this);
  }
  String toJson2() {
    Map<String, Object> tmap={
      "fdMap":finishDetailMap
       };
    this.getFinishResults().forEach((fr){
      tmap["lane"+fr.pinNumber]=fr.toJson();
    });
    tmap["captureTime"]=      new DateTime.now().millisecondsSinceEpoch;

    return jsonEncode(tmap);
    //return jsonEncode(this);
  }
  void _initMap() {
    if(historyBuffer==null) {
      print ("JSON COMPAT WARNING: null history buffer");
      return;
    }
    int newestTime = historyBuffer.getNewestTime();

    print("initMap: begin");
    historyBuffer.pinHistoryMap.forEach((pinName, laneHistory) {
      print("initMap: $pinName pingQ length ${laneHistory.pinQ.length}");

      if (laneHistory.isClockPin()) {
        print("initMap skipping: $pinName length ${laneHistory.pinQ.length}");

      } else {
        laneHistory.getCandidatePinChanges(newestTime).forEach((pinChange) {
          print("initMap: pinChange ${pinChange.toJson()}");
          finishDetailLogicPlaceholder(pinChange);
        });
      }
    });
  }
}


@JsonSerializable(nullable: true, includeIfNull: false)
class FinishPeBlocked extends Object {
  int noseTime;
  int tailTime;
  bool isValid() {
    return noseTime != null && tailTime != null;
  }
  FinishPeBlocked();
  Map<String, dynamic> toJson() => _$FinishPeBlockedToJson(this);
  factory FinishPeBlocked.fromJson(Map<String, dynamic> json) => _$FinishPeBlockedFromJson(json);

/*
  FinishPeBlocked.fromJsonMap(Map jsonMap) {
    initFromMap(jsonMap);
  }
  */
  /*
  Map<String, Object> toJson() {
    return {
      "noseTime": noseTime, //
      "tailTime": tailTime, //
    };
  }
  */
  /*

  void initFromMap(Map jsonMap) {
    this.noseTime=jsonMap["noseTime"];
    this.tailTime=jsonMap["tailTime"];
  }
  */
}

@JsonSerializable(nullable: true, includeIfNull: false)
class FinishDetail {
  FinishDetail({this.pinNumber});
  final String pinNumber;
  List<FinishPeBlocked> carSegmentList = [];

  String carNumber;   // Populated after results posted to DB.
  Map<String, dynamic> toJson() => _$FinishDetailToJson(this);
  factory FinishDetail.fromJson(Map<String, dynamic> json) => _$FinishDetailFromJson(json);

  bool isValid() {
    int validSegmentCount = 0;
    carSegmentList.forEach((carSegment) {
      if (carSegment.isValid()) {
        validSegmentCount++;
      }
    });
    // all segments must be valid
    return (carSegmentList.length > 0 &&
        carSegmentList.length == validSegmentCount);
  }

  int laneBlockedTime() {
    if (isValid()) {
      print(
          "laneBlocked: tail ${carSegmentList.last.tailTime} nose: ${carSegmentList.first.noseTime} ");
      return uint32Delta(
          carSegmentList.last.tailTime, carSegmentList.first.noseTime);
    }

    return null;
  }

  Map<String, Object> toJsonSummary() {
    return {
      "pinName": pinNumber, //
      "noseTime": getNoseTime(), //
      "tailTime": getTailTime(), //
      "elapsed": laneBlockedTime(), //
    };
  }
  /*
  Map<String, Object> toJson(){
    return toJsonOriginal();
  }
  */
  Map<String, Object> toJsonOriginal() {
    return {
      "pinNumber": pinNumber, //
      "carSegmentList": jsonEncode(carSegmentList), //
    };
  }

  int getNoseTime() {
    if (carSegmentList.length == 0) return null;
    return carSegmentList.first.noseTime;
  }

  int getTailTime() {
    if (carSegmentList.length == 0) return null;

    return carSegmentList.last.tailTime;
  }

  List<int> getPerforations() {
    List<int> perfList = [];
    // intentionally skip first car segment...
    for (int x = 1; x < carSegmentList.length; x++) {
      var prior = carSegmentList[x - 1];
      var current = carSegmentList[x];
      perfList.add(current.noseTime - prior.tailTime);
    }
    return perfList;
  }

  // assuming PinChanges are ascending w/o duplicates.
  // TODO: validate assumptions??
  void addNoseTime(int micros) {
    //print("addNoseTime [$pinNumber] nose: $micros segments: ${carSegmentList.length}");
    carSegmentList.add(new FinishPeBlocked());
    carSegmentList.last.noseTime = micros; // update newly added noseTime
  }

  void addTailTime(int micros) {
    //print("addTailTime [$pinNumber] tail: $micros segments: ${carSegmentList.length}");

    if (carSegmentList.length > 0) {
      carSegmentList.last.tailTime = micros; // update most recent tail
    }
  }
}
