import 'dart:convert';

import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:json_annotation/json_annotation.dart';

part 'FinishResult.g.dart';


@JsonSerializable(nullable: true, includeIfNull: false)
class FinishResult {
  FinishResult({this.pinNumber, this.victoryMarginMicros, this.finishDetail, this.elapsedMicros}){
    if(this.finishDetail!=null){
      this.elapsedMicros=finishDetail.laneBlockedTime();
    }
  }
  final String pinNumber;
  final int victoryMarginMicros;
   int elapsedMicros;
  final FinishDetail finishDetail;
  bool validDerbyRace=false;

  factory FinishResult.fromJson(Map<String, dynamic> json) => _$FinishResultFromJson(json);
  Map<String, dynamic> toJson() => _$FinishResultToJson(this);
  String toStringNot(){
    return jsonEncode(this).toString();

  }
  /*
  Map<String, Object> toJson() {
    return {
      "pinName": pinNumber, //
      "victoryMarginMicros": victoryMarginMicros, //
      "validDerbyRace": validDerbyRace, //
      "elapsedMicros": elapsedMicros, //
    };
  }
  */
}