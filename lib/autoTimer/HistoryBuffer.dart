import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:flutter0322/autoTimer/RawDataBuffer.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import 'package:semaphore/semaphore.dart';

part 'HistoryBuffer.g.dart';
@JsonSerializable(nullable: true, includeIfNull: false)
class HistoryBuffer {
  Map<String, LaneHistoryBuffer> pinHistoryMap =
      new Map<String, LaneHistoryBuffer>();

  @JsonKey(ignore: true)
  final PropagateStatusCallbacks psCallback;
  static const   int retainTime = 5 * 1000 * 1000;

  HistoryBuffer({this.psCallback});

  Map<String, dynamic> toJson() => _$HistoryBufferToJson(this);
  factory HistoryBuffer.fromJson(Map<String, dynamic> json) => _$HistoryBufferFromJson(json);
  void periodicCheck() {
    //TODO: publish heartbeat to mqtt.
  }

  bool addPinChange(PinChange pc) {
    LaneHistoryBuffer pinHistory = pinHistoryMap.putIfAbsent(
        pc.pinNumber, () => new LaneHistoryBuffer( pc.pinNumber));

    pinHistory.addPinChange(pc);
    this.purgeQueueToRecentDataPlusStable();

    if (pinHistory.isClockPin()) {
      if (pc.pinState) {
        int newestTime = this.getNewestTime();

        List<String> errorList=[];
        this.pinHistoryMap.forEach((pinName, laneHistory) {
          if (!laneHistory.isLaneReady(newestTime: newestTime)) {
            errorList.add("Lane [${pinName}] not ready.");
          }
        });
        psCallback.onHeartbeat(errorList);
      }
      return false;
    }
    if (!pc.pinState) {
      // finish can only be triggered when PE turns on after car clears it!
      print("checking for finish ${pc.pinNumber}");
      return checkForFinish(new FinishDetailMap(this));
    } else {
      return false;
    }
  }
  void purgeQueueToOne() {
    this.pinHistoryMap.forEach((pinName, laneHistory) {
      laneHistory.purgeQueueToOne();
    });
  }
  void purgeQueueToRecentDataPlusStable({int newestTime}) {
    if (newestTime == null) {
      newestTime = this.getNewestTime();
    }
   this.pinHistoryMap.forEach((pinName, laneHistory) {
        laneHistory.purgeQueueToRecentDataPlusStable(newestTime: newestTime);
    });
  }
  bool checkForFinish(FinishDetailMap finishDetailMap) {
    /*
    finishDetailMap.forEach((pinName, finishDetail) {
      print("found finishdetail (wtf) " + finishDetail.toJson());
    });
    */

    var jsonText = finishDetailMap.toJson();
    print("CheckForFinish (finishDetailMap) maybe:" + jsonText.toString());

    bool rc = finishDetailMap.getValidFinishLaneCount() >= 2;
    if (rc && psCallback != null) {
      psCallback.onFinish(finishDetailMap);
      logFinishJson(finishDetailMap);
      this.purgeQueueToOne();
    }
    return rc;
  }
  static var   sm = new LocalSemaphore(1);

  void logFinishJson(FinishDetailMap finishDetailMap, {baseName:"timerLog.ndjson"}) async {
    print ("logFinishJson: BEGIN\n");
    final String logJson=json.encode(finishDetailMap.toJson());

    await logString(logJson, baseName: baseName);
    print ("logFinishJson: DONE\n");
  }

  static void logString(String logJson,{@required String baseName})async{
    await sm.acquire();
    try {
      print ("logFinishJson: $logJson\n");
      String path = await StaticShared
          .getShared()
          .localPersistenceProvider
          .getRacePersistenceDir();
      File ndJsonFile = await new File('$path/$baseName');
      await ndJsonFile.writeAsString("$logJson\n", mode: FileMode.append);

    }
    finally {
      await sm.release();
    }
  }
  int getNewestTime() {
    int rc = 0;
    pinHistoryMap.forEach((pinName, laneHistoryBuffer) {
      rc = max(rc, laneHistoryBuffer.getNewestTime());
    });
    return rc;
  }
}
@JsonSerializable(nullable: true, includeIfNull: false)
class LaneHistoryBuffer {
  @JsonKey(ignore: true)
  Queue<PinChange> pinQ = new Queue<PinChange>();

  //JsonSerializable won't help with Queue.  expose and restore as List, which it is familiar with.
  List<PinChange> get pinList {List<PinChange> rc=[];pinQ.forEach((pc){rc.add(pc);}); return rc;}
  set pinList(List<PinChange> bar) {pinQ.clear();bar.forEach((pc){pinQ.add(pc);});}

  final String pinNumber;
  LaneHistoryBuffer(this.pinNumber );

  Map<String, dynamic> toJson() => _$LaneHistoryBufferToJson(this);
  factory LaneHistoryBuffer.fromJson(Map<String, dynamic> json) => _$LaneHistoryBufferFromJson(json);
  bool addPinChange(PinChange pc) {
    pinQ.addLast(pc);


    return false;

    //this.purgeQueueToRecentDataPlusStable();
  }

  bool isClockPin() {
    return pinNumber.contains("clock");
  }

  // keep most recent 5seconds, plus one prior entry that will be used to validate the the lane was clear prior to
  //  the finish (need to detect flickering PE, etc.  finish only counts if PE was stable prior to car crossing finish line!
  //TODO: reset all history buffers if/when micro sequence resets (python restarts)
  void purgeQueueToRecentDataPlusStable({ @required int newestTime}) {


    //DONE: this is compatible with expectation that top of queue is at least 5 seconds old?
    // (isLaneFlickering will flesh out lanes that are trimmed by length instead of time)
    while (pinQ.length > 30) {
      pinQ.removeFirst();
    }

    print("pinq [$pinNumber]: $pinQ");
    print("pinq [$pinNumber] length: ${pinQ.length}");

    int purgeThreshold = getPurgeThreshold(newestTime: newestTime);
    while (pinQ.length > 1 && pinQ.elementAt(1).micros < purgeThreshold) {
      print("    this.purgeQueueToRecentDataPlusStable: purged 1 pt: ${purgeThreshold}");

      pinQ.removeFirst();
    }
    print("    this.purgeQueueToRecentDataPlusStable: final: ${pinQ.length}");

  }

  //This is done after a valid race is found to prevent duplicating the result.
  void purgeQueueToOne() {
    while (pinQ.length > 1) {
      pinQ.removeFirst();
    }
  }

    int getNewestTime() {
    return pinQ.last.micros;
  }

  int getPurgeThreshold({@required int newestTime}) {


    return newestTime - HistoryBuffer.retainTime;
  }

  // a stable lane should not be flickering.
  // a flickering lane will be purged for size, and the oldest data will be too new.
  bool isLaneStable({@required int newestTime}) {
    return (!isLaneFlickering(newestTime: newestTime));
  }

  List<PinChange> getCandidatePinChanges(int newestTime) {
    List<PinChange> rc = [];

    // No candidate pins s/b returned if PE is flickering!
    if (isLaneFlickering(newestTime: newestTime)) {
      print("Lane Flicker [$pinNumber] warning!");
      return rc;
    }

    for (int x = 1; x < pinQ.length; x++) {
      rc.add(pinQ.elementAt(x));
    }
    return rc;
  }

  bool isLaneFlickering({@required int newestTime}) {

    int purgeThreshold = getPurgeThreshold(newestTime: newestTime);

    return (pinQ.length > 0 && pinQ.elementAt(0).micros >= purgeThreshold);
  }

  isLaneReady({@required int newestTime}) {
    if (isClockPin()) {
      return true;
    }

    print ("isLaneReady: pql ${pinQ.length} ");
    pinQ.forEach((pc){
      print ("  isLaneReady: pq ${jsonEncode(pc)} ");

    });
    return (isLaneStable(newestTime: newestTime) && pinQ.length == 1);
  }
}
