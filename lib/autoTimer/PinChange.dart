import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'PinChange.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class PinChange {
  final int micros;
  final int pubTime;
  final String pinNumber;
  final String pinName; // TODO: deleteme and configure on server side? (instead of hardcode)

  /*
  bool get pinState{
    return (_pinState!=0);
  }
*/
  //@JsonKey(name:"pinState")
   bool pinState;

   /*
  int get  ps{return pinState?1:0;}
   set  ps(int ps){pinState= (ps==0)?true:false;}
*/

  PinChange({this.micros, this.pinNumber, this.pinState, this.pinName, this.pubTime}) ;
  Map<String, dynamic> toJson() => _$PinChangeToJson(this);
  factory PinChange.fromJson(Map<String, dynamic> json) {

    /* python emits 0/1 for pinState.  accept that in ADDITON to dart true/valse */
    if(json['pinState']==0){ json['pinState']=false;}
    if(json['pinState']==1){ json['pinState']=true;}
  return _$PinChangeFromJson(json);
  }


  String toJsonString(){
    return jsonEncode(this).toString();
  }
  String toString(){
    return toJsonString();
  }
/*

  Map<String,Object> toJson() {
    int intState = (this.pinState ? 1 : 0);
    return {"micros": micros, "pinState":intState, "pinNumber":pinNumber };
  }
*/

}