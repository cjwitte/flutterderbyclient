
import 'dart:convert';

import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/HistoryBuffer.dart';
import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:intl/intl.dart';
int uint32Delta(int newerTime, int olderTime) {
  int c = (newerTime - olderTime);
  c &= 0x00ffffffff;
  return c;
}

class Config {
  static const int flushTime = 1000 * 1000;
}

abstract class PropagateStatusCallbacks {
  void onPinChange( PinChange pinChange);
  void onFinish( FinishDetailMap finishDetailMap);
  void onHeartbeat(List<String> errorList);
}

class RawDataBuffer {
  final PropagateStatusCallbacks psCallback;
  HistoryBuffer historyBuffer;

  RawDataBuffer(this.psCallback) {
    historyBuffer = new HistoryBuffer(psCallback: psCallback);
	}
  var pinRX = new RegExp(r"^(\d)(.*)$");

  bool addJsonData(String jsonString) {
    //final json = jsonDecode(jsonString);
    //print("Decoded json: $json");
    //PinChange pinChange = PinChange.fromJsonString(jsonString);
    PinChange pinChange = PinChange.fromJson(json.decode(jsonString));
    if(pinChange.pinName !=null && pinChange.pinName.startsWith("lane")) {
      psCallback.onPinChange(pinChange);
    }
    return historyBuffer.addPinChange(pinChange);
  }

  void periodicCheck() {
    String formattedJms = new DateFormat.yMd().add_jms().format(new DateTime.now());

    print("periodicCheck! $formattedJms");
    historyBuffer.periodicCheck();
  }
}
