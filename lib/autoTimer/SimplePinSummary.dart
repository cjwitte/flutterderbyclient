import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:intl/intl.dart';

class SimplePinSummary {
  int l1lengthMs;
  int l2lengthMs;
  int latestPubTime;

  String winLane;
  int winMs;
  static List<SimplePinSummary> transform(List<PinChange> pcl) {
    const bool nose = true;
    const bool tail = !nose;

    List<SimplePinSummary> rc = [];
    _PcTemp _pcTemp = new _PcTemp();

    pcl.forEach((pc) {
      int idx = -1;
      if (pc.pinName.endsWith("1")) idx = 0;
      if (pc.pinName.endsWith("2")) idx = 1;

      _pcTemp.latestPubTime=pc.pubTime;

      if (idx >= 0) {
        if (pc.pinState == nose) {
          _pcTemp.noseMicros[idx] = pc.micros;
        } else {
          _pcTemp.tailMicros[idx] = pc.micros;
          rc.addAll(_pcTemp.generate());
        }
      }
    });
    return rc;
  }
  static int microsToMillis(int micros){
    return(micros/1000).floor(); // converting from micros to millis!
  }
  static String microsToMillisString(int micros){
    var threeDigits = new NumberFormat("000", "en_US");
    int millis=SimplePinSummary.microsToMillis(micros);
    return threeDigits.format(millis)+"MS";
  }
}

class _PcTemp {
  List<int> noseMicros = [0, 0];
  List<int> tailMicros = [0, 0];
  int latestPubTime;

  bool isComplete() {
    return (noseMicros[0] > 0 &&
        noseMicros[1] > 0 &&
        tailMicros[0] > 0 &&
        tailMicros[1] > 0);
  }

  Iterable<SimplePinSummary> generate() {
    if (!isComplete()) return [];

    var sps = new SimplePinSummary();
    sps.l1lengthMs = SimplePinSummary.microsToMillis(tailMicros[0] - noseMicros[0]);
    sps.l2lengthMs = SimplePinSummary.microsToMillis(tailMicros[1] - noseMicros[1]);

    sps.winMs = SimplePinSummary.microsToMillis(noseMicros[0] - noseMicros[1]);

    sps.winLane = "lane2";
    if (sps.winMs < 0) {
      //changed to conform to FinishDetai.getWinTime()
      //sps.winMs *= -1;
      sps.winMs = SimplePinSummary.microsToMillis(noseMicros[1] - noseMicros[0]);
      sps.winLane = "lane1";
    }

    sps.latestPubTime=this.latestPubTime;
    return ([sps]);
  }

}
