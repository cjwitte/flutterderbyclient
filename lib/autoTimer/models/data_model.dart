library data_model;

/*
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'dart:convert';
import 'package:flutter0322/autoTimer/models/serializers.dart';
part 'data_model.g.dart';

/// Marker interface for classes sent from client to server.
abstract class Command {}
abstract class Login implements Built<Login, LoginBuilder>, Command {
  static Serializer<Login> get serializer => _$loginSerializer;
  String get username;
  String get password;

  factory Login([updates(LoginBuilder b)]) = _$Login;
  Login._();
}
abstract class Foo implements Built<Foo, FooBuilder>, Command {
  static Serializer<Foo> get serializer => _$fooSerializer;
  Login get login;
  RacePhase get racePhase;

  factory Foo([updates(FooBuilder b)]) = _$Foo;
  Foo._();
}

abstract class RacePhase implements Built<RacePhase, RacePhaseBuilder> {
  RacePhase._();

  factory RacePhase([updates(RacePhaseBuilder b)]) = _$RacePhase;

  @BuiltValueField(wireName: 'carNumber1')
  int get carNumber1;
  @BuiltValueField(wireName: 'carNumber2')
  int get carNumber2;
  @BuiltValueField(wireName: 'loadMS')
  int get loadMS;
  @BuiltValueField(wireName: 'lastUpdateMS')
  int get lastUpdateMS;
  @BuiltValueField(wireName: 'id')
  int get id;

  @nullable
  int get raceStandingId;
  @nullable
  @BuiltValueField(wireName: 'version')
  int get version;
  String toJson() {
    return json.encode(serializers.serializeWith(RacePhase.serializer, this));
  }

  static RacePhase fromJson(String jsonString) {
    return serializers.deserializeWith(
        RacePhase.serializer, json.decode(jsonString));
  }

  static Serializer<RacePhase> get serializer => _$racePhaseSerializer;
}

*/
