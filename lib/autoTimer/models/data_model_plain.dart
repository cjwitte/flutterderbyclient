class RacePhaseCOMPAT {
  int id;
  int carNumber1;
  int carNumber2;
  int loadMs;
  int lastUpdateMs;
  int startMs;
  int phaseNumber;
  int raceStandingID;
  bool isDeleted;

       int resultMS;

  String getPhaseLetter() {
    return phaseNumber == 1 ? "A" : "B";
  }

  RacePhaseCOMPAT();
  RacePhaseCOMPAT.fromSqlMap(Map jsonMap) {
    initFromMap(jsonMap);
  }
  RacePhaseCOMPAT.fromJsonMap(Map jsonMap) {
    initFromMap(jsonMap);
  }
  void initFromMap(Map<String, dynamic> jsonMap) {
    this.loadMs = jsonMap["loadMS"];
    this.lastUpdateMs = jsonMap["lastUpdateMS"];
    this.phaseNumber = jsonMap["phaseNumber"];
    this.raceStandingID = jsonMap["raceStandingID"];
    this.id = jsonMap["id"];
    this.resultMS = jsonMap["resultMS"];
    this.carNumber1 = jsonMap["carNumber1"];
    this.carNumber2 = jsonMap["carNumber2"];

    //this.isDeleted = parseIsDeleted(jsonMap["isDeleted"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "raceStandingID": raceStandingID,
      "phaseNumber": phaseNumber,
      "startMs": startMs,
      "lastUpdateMs": lastUpdateMs,
      "loadMs": loadMs,
      "carNumber1": carNumber1,
      "carNumber2": carNumber2,
      "resultMS": resultMS,
    };
  }
}
