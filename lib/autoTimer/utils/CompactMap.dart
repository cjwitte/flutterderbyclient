import 'package:flutter0322/autoTimer/PinChange.dart';

class CompactMap {
  static const int million = 1 * 1000 * 1000;

  List<PinChange> buildPinChangeListFromCompactMapList(
      final List<Map<String, String>> raw) {
    List<PinChange> pcList = [];
    for (var rawMap in raw) {
      pcList.addAll(formatPclFromCompactMap(rawMap));

      // pins should all have an initial value!
      pcList.add(
          new PinChange(micros: 1, pinState: true, pinNumber: rawMap["pin"]));
    }
    pcList.sort((a, b) => a.micros.compareTo(b.micros));
    return pcList;
  }

  List<PinChange> formatPclFromCompactMap(final Map<String, String> rawMap) {
    List<PinChange> pcList = [];

    List<String> microStringList = rawMap["times"].split(":");
    bool pinState = (rawMap["state"] == "1");
    int microSum = 0;
    if (rawMap["base"] != null) {
      microSum = int.parse(rawMap["base"]);
    }
    for (String micro in microStringList) {
      microSum += int.parse(micro);
      PinChange pc = new PinChange(
          micros: microSum, pinState: pinState, pinNumber: rawMap["pin"]);
      pcList.add(pc);
      // prepare for next loop iteration...
      pinState = !pinState;
    }
    return pcList;
  }
}
