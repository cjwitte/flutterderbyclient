import 'package:flutter/material.dart';
import 'package:flutter0322/modelUi.dart';
import 'package:flutter0322/models.dart';
import 'package:flutter0322/widgets/RaceResultWidget.dart';
import 'package:flutter0322/widgets/RaceSelectionWidget.dart';

class DerbyBodyWidgets {

  Widget getRaceSelectionBody(Map<String, String> flist) {
    var raceSelectionList = new List<Widget>();

    int x = 0;
    // Switched to splayTreeMap, so incoming map is sorted now.
    //List<String> keys = flist.keys.toList();
    //keys.sort((a, b) => a.compareTo(b) * -1);

    for (var displayFile in flist.keys) {
      Color bg = x % 2 == 0 ? Colors.grey : null;
      x++;
      raceSelectionList.add(new RaceSelectionWidget(
        displayFile: displayFile,
        fullPath: flist[displayFile],
        bgColor: bg,
      ));
    }
    return new ListView(
      children: raceSelectionList,
    );
  }
}
