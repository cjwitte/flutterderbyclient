import 'package:flutter/material.dart';
import 'package:flutter0322/shared/HistoryType.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';

class AddPendingCarsDialog extends StatefulWidget {
  final HistoryType historyType;
  String title;

  AddPendingCarsDialog({this.historyType}) : assert(historyType != null) {
    setTitle();
  }

  @override
  AddPendingCarsDialogState createState() => new AddPendingCarsDialogState();
  void setTitle() {
    title = "";
    if (historyType == HistoryType.Pending) {
      title = "Add Pending Race";
    }
    if (historyType == HistoryType.Phase) {
      title = "Cars on Ramp";
    }
  }
}

class AddPendingCarsDialogState extends State<AddPendingCarsDialog> {
  final int laneCount = 2;
  final _formKey = GlobalKey<FormState>();

  Map<String,TextEditingController> __editControllerMap = {};

  Map<String,FocusNode> _focusNodeMap = {};
  Map<String,String> _postDataMap = {};

  var carStyle = const TextStyle(fontSize: 36.0, color: Colors.black);

  //ChangeRecord c;
  //Observable l1co=new Observable<ChageRecord>(_lane1Car);

  @override
  void initState() {
    // _lane1Car = widget.raceBracket.raceName;

    List<String> keyLits=[];
    for (int laneNumber = 0; laneNumber < laneCount; laneNumber++) {
      String keyLit=blockKeyFromInt(laneNumber);
      keyLits.add(keyLit);
    }
    if(this.widget.historyType==HistoryType.Pending){
      keyLits.add("chartPosition");
    }
      print("AddPendingController initState");

    keyLits.forEach((keyLit){
      __editControllerMap[keyLit]=new TextEditingController(text: "");
      _focusNodeMap[keyLit]=new FocusNode();
      _postDataMap[keyLit]="";
    });

    super.initState();
  }

  String  blockKeyFromInt(int laneNumber){
    return "block"+(laneNumber+1).toString();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed
    _focusNodeMap.forEach((key,val) => val.dispose());

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.historyType == HistoryType.Standing) {
      return new Text("Illogic");
    }

    return Form(
        key: _formKey,
        child: new Scaffold(
            appBar: new AppBar(
              title: new Text(widget.title),
              actions: [
                new FlatButton(
                    onPressed: () {
                      // TODO: save cars on server!
                      _formKey.currentState.save();

                      LoginAjax loginAjax = new LoginAjax();
                      loginAjax.getAjax();

                      _postDataMap.forEach((key,val) {
                        print("saving car number: $key : $val");
                      });
                      new PostAddBlocks()
                          .postAddBlocks(_postDataMap, this.widget.historyType);
                      Navigator.of(context).pop();
                    },
                    child: new Text('SAVE',
                        style: Theme.of(context)
                            .textTheme
                            .subhead
                            .copyWith(color: Colors.white))),
              ],
            ),
            body: new Column(
              children: getInputTiles(),
            )));
  }

  List<Widget> getInputTiles() {
    List<Widget> rc = [];

    if(this.widget.historyType==HistoryType.Pending){
      rc.add(getChartNumberPrompt());
    }
    for (int idx = 0; idx < laneCount; idx++) {
      rc.add(getInputTile(laneNumber: idx + 1));
    }


    return rc;
  }

  Widget getChartNumberPrompt(){
    String keyLit="chartPosition";
    return new ListTile(
      leading: new Icon(Icons.brightness_1),
      title: new TextFormField(
        decoration: new InputDecoration(
          hintText: 'Chart Position',
        ),
        controller: __editControllerMap[keyLit],
        keyboardType: TextInputType.number,
        maxLength: 15,
        onSaved: (cpos) {
          _postDataMap[keyLit]=cpos;
        },
        //validator: derbyCarOnly,
        //autovalidate: true,
        //style: carStyle,
        focusNode: _focusNodeMap[keyLit],
      ),
    );
  }
  Widget getInputTile({int laneNumber}) {

    int laneIndex = laneNumber - 1;
    String keyLit=blockKeyFromInt(laneIndex);

    bool autoFocusBoolean = (laneIndex == 0);
    print("laneIndex size: " + _postDataMap.length.toString());
    return new ListTile(
      leading: new Icon(Icons.brightness_1),
      title: new TextFormField(
        decoration: new InputDecoration(
          hintText: 'Lane Number $laneNumber',
        ),
        controller: __editControllerMap[keyLit],
        keyboardType: TextInputType.number,
        maxLength: 3,
        onSaved: (carString) {
          _postDataMap[keyLit]=carString;
        },
        validator: derbyCarOnly,
        autovalidate: true,
        style: carStyle,
        autofocus: autoFocusBoolean,
        focusNode: _focusNodeMap[keyLit],
      ),
    );
  }

  void onSaveSetter(String newValue) {}

  String derbyCarOnly(String x) {
    print("derby validator $x");

    if (x.length < 3) {
      return "Car Number Should be 3 digits long";
    } else {
      if (_focusNodeMap["block1"].hasFocus && laneCount > 1) {
        FocusScope.of(context).requestFocus(_focusNodeMap["block2"]);
      }
      /*
      if(__focusNode2.hasFocus){
        FocusScope.of(context).requestFocus(__focusNode1);
      }
      else{
        FocusScope.of(context).requestFocus(__focusNode2);
      }
      */
    }
    return "";
  }
}
