import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';

class LoginDialog extends StatefulWidget {
  String qrString;
  LoginDialog({this.qrString});
  @override
  LoginDialogState createState() => new LoginDialogState();

  static showLoginDialog(BuildContext context, {String qrString}) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new LoginDialog(qrString: qrString);
        },
        fullscreenDialog: false));
  }
}

enum LoginStatus {
  PendingUserInput,
  PendingAjax,
  LoginFailed,
  LoginOK,

}

class LoginDialogState extends State<LoginDialog> {
  var carStyle = const TextStyle(fontSize: 36.0, color: Colors.black);
  final statusStreamController;
  final _formKey = GlobalKey<FormState>();
  Credentials creds;
  LoginDialogState()
      : statusStreamController = new StreamController<LoginStatus>.broadcast() {
    print("LoginDialogState constructor.");
  }
  @override
  void initState() {
    // _lane1Car = widget.raceBracket.raceName;
    print("initState: ${this.widget.qrString}");
    creds = Credentials.fromQrString(this.widget.qrString);

    super.initState();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: new Scaffold(
            appBar: new AppBar(
              title: new Text("Login"),
              actions: [
                new FlatButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        handleLoginClick(context);
                      }
                    },
                    child: new Text('Login',
                        style: Theme.of(context)
                            .textTheme
                            .subhead
                            .copyWith(color: Colors.white))),
              ],
            ),
            body: buildLoginBody(context)));
  }

  Widget buildLoginBody(BuildContext context) {
    print("buildLoginBody: begin");

    return new StreamBuilder<LoginStatus>(
        stream: statusStreamController.stream,
        initialData: LoginStatus.PendingUserInput,
        builder:
            (BuildContext context, AsyncSnapshot<LoginStatus> asyncSnapshot) {
          if (asyncSnapshot.hasError) {
            print("pickLoginBody: error");

            return new Text("Error!");
          } else if (asyncSnapshot.data == null) {
            print("pickLoginBody: null");

            return new Text("Null Data");
          } else {
            print("pickLoginBody: data");

            return getLoginBody(context, asyncSnapshot.data);
          }
        });
  }

  Widget getLoginBody(BuildContext context, LoginStatus loginStatus) {
    //print("getLoginBody: $loginStatus");
    switch (loginStatus) {
      case LoginStatus.PendingUserInput:
        return new Column(children: [
          getUserTile(),
          getPasswordTile(),
        ]);
      case LoginStatus.PendingAjax:
        return getStringMessage("Waiting for login...");

      case LoginStatus.LoginOK:
        return getStringMessage("Login Success!");

      case LoginStatus.LoginFailed:
        return getStringMessage("Login Failed!");

    }
  }

  Widget getStringMessage(String msg) {
    double f1 = 2.0;

    return new Center(
        child: new Text(
      msg,
      textScaleFactor: f1,
    ));
  }

  Widget getUserTile() {
    TextEditingController tec = new TextEditingController();
    if (creds.user.length > 0) {
      tec = TextEditingController.fromValue(
          new TextEditingValue(text: creds.user));
    } else {
      tec =
          TextEditingController.fromValue(new TextEditingValue(text: "timer"));
    }
    return new ListTile(
      leading: new Icon(Icons.speaker_notes,  color: Theme.of(context).accentColor),
      title: new TextFormField(
        decoration: new InputDecoration(
          hintText: 'User',
        ),
        controller: tec,
        keyboardType: TextInputType.text,
        autovalidate: true,
        style: carStyle,
        autofocus: true,
        //validator: fakeLogin,
        onSaved: (String value) {
          creds.user = value;
          fakeLogin(creds.user);
        },
      ),
    );
  }

  Widget getPasswordTile() {
    TextEditingController tec = new TextEditingController();
    if (creds.password.length > 0) {
      tec = TextEditingController.fromValue(
          new TextEditingValue(text: creds.password));
    }
    return new ListTile(
      leading: new Icon(Icons.speaker_notes,  color: Theme.of(context).accentColor),
      title: new TextFormField(
        decoration: new InputDecoration(
          hintText: 'Password',
        ),
        controller: tec,
        keyboardType: TextInputType.text,
        autovalidate: true,
        style: carStyle,
        autofocus: false,
        onSaved: (String value) {
          creds.password = value;
        },
      ),
    );
  }

  String fakeLogin(String user) {
    user = user.toLowerCase();
    Map<String, LoginRole> fakeMap = {
      "ttt": LoginRole.Timer,
      "sss": LoginRole.Starter,
      "aaa": LoginRole.Admin,
      "nnn": LoginRole.None,
    };

    if (fakeMap.containsKey(user)) {
      print("setting user to: ${fakeMap[user]}");
      StaticShared.getShared().loginCredentials = new LoginCredentials(loginRole: fakeMap[user]);
    }
    return null;
  }

  void handleLoginClick(BuildContext context) async {
    _formKey.currentState.save();
    print("formuser: ${creds.user} : ${creds.password}");
    creds.debugAugmentCredentials();
    statusStreamController.add(LoginStatus.PendingAjax);

    Login llogin = new Login(user: creds.user, password: creds.password);
    LoginCredentials loginCredentials = await llogin.doLogin();
    // print("handleClick: $loginCredentials");

    if (loginCredentials == null ||
        loginCredentials.loginRole == null ||
        loginCredentials.loginRole == LoginRole.None) {
      // print("handleClick: sending failed");
      statusStreamController.add(LoginStatus.LoginFailed);
    } else {
      // print("handleClick: sending ok");
      statusStreamController.add(LoginStatus.LoginOK);
      await loginCredentials.persistToFile();
      var xxx=await LoginCredentials.restoreLoginCredentials();
      print("restored from file: $xxx");
    }

    await Future.delayed(new Duration(seconds: 2));
    //Navigator.popUntil(context, ModalRoute.withName('/invalidUnnamed'));

    Navigator.of(context).pop();
  }
}

class Credentials {
  String user = "";
  String password = "";
  Credentials.fromQrString(String qrString) {
    if (qrString != null) {
      List<String> qrl = qrString.split(":");
      if (qrl.length > 0) {
        this.user = qrl.removeAt(0);
      }
      this.password = qrl.join(":");
    }
  }
  void debugAugmentCredentials() {
    if (user.toLowerCase() == "t9") {
      print("augmenting user");
      user = "timer";
      password = ":t9:t9:N:t9";
    }
  }
}
