import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter0322/DerbyNavDrawer.dart';
import 'package:flutter0322/autoTimer/models/Constants.dart';
import 'package:flutter0322/globals.dart';
import 'package:flutter0322/network/MqttOberver.dart';
import 'package:flutter0322/research/PrefDefs.dart';
import 'package:flutter0322/widgets/MqttStatusWidget.dart';
import 'package:intl/intl.dart';


class PrefPage extends StatelessWidget {
  static   const String versionPrompt="Version";
  static const String laneStatusPrompt="Show Lane Status";
  static const String wifiTimerLoginPrompt="WiFi Timer Login";
  static const String wifiTimerSettingsPrompt="Timer Auto Apply Settings";
  static const String wifiTimerConfigServerPrompt="Connect Timer to Cloud";
  static const String changeThemePrompt="Change Theme";
  static const String enableMqtt="Watch for Changes";
  static const String resetMqttPrompt="Reset MQTT";
  final PrefList prefList= PrefList(prefDefs: [
    new PrefDef(prefName: versionPrompt),
    new PrefDef(prefName: laneStatusPrompt),
    new PrefDef(prefName: wifiTimerLoginPrompt),
    new PrefDef(prefName: wifiTimerSettingsPrompt),
    new PrefDef(prefName: wifiTimerConfigServerPrompt),
    new PrefDef(prefName: changeThemePrompt),
    new PrefDef(prefName: enableMqtt, persist: true),
    new PrefDef(prefName: resetMqttPrompt),
  ]);

  StreamController<PrefDef> prefStreamController;
  Stream<PrefDef> prefStream;
  PrefPage(){
    prefStreamController =
    new StreamController<PrefDef>(
        onPause: null,
        onCancel: null
    );
    prefStream = prefStreamController.stream;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Preferences"),
        ),
        drawer: DerbyNavDrawer.getDrawer(context),
        body:getPrefColumnBuilder(context) );
  }

  @override
  Widget getPrefColumnBuilder(BuildContext context) {
    return new StreamBuilder<PrefDef>(
        stream: prefStream,
        initialData: null,
        builder: (BuildContext context,
            AsyncSnapshot<PrefDef> asyncSnapshot) {
          if (asyncSnapshot.hasError) {
            return new Text("PrefDef Error!");
          } else {
            //return buildStatus(context, asyncSnapshot.data);
            return new Column(children: getPrefWidgets(context));
          }
        });
  }
  List<Widget> getPrefWidgets(BuildContext context) {
    List<Widget> rc = [];
    for (PrefDef prefDef in prefList.prefDefs) {
      //rc.add(prefDef.prefWidgetProvider(onClickWrapper));
      if(prefDef.prefName == laneStatusPrompt){
        prefDef.vc=(x){
          prefDef.prefValue=x;
          globalDerby.showLaneStatusOnPhasePage=x;
          prefStreamController.add(prefDef);
        };
        rc.add( new SwitchListTile(value: globalDerby.showLaneStatusOnPhasePage, onChanged: prefDef.vc, title: new Text(prefDef.prefName),));
      }

      if(globalDerby.loginCredentials.isRoleTimer() && prefDef.prefName==wifiTimerLoginPrompt){
        var otap=(){
          prefStreamController.add(prefDef);
          print (wifiTimerLoginPrompt);

          globalDerby.mqttRaspberryPi=  new MqttObserver("rpi-timer1.local", userEnabled: true);
          globalDerby.mqttRaspberryPi.refreshConnectionStatus();

        };
        rc.add( new ListTile( onTap: otap, title: new Text(prefDef.prefName),));

      }
      if(globalDerby.loginCredentials.isRoleTimer() && prefDef.prefName==wifiTimerSettingsPrompt){
        var otap=(){
          //prefStreamController.add(prefDef);
          print (wifiTimerSettingsPrompt);

            Navigator.of(context).pushNamed("/PrefsForTimer");

        };
        rc.add( new ListTile( onTap: otap, title: new Text(prefDef.prefName),));

      }
      if( prefDef.prefName==wifiTimerConfigServerPrompt){
        var otap=(){
          print (wifiTimerConfigServerPrompt);

          Navigator.of(context).pushNamed("/BirdhouseConfig");

        };
        rc.add( new ListTile( onTap: otap, title: new Text(prefDef.prefName),));

      }
      if(prefDef.prefName==changeThemePrompt){
        var otap=(){
          Navigator.of(context).pushNamed("/Themes");
        };
        rc.add( new ListTile( onTap: otap, title: new Text(prefDef.prefName),));

      }
      if(prefDef.prefName == enableMqtt){
        prefDef.vc=(x){
          prefDef.prefValue=x;
          globalDerby.enableMqtt=x;

          globalDerby.mqttObserver.setUserEnabled(globalDerby.enableMqtt); // TODO: refactor
          globalDerby.resetMqttObserver();
          prefStreamController.add(prefDef);
          var fooJson=prefList.toJson();
          var foowJson=prefDef.toJson();
          print("PrefList json: $fooJson");
          print("PrefList 2 json: $foowJson");
        };
        rc.add( new SwitchListTile(value: globalDerby.enableMqtt, onChanged: prefDef.vc, title: new Text(prefDef.prefName),));
      }
      // only show if enabled!
      if(prefDef.prefName==resetMqttPrompt && globalDerby.enableMqtt){
        var otap=(){
            print (resetMqttPrompt);
            globalDerby.mqttObserver.refreshConnectionStatus();
        };
        //rc.add( new ListTile( onTap: otap, title: new Text(prefDef.prefName),));
        rc.add( new ListTile( onTap: otap, title: new MqttStatusWidget(),));

      }
      if(prefDef.prefName==versionPrompt){
        rc.add( new ListTile( onTap: null, title: new Text(prefDef.prefName+" :${buildVersion} "+hhmmss()),));
      }
    }
    return rc;
  }

  String hhmmss(){
      var now = new DateTime.now();
      var formatter = new DateFormat('hhmmss');
      return formatter.format(now);
  }

  void onClickWrapper(ValueChanged<bool> ok){

    print( "onClickWrapper");
  }


}

