import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter0322/DerbyNavDrawer.dart';
import 'package:flutter0322/globals.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/shared/TImerConfig.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:intl/intl.dart';



class PrefsForTimer extends StatelessWidget {
  static const String minMsPrompt = "Min MS";
  static const String maxMsPrompt = "Max MS";
  static const String maxPerfMs = "Max Perf MS";
  static const String maxPerfCount = "Max Perf count";
  final userPrefList = [
    new PrefDef(prefName: minMsPrompt),
    new PrefDef(prefName: maxMsPrompt),
    new PrefDef(prefName: maxPerfMs),
    new PrefDef(prefName: maxPerfCount),
  ];

  TimerConfig pendingConfig = StaticShared.getShared().timerConfig.clone();

  StreamController<TimerConfig> prefStreamController;
  Stream<TimerConfig> prefStream;
  PrefsForTimer() {
    prefStreamController =
        new StreamController<TimerConfig>(onPause: null, onCancel: null);
    prefStream = prefStreamController.stream;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Timer AutoApply Settings"),
          actions: [
            new FlatButton(
                onPressed: () {
                  handleLoginClick(context);
                },
                child: new Text('Save',
                    style: Theme.of(context)
                        .textTheme
                        .subhead
                        .copyWith(color: Colors.white))),
          ],
        ),
        drawer: DerbyNavDrawer.getDrawer(context),
        body: getPrefColumnBuilder(context));
  }

  @override
  Widget getPrefColumnBuilder(BuildContext context) {
    return new StreamBuilder<TimerConfig>(
        stream: prefStream,
        initialData: pendingConfig,
        builder:
            (BuildContext context, AsyncSnapshot<TimerConfig> asyncSnapshot) {
          if (asyncSnapshot.hasError) {
            return new Text("TimerPref Error!");
          } else {
            //return buildStatus(context, asyncSnapshot.data);
            print("timer json: " + asyncSnapshot.data.toString());
            return new Column(
                children: getPrefWidgets(context, asyncSnapshot.data));
          }
        });
  }

  List<Widget> getPrefWidgets(
      BuildContext context, TimerConfig timerConfigData) {
    List<Widget> rc = [];
    for (PrefDef prefDef in userPrefList) {
      //rc.add(prefDef.prefWidgetProvider(onClickWrapper));
      if (globalDerby.loginCredentials.isRoleTimer() &&
          prefDef.prefName == minMsPrompt) {
        prefDef.prefValue = pendingConfig.minTime.inMilliseconds.toString();
        prefDef.vc = (x) {
          prefDef.prefValue = x;
          pendingConfig.minTime = new Duration(milliseconds: int.parse(x));
        };
        //rc.add( new ListTile(  title: getTextEdit2(prefDef)));
        rc.add(getTextEditRow(prefDef));

        //rc.add( new ListTile( onTap: null, title: new Text(prefDef.prefName), subtitle: getTextEdit(prefDef),isThreeLine: true,));

      }

      if (globalDerby.loginCredentials.isRoleTimer() &&
          prefDef.prefName == maxMsPrompt) {
        prefDef.prefValue = pendingConfig.maxTime.inMilliseconds.toString();
        prefDef.vc = (x) {
          prefDef.prefValue = x;
          pendingConfig.maxTime = new Duration(milliseconds: int.parse(x));
        };

        //rc.add( new ListTile(  title: getTextEdit2(prefDef)));
        rc.add(getTextEditRow(prefDef));
      }

      if (globalDerby.loginCredentials.isRoleTimer() &&
          prefDef.prefName == maxPerfCount) {
        prefDef.prefValue = pendingConfig.maxPerforationCount.toString();
        prefDef.vc = (x) {
          prefDef.prefValue = x;
          pendingConfig.maxPerforationCount = int.parse(x);
        };
        rc.add(getTextEditRow(prefDef));
      }

      if (globalDerby.loginCredentials.isRoleTimer() &&
          prefDef.prefName == maxPerfMs) {
        prefDef.prefValue = pendingConfig.maxPerforationTime.inMilliseconds.toString();
        prefDef.vc = (x) {
          prefDef.prefValue = x;
          pendingConfig.maxPerforationTime =new Duration(milliseconds: int.parse(x));
        };
        rc.add(getTextEditRow(prefDef));
      }
    }
    return rc;
  }

  void saveTimerConfig(TimerConfig timerConfig) async {
    new PostAddBlocks().postJson(timerConfig.toString(), "/timer/desiredTimerConfig");
    //prefStreamController.add(timerConfig);
  }

  String hhmmss() {
    var now = new DateTime.now();
    var formatter = new DateFormat('hhmmss');
    return formatter.format(now);
  }

  void onClickWrapper(ValueChanged<bool> ok) {
    print("onClickWrapper");
  }

  Widget getTextEdit(PrefDef prefDef) {
    return new Text(prefDef.prefValue);
  }

  Widget getTextEdit2(PrefDef prefDef) {
    return new TextField(
      autofocus: false,
      maxLength: 3,
      controller: new TextEditingController(text: prefDef.prefValue),
      onSubmitted: prefDef.vc,
      //onChanged: null,
      //keyboardType: TextInputType.number,
      //decoration: new InputDecoration(labelText:"Label",/*hintText: "q"*/),
    );
  }

  Widget getTextEditRow(PrefDef prefDef) {
    return new Row(
      children: <Widget>[
        new Icon(Icons.adjust, size: 48.0),
        new Container(width: 30.0),
        new Container(
            width: 200.0,
            child: new TextField(
              autofocus: false,
              maxLength: 4,
              controller: new TextEditingController(text: prefDef.prefValue),
              onSubmitted: prefDef.vc,
              onChanged: prefDef.vc,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                labelText: prefDef.prefName,
                //  hintText: 'qqq', //
              ),
            )),
      ],
    );
  }

  void handleLoginClick(BuildContext context) {
    saveTimerConfig(pendingConfig);
  }
}

typedef Widget WidgetProvider(ValueChanged);

//  Forked from public class to support templated Value (string vs bool).
class PrefDef<x> {
  final String prefName;
  x prefValue;

  //WidgetProvider prefWidgetProvider;
  ValueChanged<x> vc;

  PrefDef({@required this.prefName, this.prefValue}) {
    /*if(prefWidgetProvider==null){
      prefWidgetProvider=this.foo;
    }*/
  }
}
