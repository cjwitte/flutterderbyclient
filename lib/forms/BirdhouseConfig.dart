import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:dio/dio.dart';
import 'package:wifi/wifi.dart';

class BirdHouseConfig extends StatefulWidget {
  @override
  BirdHouseConfigState createState() {
    return BirdHouseConfigState();
  }
}

class BirdHouseConfigState extends State<BirdHouseConfig> {
  var data;
  bool autoValidate = true;
  bool readOnly = false;
  bool showSegmentedControl = true;
  String wifiIP = "";
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final GlobalKey<FormFieldState> _specifyTextFieldKey =
      GlobalKey<FormFieldState>();

  TextEditingController phoneIpController = new TextEditingController();
  TextEditingController timerIpController = new TextEditingController();

  ValueChanged _onChanged = (val) => print("_onChanged: $val");

  postHttpConfig(_formKey, ip) async {
    print("postHttpConfig $ip");
    Dio dio = new Dio();

    LoginCredentials loginCredentials =
        StaticShared.getShared().loginCredentials;
    var controller = Scaffold.of(_formKey.currentContext)
        .showSnackBar(SnackBar(content: Text('Posting Config.')));

    var appUrl = Uri.parse(StaticShared.getShared().raceConfig.applicationUrl);

    Map<String, String> dataMap = {
      "MQTT_PASSWORD": loginCredentials.password, //
      "MQTT_USER": loginCredentials.user.toLowerCase(), // TODO: user is case sensitive in mqtt, but not in spring :-(
      "MQTT_HOST": appUrl.host, //
    };

    var jsonCodec = new JsonCodec();
    String jsonString = jsonCodec.encode(dataMap);
    print("postBhtConfig json $jsonString");

    var result = "Result unknown";
    try {
      //var ip="rpi-timer2.local";
      var res = await dio.post("http://${ip}:8080",
          data: jsonString,
          options: new Options(
              contentType:
                  ContentType.parse("application/x-www-form-urlencoded")));
      print("PostHttpConfig response: $res");
      result = res.toString();
    } catch (e) {
      print("PostHttpConfig error: $e");
      result = e.toString();
    }
    //controller.close();
    Scaffold.of(_formKey.currentContext).removeCurrentSnackBar();
    Scaffold.of(_formKey.currentContext)
        .showSnackBar(SnackBar(content: Text('Post Config result: ($result)')));

    return "posted";
  }

  @override
  void initState() {
    super.initState();
    Wifi.ip.then((ip) {
      print("ipis: $ip");
      InternetAddress iaip = InternetAddress(ip);
      print("ipis2: $iaip");
      setState(() {
        this.wifiIP = ip;
        phoneIpController.text = ip;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("B.H. Timer Configuration"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              FormBuilder(
                // context,
                key: _fbKey,
                autovalidate: true,
                // readonly: true,
                child: Column(
                  children: <Widget>[
                    FormBuilderTextField(
                      attribute: "phoneIp",
                      decoration: InputDecoration(labelText: "Phone IP (WiFi)"),
                      onChanged: _onChanged,
                      //enabled: false,
                      readOnly: true,
                      controller: phoneIpController,
                      validators: [
                        FormBuilderValidators.IP(),
                      ],
                    ),
                    FormBuilderTextField(
                      attribute: "timerIP",
                      decoration: InputDecoration(labelText: "Timer IP"),
                      onChanged: _onChanged,
                      controller: timerIpController,
                      validators: [
                        requireTimerHack,
                        FormBuilderValidators.IP(),
                        FormBuilderValidators.required(),

                      ],
                    ),
                  ],
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: MaterialButton(
                      color: Theme.of(context).accentColor,
                      child: Text(
                        "Submit",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        _fbKey.currentState.save();
                        if (_fbKey.currentState.validate()) {
                          print(_fbKey.currentState.value);
                          postHttpConfig(
                              _fbKey, _fbKey.currentState.value["timerIP"]);
                        } else {
                          print(_fbKey.currentState.value);
                          print("validation failed");
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: MaterialButton(
                      color: Theme.of(context).accentColor,
                      child: Text(
                        "Reset",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        _fbKey.currentState.reset();
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  /*
  Currently, the only configured for mqtt is lower case timer.  enforce a match here :-(
   */
  String requireTimerHack( val) {
    LoginCredentials loginCredentials =
        StaticShared.getShared().loginCredentials;
    if (loginCredentials.user==null)
      return "Must be logged in.";  // this happened. was it due to debugger reload??
    if (loginCredentials.user.toLowerCase() != "timer")
      return "Please Login as 'timer'";
  }
}
