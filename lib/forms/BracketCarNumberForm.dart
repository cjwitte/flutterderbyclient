import 'dart:convert';

import 'package:flutter0322/modelUi.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:flutter/material.dart';
import 'package:flutter0322/models.dart';
import 'package:flutter0322/utils/Utils.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter0322/globals.dart' as globals;

class BracketArg {
  final RaceBracket raceBracket;
  final RaceBracketDetail raceBracketDetail;
  final List<int> carNumbers;
  final int carNumber;
  final DisplayableRace displayableRace;

  BracketArg(this.raceBracket, this.raceBracketDetail, this.carNumbers,
      this.carNumber, this.displayableRace);
  String getAlphaChartPositionQualifier() {
    // "A" or "B"
    int index = carNumbers.indexOf(carNumber);
    if (index < 0) return "";

    //TODO: this was Connor's idea.  revisit it!
    if (index == 0) {
      return "A";
    }
    if (index == 1) {
      return "B";
    }
    return "Z";
  }
}

class BracketCarNumberWidget extends StatefulWidget {
  @override
  BracketCarNumberWidgetState createState() {
    return BracketCarNumberWidgetState();
  }
}

const carStyle = const TextStyle(fontSize: 36.0, color: Colors.black);

class BracketCarNumberWidgetState extends State<BracketCarNumberWidget> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  RaceBracketDetail rbd;
  RaceBracket raceBracket;

  BracketArg bracketArg;
  @override
  Widget build(BuildContext context) {
    bracketArg = ModalRoute.of(context).settings.arguments;
    rbd = bracketArg.raceBracketDetail;
    raceBracket = bracketArg.raceBracket;

    List<DriverCardState> driverCardStates = [];
    List<DriverCard> driverCards = [];

    bracketArg.carNumbers.forEach((carNumber) {
      DriverCardState dcs = DriverCardState(
          bracketArg: BracketArg(raceBracket, rbd, bracketArg.carNumbers,
              carNumber, bracketArg.displayableRace));
      driverCards.add(DriverCard(dcs));
      driverCardStates.add(dcs);
    });
    ;

    return Scaffold(
        appBar: AppBar(
          title: Text(bracketArg == null
              ? "Add Bracket Car Number"
              : "Edit Bracket Car Number"),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: SingleChildScrollView(
                child: Column(
              children: <Widget>[
                FormBuilder(
                  // context,
                  key: _fbKey,
                  autovalidate: true,
                  // readonly: true,
                  child: Column(children: driverCards),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: MaterialButton(
                        color: Theme.of(context).accentColor,
                        child: Text(
                          "Submit",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          _fbKey.currentState.save();
                          if (_fbKey.currentState.validate()) {
                            print(_fbKey.currentState.value);
                            httpSaveBcn(driverCardStates);
                            Navigator.of(context).pop();

                            //postHttpConfig(_fbKey,_fbKey.currentState.value["timerIP"]);
                          } else {
                            print(_fbKey.currentState.value);
                            print("validation failed");
                          }
                        },
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: MaterialButton(
                        color: Theme.of(context).accentColor,
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();

                          //_fbKey.currentState.reset();
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ))));
  }

  Future<void> httpSaveBcn(final List<DriverCardState> driverCardStates) async {
/*
    @RequestParam(value = "raceBracketID", required = true) Long raceBracketID,//
    @RequestParam(value = "carNumber", required = true) Short carNumber,//
    @RequestParam(value = "dest", required = true) String dest,
    @RequestParam(value = "forfeit", required = false) Boolean isForfeit,
    */

    List<Map<String,String>>dataMapList=[];
    driverCardStates.forEach((dcs){
      var cPos=dcs.bracketArg.getAlphaChartPositionQualifier();
      dataMapList.add(createSubmitMap(cPos));
    });


    Map<String,Object>dataMap={
      //"addPending":false,
      "raceBracketID": raceBracket.id,
      "positionList":dataMapList};
    var json = new JsonCodec();
    String jsonString = json.encode(dataMap);
    print ("httpSaveBcn $jsonString");
    //TODO: modify server to handle upload of list of "raceBracketCarNumbers as Json
    await new PostAddBlocks().getUrl({"json":jsonString}, "/setRaceBracketCarNumberList");
  }

  String getInitialCarNumber() {
    String initialCarNumber =
        (bracketArg.carNumber == null) ? "" : bracketArg.carNumber.toString();

    if (initialCarNumber.startsWith("-")) {
      initialCarNumber = "";
    }
    return initialCarNumber;
  }

  Map <String,String> createSubmitMap(String cPos) {
    bool isForfeit = _fbKey.currentState.value["${cPos}.seedType"] == "Forfeit";

    String nakedChartPosition = _fbKey.currentState.value["${cPos}.chartPosition"]
        .toString()
        .replaceFirst(new RegExp(r".*:"), "");
    String submitCar=_fbKey.currentState.value["${cPos}.carNumber"];

    print("nakedCP: $nakedChartPosition");
    if (_fbKey.currentState.value["${cPos}.seedType"] == "Bye") {
      submitCar=determineByeNumber(submitCar, nakedChartPosition);
    }

    Map<String, String> dataMap = {
      //"id":id.toString(),
      "carNumber": submitCar,
      "dest": nakedChartPosition,
      "isForfeit": isForfeit.toString()
    };
    return dataMap;
  }

  String determineByeNumber(String submitCar, String nakedChartPosition) {
    String abSuffix=nakedChartPosition.replaceAll(new RegExp(r"^\d*"), "");
    var encodedSuffix = ascii.encode(abSuffix.toLowerCase());
    var encodedLowerA = ascii.encode("a");

    int suffixOffset=encodedSuffix[0] - encodedLowerA[0];
    String heatStr=nakedChartPosition.replaceAll(new RegExp(r"[a-zA-Z]*$"), "");
    print ("determineByeNumber: [$nakedChartPosition]  suffix: $abSuffix  heat: $heatStr suffixOffset: $suffixOffset");
     int heatInt=int.parse(heatStr);
    return (9000+(heatInt*10)+suffixOffset).toString();
  }
}

class DriverCard extends StatefulWidget {
  DriverCardState driverCardState;
  DriverCard(this.driverCardState);
  @override
  DriverCardState createState() {
    return this.driverCardState;
  }
}

class DriverCardState extends State<DriverCard> {
  final BracketArg bracketArg;
  TextEditingController _nameController;

  bool carNumberVisible = true;

  DriverCardState({@required this.bracketArg});

  void _onCarChanged(value) {
    _nameController.text = value + " Not found";
    Utils.lookupRacerFromDb(value).then((racer) {
      if (racer != null) _nameController.text = racer.racerName;
    });
  }

  void _onTypeChanged(value) {
    print("type changed: $value");
    if (value == "Bye") {
      setState(() => carNumberVisible = false);
      //TODO: disable field. google indicates need for focusScope.
    } else {
      setState(() => carNumberVisible = true);
    }
  }

  @override
  Widget build(BuildContext context) {
    String initialRacerType = "Racer";

    _nameController=new TextEditingController();

    String initialCarNumber =
        bracketArg.carNumber <= 0 ? "" : bracketArg.carNumber.toString();
    if (bracketArg.carNumber >= 9000) {
      initialRacerType = "Bye";
    }
    _onCarChanged(initialCarNumber);
    String quailifiedChartPosition =
        bracketArg.displayableRace.getRaceMetaData().chartPosition +
            bracketArg.getAlphaChartPositionQualifier();
    String cPos=bracketArg.getAlphaChartPositionQualifier();

    return Card(
        child: Column(children: <Widget>[
      FormBuilderTextField(
        attribute: "${cPos}.chartPosition",
        decoration: InputDecoration(labelText: "$cPos - Chart Position"),
        //initialValue:(racerArg==null)?"":racerArg.carNumber.toString() ,
        initialValue: quailifiedChartPosition,
        readOnly: true,
      ),
      FormBuilderDropdown(
        attribute: "${cPos}.seedType",
        validators: [FormBuilderValidators.required()],
        onChanged: _onTypeChanged,
        initialValue: initialRacerType,
        items: [
          "Racer",
          "Bye",
          "Forfeit",
        ]
            .map((lang) => DropdownMenuItem(
                  value: lang,
                  child: Text(lang),
                ))
            .toList(growable: false),
      ),
      Visibility(
        visible: carNumberVisible,
        child: FormBuilderTextField(
          attribute: "${cPos}.carNumber",
          keyboardType: TextInputType.number,
          style: carStyle,
          decoration: InputDecoration(labelText: "${cPos} - Car Number"),
          initialValue: initialCarNumber,
          onChanged: _onCarChanged,
          validators: [
            FormBuilderValidators.maxLength(3),
            FormBuilderValidators.numeric(),
            FormBuilderValidators.required(),
          ],
        ),
      ),
      Visibility(
          visible: carNumberVisible,
          child: FormBuilderTextField(
            attribute: "${cPos}.driverName",
            style: carStyle,

            decoration: InputDecoration(labelText: "${cPos} - Driver Name"),
            //onChanged: _onChanged,
            controller: _nameController,
            readOnly: true,
          )),
    ]));
  }
}
