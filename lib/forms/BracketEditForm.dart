import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:flutter/material.dart';
import 'package:flutter0322/models.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import 'data.dart';

class BracketEditWidget extends StatefulWidget {
  @override
  BracketEditWidgetState createState() {
    return BracketEditWidgetState();
  }
}

class BracketEditWidgetState extends State<BracketEditWidget> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  RaceBracket raceBracketArg;
  @override
  Widget build(BuildContext context) {
    raceBracketArg = ModalRoute.of(context).settings.arguments;

    bool carNumberEnabled = false;
    return Scaffold(
        appBar: AppBar(
          title: Text(
              raceBracketArg == null ? "Add RaceBracket" : "Edit RaceBracket"),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: SingleChildScrollView(
                child: Column(
              children: <Widget>[
                FormBuilder(
                  // context,
                  key: _fbKey,
                  autovalidate: true,
                  // readonly: true,
                  child: Column(
                    children: <Widget>[
                      FormBuilderTextField(
                        attribute: "raceName",
                        decoration: InputDecoration(labelText: "Race Name"),
                        //onChanged: _onChanged,
                        initialValue: (raceBracketArg == null)
                            ? ""
                            : raceBracketArg.raceName,
                        enabled: false,
                        validators: [
                          FormBuilderValidators.required(),
                        ],
                      ),

                      raceBracketArg == null
                          ? FormBuilderDropdown(
                              attribute: "template",
                              decoration: InputDecoration(
                                  labelText: "Bracket Template"),
                              hint: Text('Select Bracket Template'),
                              validators: [FormBuilderValidators.required()],
                              items: validTemplates
                                  .map((templateName) => DropdownMenuItem(
                                        value: templateName,
                                        child: Text('$templateName'),
                                      ))
                                  .toList(),
                            )
                          : new Container(), // no template needed /allowed in edit mode.
                    ],
                  ),
                ),
                Divider(height: 170),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: MaterialButton(
                        color: Theme.of(context).accentColor,
                        child: Text(
                          "Submit",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          _fbKey.currentState.save();
                          if (_fbKey.currentState.validate()) {
                            print(_fbKey.currentState.value);
                            httpSaveRacer();
                            Navigator.of(context).pop();

                            //postHttpConfig(_fbKey,_fbKey.currentState.value["timerIP"]);
                          } else {
                            print(_fbKey.currentState.value);
                            print("validation failed");
                          }
                        },
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: MaterialButton(
                        color: Theme.of(context).accentColor,
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();

                          //_fbKey.currentState.reset();
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ))));
  }

  Future<void> httpSaveRacer() async {
    if (raceBracketArg == null) {
      Map<String, String> dataMap = {
        "raceName": _fbKey.currentState.value["raceName"],
        "template": _fbKey.currentState.value["template"],
      };
      if (raceBracketArg != null) {
        dataMap["id"] = raceBracketArg.id.toString();
      }
      await new PostAddBlocks().getUrl(dataMap, "/addRaceBracket");
    } else {
      Map<String, String> dataMap = {
        "raceName": _fbKey.currentState.value["raceName"],
        "id": raceBracketArg.id.toString(),
      };
      await new PostAddBlocks()
          .getUrl(dataMap, "/renameRaceBracket"); // TODO: need backed endpoint!
    }
  }
}
