import 'package:flutter0322/modelUi.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:flutter/material.dart';
import 'package:flutter0322/models.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';



class ManualTimeEntryPage extends StatefulWidget {
  @override
  ManualTimeEntryPageState createState() {
    return ManualTimeEntryPageState();
  }
}

class ManualTimeEntryPageState extends State<ManualTimeEntryPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  var carStyle = const TextStyle(fontSize: 36.0, color: Colors.black);

  bool firstTime = true;
  RacePhase racePhaseArg;

  @override
  Widget build(BuildContext context) {
    print("BracketCarNumberWidgetState building: $firstTime");

    racePhaseArg = ModalRoute.of(context).settings.arguments;



    String initialRacerType = "Racer";

    bool carNumberEnabled = false;
    return Scaffold(
        appBar: AppBar(
          title: Text(
               "Enter Manual Race Time"),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    FormBuilder(
                      // context,
                      key: _fbKey,
                      autovalidate: true,
                      // readonly: true,
                      child: Column(
                        children: <Widget>[

                          FormBuilderRadio(
                            //decoration: InputDecoration(labelText: 'Type'),
                            // leadingInput: true,
                            attribute: "winningLane",
                            validators: [FormBuilderValidators.required()],

                            initialValue: initialRacerType,
                            options: [
                              "Lane 1",
                              "Lane 2",
                            ].map((lang) => FormBuilderFieldOption(value: lang))
                                .toList(growable: false),
                          ),
                           FormBuilderTextField(
                              attribute: "phaseTime",
                              keyboardType: TextInputType.number,
                              style: carStyle,
                              decoration: InputDecoration(labelText: "Winning Phase Time"),
                              initialValue: "",

                              validators: [
                                FormBuilderValidators.numeric(),
                                FormBuilderValidators.required(),
                              ],
                            ),



                        ],
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: MaterialButton(
                            color: Theme.of(context).accentColor,
                            child: Text(
                              "Submit",
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              _fbKey.currentState.save();
                              if (_fbKey.currentState.validate()) {
                                print(_fbKey.currentState.value);
                                httpApplyWinner();
                                Navigator.of(context).pop();
                              } else {
                                print(_fbKey.currentState.value);
                                print("validation failed");
                              }
                            },
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: MaterialButton(
                            color: Theme.of(context).accentColor,
                            child: Text(
                              "Cancel",
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();

                              //_fbKey.currentState.reset();
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ))));
  }

  Future<void> httpApplyWinner() async {
    String signedPhaseTime=_fbKey.currentState.value["phaseTime"];
    if(_fbKey.currentState.value["winningLane"]=="Lane 2"){
      signedPhaseTime="-" +signedPhaseTime;
    }
    Map<String, String> dataMap = {
      //"id":id.toString(),
      "phaseTime": signedPhaseTime.toString(),
      "racePhaseId": racePhaseArg.id.toString(),
    };

    print( "httpApplyWinner: $dataMap");
    await new PostAddBlocks().getUrl(dataMap, "/applyWinner");
  }




}
