import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:flutter/material.dart';
import 'package:flutter0322/models.dart';
import 'package:flutter0322/widgets/RacerWidget.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:basic_utils/basic_utils.dart';

class RacerEditWidget extends StatefulWidget {
  @override
  RacerEditWidgetState createState() {
    return RacerEditWidgetState();
  }
}

class RacerEditWidgetState extends State<RacerEditWidget> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  var carStyle = const TextStyle(fontSize: 36.0, color: Colors.black);

  Racer racerArg;
  @override
  Widget build(BuildContext context) {
    racerArg = ModalRoute.of(context).settings.arguments;

    bool carNumberEnabled = false;
    return Scaffold(
        appBar: AppBar(
          title: Text(racerArg == null ? "Add Racer" : "Edit Racer"),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: SingleChildScrollView(
                child: Column(
              children: <Widget>[
                FormBuilder(
                  // context,
                  key: _fbKey,
                  autovalidate: true,
                  // readonly: true,
                  child: Column(
                    children: <Widget>[
                      FormBuilderTextField(
                        attribute: "carNumber",
                        decoration: InputDecoration(labelText: "Car Number"),
                        initialValue: (racerArg == null)
                            ? ""
                            : racerArg.carNumber.toString(),
                        keyboardType: TextInputType.number,
                        style: carStyle,
                        validators: [
                          FormBuilderValidators.maxLength(3),
                          FormBuilderValidators.numeric(),
                          FormBuilderValidators.required(),
                        ],
                      ),
                      FormBuilderTextField(
                        attribute: "driverName",
                        decoration: InputDecoration(labelText: "Driver Name"),
                        initialValue:
                            (racerArg == null) ? "" : racerArg.racerName,
                        validators: [
                          FormBuilderValidators.required(),
                        ],
                      ),
                      FormBuilderTextField(
                        attribute: "phoneticName",
                        decoration: InputDecoration(labelText: "Phonetic Name"),
                        //onChanged: _onChanged,
                        initialValue:
                            (racerArg == null) ? "" : racerArg.lastName,
                        validators: [],
                      )
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: MaterialButton(
                        color: Theme.of(context).accentColor,
                        child: Text(
                          "Submit",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: _onSavePressed,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    globals.globalDerby.canSpeak()?getSpeakButton():new Container(width: 0, height: 0),
                    globals.globalDerby.canSpeak()?SizedBox(width: 20):new Container(width: 0, height: 0),
                    Expanded(
                      child: MaterialButton(
                        color: Theme.of(context).accentColor,
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();

                          //_fbKey.currentState.reset();
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ))));
  }


  void _speakName(){
    _fbKey.currentState.save();

    if(!globals.globalDerby.canSpeak()){
      // this blows up too :-(
      //RacerWidget.showSnackBar("TextToSpeech unavailable", context);
      return;
    }
    var driverName= _fbKey.currentState.value["driverName"];
    var phonenticName= _fbKey.currentState.value["phoneticName"];
    var speakName=StringUtils.isNotNullOrEmpty(phonenticName)?phonenticName:driverName;
    print("speaking: $speakName dn: $driverName pn: $phonenticName");
    if(speakName!=null) {
      FlutterTts flutterTts = new FlutterTts();
      flutterTts.speak(speakName);
    }
  }
  void _onSavePressed() {
    _fbKey.currentState.save();
    if (_fbKey.currentState.validate()) {
      print(_fbKey.currentState.value);
      httpSaveRacer();
      Navigator.of(context).pop();

      //postHttpConfig(_fbKey,_fbKey.currentState.value["timerIP"]);
    } else {
      print(_fbKey.currentState.value);
      print("validation failed");
    }
  }

  Future<void> httpSaveRacer() async {
    int id = racerArg == null ? null : racerArg.id;
    String carNumber = _fbKey.currentState.value["carNumber"];
    Map<String, String> dataMap = {
      //"id":id.toString(),
      "carNumber": carNumber,
      "firstName": _fbKey.currentState.value["driverName"],
      "lastName": _fbKey.currentState.value["phoneticName"],
    };
    if (racerArg != null) {
      dataMap["id"] = racerArg.id.toString();
    }
    await new PostAddBlocks().getUrl(dataMap, "/addRacer");
  }

  Widget getSpeakButton() {
    return
      Expanded(
      child: MaterialButton(
        color: Theme.of(context).accentColor,
        child: Text(
          "Speak",
          style: TextStyle(color: Colors.white),
        ),
        onPressed: _speakName,



      ),

    );
  }
}
