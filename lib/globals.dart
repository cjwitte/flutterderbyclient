library globals;

import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter0322/models.dart';
import 'package:flutter0322/network/DerbyDbCache.dart';
import 'package:flutter0322/network/GetS3Object.dart';
import 'package:flutter0322/network/MqttOberver.dart';
import 'package:flutter0322/network/RefreshStatus.dart';
import 'package:flutter0322/network/derbyDb.dart';
import 'package:flutter0322/research/MyColors.dart';
import 'package:flutter0322/shared/LocalPersistenceProvider.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/RaceConfig.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/shared/network/RestDio.dart';
import 'package:flutter0322/widgets/TimerLaneStatusWidget.dart';
import 'package:path_provider/path_provider.dart';
//import 'package:flutter0322/network/FbaseDerby.dart';

class GlobalDerby {
  StreamController<MqttStateLog> mqttStateLogStreamController;
  Stream<MqttStateLog> mqttStateLogStream;

  final TimerLaneStatusModel timerLaneStatusModel=new TimerLaneStatusModel();
  final DerbyDb derbyDb = new DerbyDb();
  final DerbyDbCache racerCache = new DerbyDbCache<Racer>("Racer");
  final DerbyDbCache raceBracketCache =
      new DerbyDbCache<RaceBracket>("RaceBracket");
  int ndJsonRefreshInProgress;
  MqttObserver mqttObserver = new MqttObserver("nohost.nodomain", userEnabled: false);
  MqttObserver mqttRaspberryPi = null;
  bool useMqtt = true;
  bool collapseBracketDisplay = false;
  RefreshStatus refreshStatus = new RefreshStatus();
  MyColors myColors = new MyColors(Colors.indigo);
  String sqlCarNumberFilter = "";

  String deviceModel = "unknown";
  AndroidBuildVersion androidVersion ;
  bool showLaneStatusOnPhasePage = false;
  bool enableMqtt = true;

  bool experimental = false;

  Map<int, Racer> getRacerCache() {
    return Map.castFrom(racerCache.cacheMap);
  }

  Map<int, RaceBracket> getRaceBrakcetCache() {
    return Map.castFrom(raceBracketCache.cacheMap);
  }

  static Future<String> documentProvider() async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static Future<String> qualifiedDocumentProvider() async {
    final directory = await getApplicationDocumentsDirectory();
    StaticShared.getShared().raceConfig;
    //TODO: qualify with racename!

    return directory.path;
  }

  GlobalDerby({RaceConfig raceConfig}) {
    resetMqttObserver();

    StaticShared.getShared().localPersistenceProvider =
        new LocalPersistenceProvider(getTopPersistenceDir: documentProvider, getRacePersistenceDir: qualifiedDocumentProvider);

    mqttStateLogStreamController =
        new StreamController<MqttStateLog>.broadcast();
    mqttStateLogStream = mqttStateLogStreamController.stream;

    StaticShared.getShared().raceConfig = raceConfig;

    getDeviceInfo();

    StaticShared.getShared()
        .loginCredentialsStream
        .listen((LoginCredentials event) {
      print("LoginCredentialsStreamEvent: got login Credentials:  ${event}");

      showLaneStatusOnPhasePage =
          (loginCredentials != null && loginCredentials.canAddRacePhase());

      resetMqttObserver();
    });
  }
  getDeviceInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    try {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      print('Running on ${androidInfo.model}'); // e.g. "Moto G (4)"
      if (androidInfo != null) {
        deviceModel = androidInfo.model;
        androidVersion=androidInfo.version;
      }
    } catch (e) {
      print("Could not get android device info.");
    }
    try {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print('Running on ${iosInfo.utsname.machine}');
      if (iosInfo != null) {
        deviceModel = iosInfo.utsname.machine;
      }
    } catch (e) {
      print("Could not get ios device info.");
    }
  }

  Future _cleanup() async {
    print("_cleanup BEGIN.");

    List<SyncS3AppendingFile> loglist = [
      new SyncS3PinChangeLog(),
      new SyncS3DataLog(),
      new SyncS3TimerLog()
    ];
    loglist.forEach((f) async {
      File oldNdJson = await f.ndJsonFile();

      if (oldNdJson != null && oldNdJson.existsSync()) {
        print("_cleanup deleting: ${oldNdJson.toString()}");

        oldNdJson.deleteSync();
      }
    });

    await deleteCookies();
  }

  // iOS complains if db is deleted w/o close!
  Future closeAll() async {
    print("Global closeAll");
    if (derbyDb != null && derbyDb.database != null) {
      print("closeAll closing db.");

      await derbyDb.database.close();
    }
  }

  Future init(bool doResetDb) async {
    //await new MqttObserver("rr1.us").init();
    if (doResetDb) {
      await _cleanup();
    }
    await derbyDb.init(doReset: doResetDb);
    await racerCache.init(derbyDb);
    await raceBracketCache.init(derbyDb);

    if (useMqtt && mqttObserver != null) {
      mqttObserver.init();
    }
  }

  get loginCredentials {
    return StaticShared.getShared().loginCredentials;
  }

  void resetMqttObserver() {
    RaceConfig raceConfig = StaticShared.getShared().raceConfig;
    if (useMqtt && raceConfig != null) {
      mqttObserver = new MqttObserver(raceConfig.getMqttHostname(),
          user: loginCredentials.user, password: loginCredentials.password, userEnabled: this.enableMqtt);
      mqttObserver.refreshConnectionStatus();
    }
  }

  void setExperimental(bool param0) {
    this.experimental = true;
  }
  bool canSpeak(){
    return (androidVersion !=null && androidVersion.sdkInt>=21);
  }
}

GlobalDerby globalDerby = new GlobalDerby();
