import 'dart:async';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter0322/appPages/BracketDetailTabs.dart';
import 'package:flutter0322/appPages/GenericDbListHome.dart';
import 'package:flutter0322/appPages/SplashPage.dart';
import 'package:flutter0322/appPages/TabbedRaceHistory.dart';
import 'package:flutter0322/appPages/TimerHistoryPage.dart';
import 'package:flutter0322/appPages/TimerSpsPage.dart';
import 'package:flutter0322/forms/BirdhouseConfig.dart';
import 'package:flutter0322/dialogs/PreferencePage.dart';
import 'package:flutter0322/dialogs/PrefsForTimer.dart';
import 'package:flutter0322/forms/BracketCarNumberForm.dart';
import 'package:flutter0322/forms/BracketEditForm.dart';
import 'package:flutter0322/forms/ManualTimeEntryPage.dart';
import 'package:flutter0322/forms/RacerEditForm.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/models.dart';
import 'package:flutter0322/research/MyColors.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/RaceConfig.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/widgets/InheritedMqttState.dart';
import 'package:flutter0322/widgets/RacerWidget.dart';
import 'package:flutter0322/widgets/TextDialogDemo.dart';
import 'package:flutter0322/widgets/ThemeSelectionWidget.dart';
import 'package:flutter0322/widgets/TimerChartWidget.dart';
import 'package:flutter0322/widgets/TimerLaneStatusWidget.dart';
import 'package:flutter0322/sentryClient.dart';

//import 'mqtt.dart';
//import 'package:pubsub/pubsub.dart';

void main() async {
  // This captures errors reported by the Flutter framework.
  FlutterError.onError = (FlutterErrorDetails details) async {
    if (isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Sentry.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };
  // This creates a [Zone] that contains the Flutter application and stablishes
  // an error handler that captures errors and reports them.
  //
  // Using a zone makes sure that as many errors as possible are captured,
  // including those thrown from [Timer]s, microtasks, I/O, and those forwarded
  // from the `FlutterError` handler.
  //
  // More about zones:
  //
  // - https://api.dartlang.org/stable/1.24.2/dart-async/Zone-class.html
  // - https://www.dartlang.org/articles/libraries/zones
  runZoned<Future<Null>>(() async {
    runAppLauncher();
  }, onError: (error, stackTrace) async {
    await sentryReportError(error, stackTrace);
  });
  //new MqttTest().mains();
  /*
  const oneSec = const Duration(seconds:5);
  new Timer.periodic(oneSec, handleTimeout);
  */
  //runApp(new RaceHistoryApp());
  // var racerMap=new TestData().getTestRacers();
}

void runAppLauncher() async {
  Map<int, Racer> racerMap = new Map();
  Widget homeWidget = null;
  MyColors restoredColor = await globals.globalDerby.myColors.loadSavedTheme();

  RaceConfig restoredRC = await RaceConfig.restoreRaceConfig();
  if (restoredRC != null) {
    globals.globalDerby = new globals.GlobalDerby(raceConfig: restoredRC);
    await globals.globalDerby.init(false);

    StaticShared.getShared().loginCredentials=await LoginCredentials.restoreLoginCredentials();
    homeWidget = getTRH();
  }

  if (StaticShared.getShared().raceConfig == null) {
    homeWidget = new SplashScreen();
  }

  runApp(wrapInheritedMqtt(wrapTheme(homeWidget, restoredColor)));
}

Widget wrapTheme(Widget homeWidget, MyColors restoredColor) {
  return new DynamicTheme(
      //defaultBrightness: Brightness.light,
      data: (brightness) => new ThemeData(
            primarySwatch: restoredColor.primarySwatch,
            brightness: brightness,
          ),
      themedWidgetBuilder: (context, theme) {
        return getMaterialApp(homeWidget: homeWidget, selectedTheme: theme);
      });
}

Widget wrapInheritedMqtt(Widget child) {
  return new MqttStateInherited(child: child, mqttPing: new MqttPing());
}

Widget getMaterialApp({Widget homeWidget, ThemeData selectedTheme}) {
  return new MaterialApp(
    title: 'SoapBox Derby',
    theme: selectedTheme,
    home: homeWidget,
    routes: <String, WidgetBuilder>{
      '/HomeScreen': (BuildContext context) =>
          new GenericDbListHome(new RacerDbModel()),
      "/Racers": (BuildContext context) =>
          new GenericDbListHome(new RacerDbModel()),
      "/RefreshLog": (BuildContext context) =>
          new GenericDbListHome(new RefreshLogDbModel()),
      "/Brackets": (BuildContext context) =>
          new GenericDbListHome(new BracketDbModel()),

      "/RacesTab": (BuildContext context) => getTRH(),
      "/TimerChart": (BuildContext context) =>
          new StackedAreaLineChart.withSampleData(),
      "/TimerLaneStatus": (BuildContext context) => new TimerHistoryPage(),
      "/TimerDetail": (BuildContext context) => new TimerSpsPage(),
      "/Preferences": (BuildContext context) => new PrefPage(),
      "/Themes": (BuildContext context) => new ThemeSelectionWidget(),
      "/PrefsForTimer": (BuildContext context) => new PrefsForTimer(),
      "/BirdhouseConfig": (BuildContext context) => new BirdHouseConfig(),
      "/RacerEditWidget": (BuildContext context) => new RacerEditWidget(),
      "/BracketEditWidget": (BuildContext context) => new BracketEditWidget(),
      "/BracketCarNumberWidget":(BuildContext context)=> new BracketCarNumberWidget(),
      "/ManualTimeEntry":(BuildContext context)=> new ManualTimeEntryPage(),
      "/BracketDetailTabsPage":(BuildContext context)=>new BracketDetailTabsPage(),
      //"/TimerHistory": (BuildContext context) => new TimerHistoryPage(),
    },
  );
}

void handleTimeout(Timer t) {
  // callback function
  print("timeoutfrom main: " +
      new DateTime.now().millisecondsSinceEpoch.toString());
  //Pubsub.publish('app.component.action', 1, 2, 3, keywords: 'work also', isnt: 'this fun');
}
