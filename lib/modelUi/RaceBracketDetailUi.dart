part of modelUi;

class RaceBracketDetailUi {
   RaceBracketDetail raceBracketDetail;
  final RaceBracket raceBracket;

  final Map<String,RaceStanding> pendingRaceStandingMap;
  RaceBracketDetailUi( this.raceBracket, this.pendingRaceStandingMap)
      {
    raceBracketDetail = RaceBracketDetail.fromJsonMap(jsonDecode(raceBracket.jsonDetail));
    raceBracketDetail.populateOriginKeys();
  }

  Map<String, List<DisplayableRace>> getDisplayableRaceByRound() {
    var rc = new Map<String, List<DisplayableRace>>();
    void iterateMapEntry(String key, HeatDetail heatDetail) {
      String round = heatDetail.raceDisposition.round;

      if (rc[round] == null) rc[round] = new List<HeatDetailUi>();
      rc[round].add(new HeatDetailUi(heatDetail, raceBracket,raceBracketDetail,pendingRaceStandingMap[heatDetail.heatKey]));
    }

    raceBracketDetail.heatDetailMap.forEach(iterateMapEntry);

    if (raceBracketDetail.places != null &&
        raceBracketDetail.places.length > 0) {
      rc["Places"] = new List<DisplayableRace>();
      for (String place in raceBracketDetail.places.keys) {
        print("Place:" + place);
        print("Place car:" + raceBracketDetail.places[place].toString());
        //rc["Places"].add(new DisplayablePlace(place: place,carNumber: 044));
        rc["Places"].add(new DisplayablePlace(
            place: place, carNumber: raceBracketDetail.places[place]));
      }
    }
    return rc;
  }
}

class DisplayablePlace implements DisplayableRace {
  final String place;
  final int carNumber;
  DisplayablePlace({this.carNumber, this.place}) : assert(carNumber != null);

  @override
  List<int> getCarNumbers({List<String> alternateText}) {
    return new List<int>()..add(this.carNumber);
  }

  @override
  RaceMetaData getRaceMetaData() {
    return new RaceMetaData(chartPosition: this.place);
  }

  static final RegExp regexNum = new RegExp(r"(\d+)");

  @override
  RaceStatus getRaceStatus() {
    if(this.carNumber!=null)
    return RaceStatus.Complete;
    else
      return RaceStatus.PendingRacers;
  }
  @override
  void getResultsSummary(ResultsSummary resultsSummary) {
    if (this.carNumber != null) {
      final Match placeIconText = regexNum.firstMatch(place);
      if (placeIconText != null) {
        resultsSummary.setIcon(
            this.carNumber,
            new Padding(
                padding: const EdgeInsets.all(16.0),
                child: new Text(placeIconText.group(0),
                    textScaleFactor: 4.0,
                    style: new TextStyle(fontWeight: FontWeight.bold))));
      }
    }
    return;
  }

  @override
  List<ContextClickHandler> getCarNumberClickHandlers() {
    return null;
  }
  @override
  HandleClick getDeleteAction() {
    return null;
  }
  @override
  HandleClick getEditAction() {
    return null;
  }

}

class HeatDetailUi implements DisplayableRace {
  static final RegExp regexSeed = new RegExp("Seed",caseSensitive: false);

  final HeatDetail heatDetail;
  final RaceBracket raceBracket;
  final RaceBracketDetail raceBracketDetail;
  final RaceStanding raceStanding;
  HeatDetailUi(HeatDetail this.heatDetail, RaceBracket this.raceBracket, RaceBracketDetail this.raceBracketDetail, this.raceStanding);


  @override
  void getResultsSummary(ResultsSummary resultsSummary) {
    if (heatDetail.winner != null) {
      return getCompletedResultsSummary(resultsSummary);
    } else {
      return getPendingResultsSummary(resultsSummary);
    }
  }
  @override
  RaceStatus getRaceStatus() {
    if(heatDetail.winner!=null){
      return RaceStatus.Complete;
    }
    if(heatDetail.carNumber1!=null && heatDetail.carNumber2!=null){
      return RaceStatus.PendingProgress;
    }

    if(regexSeed.hasMatch(heatDetail.originKeyCar1) || regexSeed.hasMatch(heatDetail.originKeyCar2)){
      return RaceStatus.PendingSeed;
    }
    return RaceStatus.PendingRacers;
  }
  void getPendingResultsSummary(ResultsSummary resultsSummary) {

    String origin1=heatDetail.originKeyCar1;
    String origin2=heatDetail.originKeyCar2;

    resultsSummary.addMessage(null, "$origin1 vs. $origin2");
    print("gprs: chartPos ${this.heatDetail.heatKey}");
    if(raceStanding!=null) {
      raceStanding.carNumbers.forEach((carNumber) {
        if (raceStanding.phase1DeltaMS == null) {
          resultsSummary.setIcon(carNumber, RaceResultWidget.getFixedAWidget());
          return; // exit lamda
        }
        if (raceStanding.phase2DeltaMS == null) {
          resultsSummary.setIcon(carNumber, RaceResultWidget.getFixedBWidget());
          return; // exit lambda
        }
      });
    }
  }

  void getCompletedResultsSummary(ResultsSummary resultsSummary) {
    resultsSummary.setIcon(
        heatDetail.winner, RaceResultWidget.getFinishFlagWidget());

    String roundDescription = raceBracketDetail
        .getRoundDescription(heatDetail.raceDisposition.winDest);

    resultsSummary.addMessage(heatDetail.winner, "To: $roundDescription");

    int loser;
    for (int carNumber in getCarNumbers()) {
      if (carNumber != heatDetail.winner) {
        loser = carNumber;
      }
    }
    if (loser != null) {
      String roundDescription = raceBracketDetail
          .getRoundDescription(heatDetail.raceDisposition.loseDest);

      resultsSummary.addMessage(loser, "To: $roundDescription");
    }
    return;
  }

  @override
  RaceMetaData getRaceMetaData() {
    return new RaceMetaData(
        chartPosition:
            heatDetail.raceDisposition.round + ":" + heatDetail.heatKey);
  }

  @override
  List<int> getCarNumbers({List<String> alternateText}) {
    List<int> hdCars = heatDetail.getCarNumbers();

    print ("heatDetail originKey1: ${heatDetail.originKeyCar1}");
    print ("heatDetail originKey2: ${heatDetail.originKeyCar2}");
    if (hdCars == null) {
      hdCars = [-2, -2];
    }
    if (hdCars != null) {
      if (hdCars[0] == null) hdCars[0] = -1;
      if (hdCars[1] == null) hdCars[1] = -2;
    }
    if(alternateText!=null) {


      if (hdCars[0] < 0) {
        alternateText[0] = heatDetail.originKeyCar1;
      }
      if (hdCars[1] < 0) {
        alternateText[1] = heatDetail.originKeyCar2;
      }
    }
    return hdCars;
    //return heatDetail.getCarNumbers();
  }

  @override
  List<ContextClickHandler> getCarNumberClickHandlers() {
    //TODO: only populate click handler when logged in as timer!
    List<ContextClickHandler> rc=[];
    var carNumbers=getCarNumbers();
    carNumbers.forEach((carNumber){
      rc.add((BuildContext context , DisplayableRace displayableRace){
          var rbd=raceBracketDetail;
        print("gcn clicked: ${rbd.raceTitle.toString()}");
        Navigator.of(context).pushNamed("/BracketCarNumberWidget", arguments: new BracketArg(raceBracket,rbd,carNumbers, carNumber, displayableRace));
      });
    });
    return rc;
  }
  @override
  HandleClick getDeleteAction() {
    return null;
  }
  @override
  HandleClick getEditAction() {
    return null;
  }
}
