part of modelUi;

class RacePhaseUi implements DisplayableRace {
  var tripleZero = new NumberFormat("000");

  final RacePhase racePhase;
  RacePhaseUi(RacePhase racePhase) : racePhase = racePhase;

  @override
  void getResultsSummary(ResultsSummary resultsSummary) {
    var sortedEntries = racePhase.getSortedRaceEntries();
    RaceEntry winner = sortedEntries[0];
    RaceEntry place2 = sortedEntries[1];
    if (winner.resultMS == null) {
      sortedEntries.forEach((raceEntry) {
        if (racePhase.getPhaseLetter() == "A") {
          resultsSummary.setIcon(
              raceEntry.carNumber, RaceResultWidget.getFixedAWidget());
        }
        if (racePhase.getPhaseLetter() == "B") {
          resultsSummary.setIcon(
              raceEntry.carNumber, RaceResultWidget.getFixedBWidget());
        }
      });
      return;
    }

    int winningMS = place2.resultMS - winner.resultMS;
    String phase = racePhase.getPhaseLetter();
    if (winningMS == 0) {
      resultsSummary.addMessage(winner.carNumber, "Phase $phase: Tied");
      resultsSummary.addMessage(place2.carNumber, "Phase $phase: Tied");
    } else {
      String formattedWinningMS = tripleZero.format(winningMS);
      resultsSummary.addMessage(
          winner.carNumber, "Phase $phase: ${formattedWinningMS}MS");
      resultsSummary.setIcon(
          winner.carNumber, RaceResultWidget.getFinishFlagWidget());
    }
  }

  @override
  List<int> getCarNumbers({List<String> alternateText}) {
    return racePhase.getCarNumbers();
  }

  @override
  RaceMetaData getRaceMetaData() {
    return racePhase.getRaceMetaData();
  }

  @override
  RaceStatus getRaceStatus() {
    return RaceStatus.Unknown;
  }

  @override
  List<ContextClickHandler> getCarNumberClickHandlers() {
    return null;
  }

  @override
  HandleClick getDeleteAction() {
    bool deletable = false;

    // starter can delete races in error, but not green(pending) races.
    if (racePhase.getRaceMetaData().phaseStatus == PhaseStatus.error &&
        StaticShared.getShared().loginCredentials.canAddRacePhase()) {
      deletable = true;
    }
    if (racePhase.getRaceMetaData().phaseStatus == PhaseStatus.pending &&
        StaticShared.getShared().loginCredentials.isRoleTimer()) {
      deletable = true;
    }
    if (deletable) {
      return (context) {
        new PostAddBlocks().postRmBlocks(racePhase.id);
        Scaffold.of(context).showSnackBar(SnackBar(content: Text("Deleted")));
      };
    }
    return null;
  }

  @override
  HandleClick getEditAction() {
    if (globalDerby.loginCredentials.isRoleTimer() &&
        this.getRaceMetaData().phaseStatus == PhaseStatus.pending) {
      return (context) {
        Navigator.of(context)
            .pushNamed("/ManualTimeEntry", arguments: racePhase);
      };
    } else {
      return null;
    }
  }
}
