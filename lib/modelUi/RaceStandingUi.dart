part of modelUi;

var tripleZero = new NumberFormat("000");

class RaceStandingUi implements DisplayableRace {
  final RaceStanding raceStanding;
  final HistoryType historyType;

  RaceStandingUi(this.raceStanding, {this.historyType});

  @override
  void getResultsSummary(ResultsSummary resultsSummary) {
    int overallWinningCar;
    if (raceStanding.phase1DeltaMS != null) {
      addResultTime(resultsSummary, "Phase A", raceStanding.phase1DeltaMS);
    }
    if (raceStanding.phase2DeltaMS != null) {
      addResultTime(resultsSummary, "Phase B", raceStanding.phase2DeltaMS);
      int overallMS = raceStanding.phase1DeltaMS + raceStanding.phase2DeltaMS;
      overallWinningCar = addResultTime(resultsSummary, "  Overall", overallMS);
    }

    if (overallWinningCar != null) {
      resultsSummary.setIcon(
          overallWinningCar, RaceResultWidget.getFinishFlagWidget());
    } else {
      raceStanding.carNumbers.forEach((carNumber) {
        if (raceStanding.phase1DeltaMS == null) {
          resultsSummary.setIcon(carNumber, RaceResultWidget.getFixedAWidget());
          return; // exit lamda
        }
        if (raceStanding.phase2DeltaMS == null) {
          resultsSummary.setIcon(carNumber, RaceResultWidget.getFixedBWidget());
          return; // exit lambda
        }
      });
    }
  }

  @override
  List<int> getCarNumbers({List<String> alternateText}) {
    return raceStanding.getCarNumbers();
  }

  @override
  RaceStatus getRaceStatus() {
    return RaceStatus.Unknown;
  }

  @override
  RaceMetaData getRaceMetaData() {
    return raceStanding.getRaceMetaData(
        bracketMap: globals.globalDerby.getRaceBrakcetCache());
  }

  int addResultTime(
      ResultsSummary resultsSummary, String phaseLiteral, int winningMS) {
    int car1 = raceStanding.getCarNumbers()[0];
    int car2 = raceStanding.getCarNumbers()[1];
    if (winningMS == 0) {
      resultsSummary.addMessage(car1, "$phaseLiteral: Tied");
      resultsSummary.addMessage(car2, "$phaseLiteral: Tied");
      return null;
    }
    int winningCar;
    if (winningMS < 0) {
      winningCar = car2;
    }
    if (winningMS > 0) {
      winningCar = car1;
    }
    String formattedMS = tripleZero.format(winningMS.abs());
    resultsSummary.addMessage(winningCar, "$phaseLiteral: ${formattedMS}MS");
    return winningCar;
  }

  @override
  List<ContextClickHandler> getCarNumberClickHandlers() {
    return null;
  }

  @override
  HandleClick getEditAction() {
    return null;
  }

  @override
  HandleClick getDeleteAction() {
    if (!globals.globalDerby.loginCredentials.canEditRacers()) {
      return null;
    }
    String voidUrl;

    if (historyType == HistoryType.Standing &&
        raceStanding.phase1DeltaMS != null) {
       voidUrl = "/voidRaceResults";
    }
    /* Delete for Race Standing for Pending races only (A-Phase) */
    if (historyType == HistoryType.Pending &&
        raceStanding.phase1DeltaMS == null &&
        raceStanding.phase2DeltaMS == null) {
      voidUrl = "/rmManualPending";
    }

    if (voidUrl == null) return null;    // delete not allowed.

    return (context) {
      Map<String, String> dataMap = {
        "id": raceStanding.id.toString(),
      };
      print("requesting delete via $voidUrl");

      new PostAddBlocks().getUrl(dataMap, voidUrl);
    };
  }
}
