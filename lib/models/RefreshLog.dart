// RefreshLog entries do NOT come from the server!  they are generated locally.dynamic
part of models;

class RefreshLog
    implements HasJsonMap, HasRelational {
  int beginMs=0;
  int networkCompleteMs=0;
  int dbCompleteMs=0;
  int httpStatus;
  String  fileName;
  String msg;
  bool  isDeleted;



  RefreshLog({isDeleted:false}){
    beginMs=DateTime.now().millisecondsSinceEpoch;
    msg="";
  }

  RefreshLog.fromSqlMap(Map jsonMap) {
    initFromMap(jsonMap);
  }

  RefreshLog.fromJsonMap(Map jsonMap) {
    initFromMap(jsonMap);
  }

  void initFromMap(Map jsonMap) {
    this.beginMs = jsonMap["beginMs"];
    this.networkCompleteMs = jsonMap["networkCompleteMs"];
    this.dbCompleteMs = jsonMap["dbCompleteMs"];
    this.httpStatus = jsonMap["httpStatus"];
    this.fileName = jsonMap["fileName"];
    this.msg = jsonMap["msg"];
    isDeleted=false;
  }

  @override
  Tuple2<String, List> generateSql() {
    return new Tuple2(insertSql, [
      //
      this.beginMs, //
      this.networkCompleteMs, //
      this.dbCompleteMs, //

      this.httpStatus, //
      this.fileName, //
      this.msg,
    ]);
  }

  @override
  Map toJson() {
    return {
      "beginMs": beginMs,
      "networkCompleteMs": networkCompleteMs,
      "dbCompleteMs": dbCompleteMs,
      "httpStatus": httpStatus,
      "fileName": fileName,
      "msg": msg,
    };
  }
  static String selectSql =
      "select * from RefreshLog  order by beginMs desc";
  static const String insertSql =
      "REPLACE INTO RefreshLog(beginMs, networkCompleteMs, dbCompleteMs,  "
      "  httpStatus, fileName, msg)    "
      "  VALUES(?,?,?,   ?,?,?)";
  static const String createSql =
      "CREATE TABLE RefreshLog(beginMs INTEGER PRIMARY KEY, networkCompleteMs Integer, dbCompleteMs integer,    "
      "httpStatus integer, fileName TEXT,msg TEXT) ";

  static String getCreateSql() {
    return createSql;
  }
  String toString(){

    int networkElapsed=networkCompleteMs - beginMs;
    int dbElapsed=dbCompleteMs - networkCompleteMs;

    String hms=new DateFormat.Hms().format( DateTime.fromMillisecondsSinceEpoch(beginMs));

    return "RefreshLog: ${toStringData()}";
  }

  String toStringData(){

    int networkElapsed=networkCompleteMs - beginMs;
    int dbElapsed=dbCompleteMs - networkCompleteMs;

    String hms=new DateFormat.Hms().format( DateTime.fromMillisecondsSinceEpoch(beginMs));

    return "$hms net ${networkElapsed} db: ${dbElapsed} rc: ${httpStatus}";
  }
  static String getSelectSql() {
    return selectSql;
  }

  void appendMsg(String s) {
    print("RefreshLog appendMsg: $s");
    msg="${this.msg} $s";
  }
}