part of models;
@JsonSerializable() //TODO: migrate to generated code.
class Racer implements HasJsonMap, HasRelational {
  final String racerName;
  final int carNumber;
  final String lastName;
  final int id;

  final bool isDeleted;
  @override
  int get hashCode {
    return carNumber.hashCode;
  }

  @override
  bool operator ==(dynamic other) {
    if (other is! Racer) return false;
    Racer racer = other;
    return carNumber == racer.carNumber;
  }

  @override
  Map toJson() {
    return {"racerName": racerName, "carNumber": carNumber, "id":id};
  }

  Racer({this.racerName, this.carNumber, this.lastName, this.isDeleted, this.id});

  Racer.fromJson(Map<String, dynamic> json)
      : carNumber = json["carNumber"],
        racerName =
            json["firstName"] != null ? json["firstName"] : json["racerName"],
        lastName = json["lastName"],
        id=json["id"],
        isDeleted = parseIsDeleted(json["isDeleted"]) {
    print(
        "NEW racer: ${this.racerName} $isDeleted number: $carNumber lastName: $lastName id: $id");
  }

  static String selectSql =
      "select * from Racer where isDeleted=0 {sqlFilter} order by carNumber";
  static const String insertSql =
      "REPLACE INTO Racer(carNumber, racerName, lastName,   id, isDeleted) VALUES(?,?,?, ?,?)";
  static const String createSql =
      "CREATE TABLE Racer(carNumber INTEGER PRIMARY KEY, racerName TEXT, lastName TEXT, id INTEGER, isDeleted INTEGER) ";

  Tuple2<String, List<dynamic>> generateSql() {
    return new Tuple2(insertSql, [
      this.carNumber,
      this.racerName,
      this.lastName,
      this.id,
      ModelFactory.boolAsInt(isDeleted)
    ]);
  }

  static String getCreateSql() {
    return createSql;
  }


  static String getSelectSql({String carFilter, bool exactMatch=false}) {
    //return selectSql;
    String sqlFilter=transformFiltertoSql(carFilter, exactMatch);

    print ("racer sqlFilter: $sqlFilter");
    return selectSql.replaceAll("{sqlFilter}", sqlFilter);

  }
  static String transformFiltertoSql(String carFilter, bool exactMatch){
    if(carFilter==null || carFilter==""){
      return exactMatch?" and 0=1 ":"";
    }
    final RegExp re=new RegExp("^${carFilter}\$");
    if(re.hasMatch(carFilter)){
      if(carFilter.length==1 && !exactMatch){
        carFilter="\"$carFilter%\"";
      }
      else{
        carFilter="\"$carFilter\"";
      }
      return (" and   cast(carNumber as text) like $carFilter  ");
    }

    return exactMatch?" and 0=1 ":"";
  }
}

