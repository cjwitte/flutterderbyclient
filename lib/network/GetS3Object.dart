import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:flutter0322/autoTimer/SimplePinSummary.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/models.dart';
import 'package:flutter0322/models/ModelFactory.dart';
import 'package:flutter0322/network/Md5Utils.dart';
import 'package:flutter0322/shared/RaceConfig.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:path_provider/path_provider.dart';
import 'package:xml/xml.dart' as xml;

class GetS3Utils {
  static Future<Map<String, String>> getS3BucketList(String targetUrl) async {
    var httpClient = new HttpClient();
    var uri = new Uri().resolve(targetUrl);
    var request = await httpClient.getUrl(uri);
    var response = await request.close();
    var responseBody = await response.transform(new Utf8Decoder()).join();
    print("response: " + responseBody);
    var document = xml.parse(responseBody);
    print("response document: " + document.toString());
    for (var child in document.lastChild.children) {
      print("response child: " + child.toString());
    }
    var reverse = (String a, String b) => b.compareTo(a);

    var rc = new SplayTreeMap<String, String>(reverse);
    for (var key in document.findAllElements("Key")) {
      print("response key: " + key.toString());
      var keyString = key.descendants
          .where((node) => node is xml.XmlText && !node.text.trim().isEmpty)
          .join("");
      print("response file/bucket: " + keyString);
      rc[keyString] = "${targetUrl}/${keyString}";
    }
    return rc;
  }

  static Future<String> getS3ObjectAsString(String target) async {
    print("Downloading object: ${target}");
    var httpClient = new HttpClient();
    var uri = new Uri().resolve(target);
    var request = await httpClient.getUrl(uri);
    var response = await request.close();
    var responseBody = await response.transform(new Utf8Decoder()).join();
    // print("gso response: " + responseBody);

    return responseBody;
  }
}

typedef Future FileProcessor(File fileName);

class SyncS3DataLog extends SyncS3AppendingFile {
  @override
  String get baseFileName => "dataLog";
  @override
  FileProcessor get fileProcessor => RefreshData.parseAndLoadFile;
}

class SyncS3TimerLog extends SyncS3AppendingFile {
  @override
  String get baseFileName => "fdmWithCarNumbers";
  @override
  FileProcessor get fileProcessor => parseTimerFile;
  Future noProcessor(File name) {}
  final List<FinishDetailMap> finishDetailMapList = [];

  Future parseTimerFile(File ndjson) async {
    Stream<List<int>> inputStream = ndjson.openRead();
    int accumulatedBytes = 0;

    finishDetailMapList.clear();
    Stream<String> lineStream = await inputStream
        .transform(new Utf8Decoder())
        .transform(new LineSplitter());
    await republishTimerStream(lineStream);

    //new Md5Utils().calcSha1(ndjson);
  }

  Future<int> republishTimerStream(Stream<String> stream) async {
    int t0 = DateTime.now().millisecondsSinceEpoch;
    bool defer = true;

    await for (var line in stream) {
      // protects against s3 xml reply And seek to middle of json
      if (line.startsWith("{")) {
        finishDetailMapList.add(
            new FinishDetailMap.fromJson(json.decode(line)));
      }
    }
    ;

    // globals.globalDerby.derbyDb.flushPendingBatch();
    int t1 = DateTime.now().millisecondsSinceEpoch;
    int elapsed = t1 - t0;
    print(
        "SyncS3TimerLog republishStream: defer: $defer elapsed: $elapsed array Size: ${finishDetailMapList.length}");

    return 0;
  }
}

class SyncS3PinChangeLog extends SyncS3AppendingFile {
  @override
  String get baseFileName => "pinChange";
  @override
  FileProcessor get fileProcessor => parsePinChangeFile;
  Future noProcessor(File name) {}
  final List<PinChange> pinChangeList = [];
  List<SimplePinSummary> spsList = [];

  Future parsePinChangeFile(File ndjson) async {
    int jlen =
        await SyncS3AppendingFile.getFileLength(ndjson, "parsePinChangeFile");

    const tailSize = 10000;
    int start = (jlen > tailSize) ? jlen - tailSize : 0;
    Stream<List<int>> inputStream = ndjson.openRead(start);

    int accumulatedBytes = 0;

    pinChangeList.clear();
    Stream<String> lineStream = await inputStream
        .transform(new Utf8Decoder())
        .transform(new LineSplitter());
    await republishPinChangeStream(lineStream);
    spsList = SimplePinSummary.transform(this.pinChangeList);
  }

  Future<int> republishPinChangeStream(Stream<String> stream) async {
    int t0 = DateTime.now().millisecondsSinceEpoch;
    bool defer = true;

    await for (var line in stream) {
      // protects against s3 xml reply And seek to middle of json
      if (line.startsWith("{")) {
        pinChangeList.add(new PinChange.fromJson(json.decode(line)));
      }
    }
    ;

    // globals.globalDerby.derbyDb.flushPendingBatch();
    int t1 = DateTime.now().millisecondsSinceEpoch;
    int elapsed = t1 - t0;
    print(
        "SyncS3PinChangeLog republishStream: defer: $defer elapsed: $elapsed array Size: ${pinChangeList.length}");

    return 0;
  }
}

abstract class SyncS3AppendingFile {
  FileProcessor get fileProcessor;
  String get baseFileName;
  String get baseFileSuffix => ".ndjson";
  Future<File> _tmpNdJson() async {
//var ms = new DateTime.now().millisecondsSinceEpoch.toString();
    var randString = new Random().nextInt(100000000).toString();
//return _localFile(fileName: "dataLog_${randString}.ndjson");
    return StaticShared.getShared()
        .localPersistenceProvider
        .localFile(fileName: "${baseFileName}_${randString}.${baseFileSuffix}");
  }

  @override
  Future<File> ndJsonFile() async {
//return _localFile(fileName: "dataLog.ndjson");
    return StaticShared.getShared()
        .localPersistenceProvider
        .localFile(fileName: "${baseFileName}${baseFileSuffix}");
  }

  String get remoteUrl {
    var raceConfig = StaticShared.getShared().raceConfig;
    String url =
        "${raceConfig.s3BucketUrlPrefix}/${baseFileName}${baseFileSuffix}";
    return url;
  }

  Future<File> resumeS3ObjectAsFile(RefreshLog refreshLog) async {
    final String target = this.remoteUrl;
    final File mainFileName = await this.ndJsonFile();

    print("Resume Download consider ${mainFileName.path}");
    print("Resume Download from ${target}");

    int resumeFrom = await getFileLength(mainFileName, "_resume begin");

    if (resumeFrom == 0) {
      print("Resume Download delegating ${mainFileName.path}");

      await getS3ObjectAsFile(target, mainFileName);
      refreshLog.networkCompleteMs = DateTime.now().millisecondsSinceEpoch;
      refreshLog.httpStatus =
          -200; // assume 200 (negative intended to indicate reduced confidence)

      await fileProcessor(mainFileName);

      refreshLog.dbCompleteMs = DateTime.now().millisecondsSinceEpoch;

      return mainFileName;
    }
    //var sink1 = fName.openWrite(mode: FileMode.WRITE_ONLY_APPEND);
    print("Resume Download from offset ${resumeFrom}");

    HttpClientResponse response = await new HttpClient()
        .getUrl(Uri.parse(target))
        .then((HttpClientRequest request) {
      request.headers..add(HttpHeaders.rangeHeader, "bytes=${resumeFrom}-");
      return request;
    }).then((HttpClientRequest request) => request.close());

    print("http response status: ${response.statusCode}");
    refreshLog.networkCompleteMs = DateTime.now().millisecondsSinceEpoch;
    refreshLog.httpStatus = response.statusCode;

    if (response.statusCode == 416) {
      print("_resumeS3ObjectAsFile: NOOP");

      await auditSha1Header(response, mainFileName);
      refreshLog.dbCompleteMs = DateTime.now().millisecondsSinceEpoch;

      //Digest calcSha=await new Md5Utils().calcSha1(mainFileName);
      //print ("416 localsha1: ${calcSha.toString()} serverSha1: ${serverSha1}");
      return mainFileName;
    }

    if (response.statusCode == 206) {
      var myTmpFile = await _tmpNdJson();
      print("_resumeS3ObjectAsFile: resuming with partial download");

      //IOSink sinkDelta = mainFileName.openWrite(mode: FileMode.WRITE_ONLY_APPEND);
      IOSink sinkDelta = myTmpFile.openWrite();
      await response.pipe(sinkDelta);

      await fileProcessor(myTmpFile);
      await concatenate(appendee: mainFileName, appendage: myTmpFile);
      await myTmpFile.delete();
      refreshLog.dbCompleteMs = DateTime.now().millisecondsSinceEpoch;

      await auditSha1Header(response, mainFileName);
    }
    await getFileLength(mainFileName, "_resume Done");

    return mainFileName;
  }

  Future concatenate({File appendee, File appendage}) async {
    IOSink sinkDelta = appendee.openWrite(mode: FileMode.writeOnlyAppend);
    Stream<List<int>> inputStream = appendage.openRead();

    await inputStream.pipe(sinkDelta);
  }

  Future<File> getS3ObjectAsFile(String target, File fname) async {
    print("Download from ${target} begin ${fname.path}");

    FileStat fstat = await FileStat.stat(fname.path);
    print("Length of file before ${fname} is ${fstat.size}");
    //var sink1 = fname.openWrite();
    var httpStatus = -1;
    HttpClientResponse response = await new HttpClient()
        .getUrl(Uri.parse(target))
        .then((HttpClientRequest request) {
      request.headers..add(HttpHeaders.CONTENT_TYPE, "text/plain");
      return request;
    }).then((HttpClientRequest request) => request.close());

    response.headers.forEach((String name, List<String> vals) {
      print("response hdr fromfile $name $vals");
    });

    await response.pipe(fname.openWrite());

    //await sink1.close();
    print("Download complete ${fname.path} status: $httpStatus");
    FileStat fstat2 = await FileStat.stat(fname.path);
    print("Length of file after ${fname} is ${fstat2.size}");
    if (fstat2.size < 500) {
      print("file content: " + fname.readAsStringSync());
    }
    /*
    if(httpStatus==-1 && fstat2.size<1000){
      // amazon will return an xml error document.  get rid of it.
      // test condition is missing datalog.ndjson on new server build.
      await fname.writeAsStringSync("");
    }
    */
/*
    if (target.endsWith(".gz")) {
      print("Gzip decode begin s ${localBasename}");

      File fname2 = await _localFile(prefix: localBasename + ".uz");
      var sink2 = fname2.openWrite();
      await new GZipDecoder().decodeStream(fname.openRead(), sink2);
      await sink2.close();
      return fname2;
    }
    */
    return fname;
  }

  static Future<int> getFileLength(File fName, String s) async {
    FileStat fstat = await FileStat.stat(fName.path);
    if (fstat.type == FileSystemEntityType.NOT_FOUND) {
      print("$s getFileLength  FileNotFound: ${fName.path}");
      return 0;
    } else {
      if (s != null) {
        print("$s getFileLength Length of file ${fName} is ${fstat.size}");
      }
      return fstat.size;
    }
  }

  Future<bool> auditSha1Header(
      HttpClientResponse response, File localFileName) async {
    var serverSha1 = null;
    response.headers.forEach((String name, List<String> vals) {
      print("auditSha1: response hdr $name $vals");
      if ("x-amz-meta-sha1" == name) {
        serverSha1 = vals[0];
      }
    });

    if (serverSha1 != null) {
      await auditSha1String(
          response.statusCode.toString(), serverSha1, localFileName);
    } else {
      print("auditSha1: ${response.statusCode} no sha1 header");
    }
    return false;
  }

  Future<bool> auditSha1String(
      String caller, String serverSha1, File localFileName) async {
    Digest calcSha = await new Md5Utils().calcSha1(localFileName);
    int ndjLength = localFileName.lengthSync();

    print(
        "auditSha1: ${caller} len: $ndjLength localsha1: ${calcSha.toString()} serverSha1: ${serverSha1}");
    if (serverSha1 != calcSha.toString()) {
      print("auditSha1: $caller ERROR: NDJSON FILE IS CORRUPT! $ndjLength");
      //TODO: re-download entire file and restart!??!
      return false;
    } else {
      print("auditSha1: $caller INFO: NDJSON FILE is good! $ndjLength");
      return true;
    }
  }

  //TODO: copy/pasted from s3 append. consolidate?
  Future _appendMqttBlock(
      ndjsonMap, RefreshLog refreshLog, File mainFileName) async {
    var myTmpFile = await _tmpNdJson();
    print("_appendMqttBlock: begin");

    //IOSink sinkDelta = mainFileName.openWrite(mode: FileMode.WRITE_ONLY_APPEND);
    IOSink sinkDelta = myTmpFile.openWrite();
    ;
    var BASE64 = const Base64Codec();
    int expectedJsonLength = ndjsonMap["dataLength"];
    var bytesInLatin1_decoded = BASE64.decode(ndjsonMap["data64"]);
    var LatinD = const Latin1Decoder();
    String json = LatinD.convert(bytesInLatin1_decoded);
    print("decoded data64: $bytesInLatin1_decoded");
    print("decoded data64: ${json.length} exp: ${expectedJsonLength} $json");
    sinkDelta.write(json);
    await sinkDelta.close();

    await fileProcessor(myTmpFile);
    await concatenate(appendee: mainFileName, appendage: myTmpFile);
    await myTmpFile.delete();
    refreshLog.dbCompleteMs = DateTime.now().millisecondsSinceEpoch;

    await auditSha1String("appendMqttBlock", ndjsonMap["sha1"], mainFileName);
  }
}

class RefreshData {
  RefreshData();
  Future<RefreshLog> doRefresh<T>(
      {RaceConfig raceConfig, dynamic ndjsonMap}) async {
    SyncS3DataLog syncS3DataLog = new SyncS3DataLog();

    RefreshLog refreshLog = new RefreshLog();

    File ndjsonFile = await syncS3DataLog.ndJsonFile();

    if (ndjsonMap != null) {
      int ndjLength = ndjsonFile.lengthSync();
      int ndjExpected = ndjsonMap["beginOffset"];
      refreshLog.httpStatus = 555;
      refreshLog.beginMs = DateTime.now().millisecondsSinceEpoch;
      refreshLog.networkCompleteMs = DateTime.now().millisecondsSinceEpoch;
      if (ndjLength == ndjExpected) {
        refreshLog.appendMsg("mqttSwitch GOOD bypass S3! $ndjLength");
        await syncS3DataLog._appendMqttBlock(ndjsonMap, refreshLog, ndjsonFile);
        return refreshLog;
      }
      if (ndjLength < ndjExpected) {
        refreshLog.appendMsg(
            "mqttSwitch underflow! file ${ndjLength} map: ${ndjExpected}");
        //fall thru to s3
      } else {
        refreshLog.appendMsg(
            "mqttSwitch overflow! file ${ndjLength} map: ${ndjExpected}");
        refreshLog.dbCompleteMs = DateTime.now().millisecondsSinceEpoch;

        return refreshLog; // s3 can't help with this...
      }
    }
    ndjsonFile = await syncS3DataLog.resumeS3ObjectAsFile(refreshLog);

    return refreshLog;

    try {
      //dumpArchive();
    } catch (e) {
      print("dumpArchive: fail $e");
    }
    //return;
  }

  dumpArchive() async {
    print("dumpArchive: BEGIN!");

    var sqlRows = await globals.globalDerby.derbyDb?.database
        ?.rawQuery(RefreshLog.getSelectSql());

    print("dumpArchive: rows: ${sqlRows.length}");

    for (var row in sqlRows) {
      RefreshLog r = RefreshLog.fromSqlMap(row);
      print("dumpArchive: ${r.toString()}");
    }
  }

  static Future parseAndLoadFile(File ndjson) async {
    Stream<List<int>> inputStream = ndjson.openRead();
    int accumulatedBytes = 0;

    Stream<String> lineStream = await inputStream
        .transform(new Utf8Decoder())
        .transform(new LineSplitter());
    await republishStream(lineStream);

    //new Md5Utils().calcSha1(ndjson);
  }

  static Future<int> republishStream(Stream<String> stream) async {
    int t0 = DateTime.now().millisecondsSinceEpoch;
    bool defer = true;

    try {
      await for (var line in stream) {
        await ModelFactory.loadDb(line, defer: defer);
      }
      globals.globalDerby.derbyDb.flushPendingBatch();
    } catch (e) {
      print("republishStream: error: $e type: ${e.runtimeType}");
      RangeError re = e;
      print("stack: ${re.stackTrace}");

      return -1;
    }
    int t1 = DateTime.now().millisecondsSinceEpoch;
    int elapsed = t1 - t0;
    print("republishStream: defer: $defer elapsed: $elapsed");

    return 0;
  }
}
