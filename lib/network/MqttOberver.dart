import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/shared/TImerConfig.dart';
import 'package:mqtt_client/mqtt_client.dart' as Mqtt;
import 'package:mqtt_client/mqtt_client.dart';
import 'package:uuid/uuid.dart';

enum MqttState { PENDING, CONNECTED, DISCONNECTED, ERROR }

class MqttStateLog {
  MqttStateLog({mqttState: MqttState.PENDING}) {
    setMqttState(mqttState);
    lastChange = 0;
  }
  MqttState mqttState;
  int lastChange;

  bool shouldAttemptConnect() {
    if (mqttState == MqttState.CONNECTED) {
      return false;
    }

    if (mqttState == MqttState.DISCONNECTED || mqttState == MqttState.ERROR) {
      return true;
    }

    // retry connect can proceed if sufficient elapsed time since prior attempt.
    int elapsedMS=  DateTime.now().millisecondsSinceEpoch-lastChange;
    print ("MqttObserver sac last: $elapsedMS");

    return(elapsedMS>10000);
  }

  bool setMqttState(MqttState mqttState) {
    this.mqttState = mqttState;
    this.lastChange = DateTime.now().millisecondsSinceEpoch;
  }

  Duration age() {
    print("observerAge: $lastChange");
    //return -5;
    int ms = DateTime.now().millisecondsSinceEpoch - lastChange;
    return Duration(milliseconds: ms);
  }

  String formattedAge() {
    //DateTime date =
    //new DateTime.fromMillisecondsSinceEpoch(this.age(), isUtc: true)
    //  .toLocal();
    //return new DateFormat.Hms().format(date);
    Duration age = this.age();
    if (age.inSeconds < 60) {
      return this.age().inSeconds.toString() + " seconds";
    }
    if (age.inMinutes < 60) {
      return this.age().inMinutes.toString() + " minutes";
    }
    return this.age().inHours.toString() + " hours";
  }

  String toString() {
    return "${mqttState}\n${formattedAge()}";
  }
}

class MqttObserver {
  String hostname;
  String user;
  String password;
  static const String ndjsonTopic = "racesNdjson"; // Not a wildcard topic
  static const String chimeTopic = "chime"; // Not a wildcard topic
  Mqtt.MqttClient client;
  int priorMessageOffset = -1;

  bool userEnabled=false;

  MqttStateLog mqttStateLog = new MqttStateLog();

  static const int KeepAlive = 75;


  MqttObserver(this.hostname, {this.user, this.password, this.userEnabled=false}) {

    print ("MqttObserver rebuilt: $hostname enabled: $userEnabled");
  }
  MqttConnectMessage getConnectMessage(){

     final String mqttClientId =
        "dart." + new Random().nextInt(100000000).toString();
     final deviceModel=globals.globalDerby.deviceModel;
     final String willMessage="{\"lastWillDevice\": \"$mqttClientId\", \"device\":\"$deviceModel\"}";
     print ("willMessage: $willMessage");
     final MqttConnectMessage connMess = new MqttConnectMessage()
        .withClientIdentifier(mqttClientId)
        .keepAliveFor(
        KeepAlive) // Must agree with the keep alive set above or not set
        .withWillTopic(
        "client/willtopic") // If you set this you must set a will message
        .withWillMessage(willMessage)
        .withWillQos(MqttQos.atLeastOnce);
     if (user != null && password != null) {
       user = user
           .toLowerCase(); // TODO: init on server is foobar.   email is uppercase, mqtt timer user is lowercase!
       connMess.authenticateAs(user, password);
     }
     connMess.startClean();
    return connMess;
  }

  /// The unsolicited disconnect callback
  void onMqttDisconnected() {
    print(
        "MqttObserver: OnDisconnected client callback - Client disconnection");
    setMqttState(MqttState.DISCONNECTED);
  }

  MqttStateLog getMqttStateLog() {
    return mqttStateLog;
  }

  Future init() async {
    if(!userEnabled) return;
    var uuid = new Uuid();
    var mqClientId = "flutter." + uuid.v4();
    client = new Mqtt.MqttClient(hostname, mqClientId);
    client.logging(on: true);
    priorMessageOffset = -1;

    /// Add the unsolicited disconnection
    /// callback
    client.onDisconnected = this.onMqttDisconnected;
    client.keepAlivePeriod = KeepAlive;

    client.connectionMessage = this.getConnectMessage();
    try {
      print("MqttObserver about to connect 747: $hostname");
      //print("MqttObserver about to connect 749: $mqttClientId");
      print("MqttObserver about to connect user: $user");

      await client.connect();
    } on Exception catch (e) {
      print("MqttObserver: Mqtt connect error: $e");
      setMqttState(MqttState.ERROR);
      //client.disconnect();  // this throws stacktrace!
      return;
    }
    print(
        "MqttObserver: Mqtt connect wait completed: ${client.connectionStatus}");

    for (int x = 0; x < 10; x++) {
      print(
          "MqttObserver::$hostname client test take: ${x} status:  ${client.connectionStatus}");
      if (client.connectionStatus.state == MqttConnectionState.connecting) {
        print(
            "MqttObserver::$hostname client left intact: ${client.connectionStatus}");
      } else {
        break;
      }
      await Future.delayed(new Duration(seconds: 2));
    }
    if (client.connectionStatus.state == MqttConnectionState.connected) {
      setMqttState(MqttState.CONNECTED);
    } else {
      print(
          "MqttObserver::ERROR $hostname client connection failed - disconnecting, state is ${client.connectionStatus.state}");
      //client.disconnect();  this throws an exception!
      setMqttState(MqttState.ERROR);

      return;
    }

    final String pubTopic = "Dart/Mqtt_client/testtopic";

    /// Ok, lets try a subscription

    print(
        "MqttObserver substatus: ${client.getSubscriptionsStatus(ndjsonTopic)}");

    client.subscribe(ndjsonTopic, MqttQos.exactlyOnce);
    client.subscribe(chimeTopic, MqttQos.exactlyOnce);
    client.subscribe(TimerConfig.desiredTimerConfigTopic, MqttQos.exactlyOnce);
    final String laneTopic = "derby/+/rpi/#";  // derby/{TIMER_HOSTNAME/rpi/{lanePin}
    client.subscribe(laneTopic, MqttQos.exactlyOnce);

    /// Subscribe to it
    //client.subscribe(pubTopic, MqttQos.exactlyOnce);

    //final observe.ChangeNotifier<observe.ChangeRecord> cn = client.subscribe(topic, Mqtt.MqttQos.exactlyOnce).observable;
    client.updates.listen((List<MqttReceivedMessage> c) {
      c.forEach((message){
        dispatchReceivedMessage(message);
      });


    });

    if (false) {
      print("MqttObserver::Publishing begin");

      /// Sleep to read the log.....
      await MqttUtilities.asyncSleep(5);

      /// Lets publish to our topic, use a high QOS

      // Use the payload builder rather than a raw buffer
      print("MqttObserver::Publishing our topic");
      final MqttClientPayloadBuilder builder = new MqttClientPayloadBuilder();
      builder.addString("Hello from mqtt_client");
      client.publishMessage(pubTopic, MqttQos.exactlyOnce, builder.payload);
    }
  }

  void refreshConnectionStatus() {
    print("MqttObserver: refreshConnectionStatus  ${mqttStateLog} enabled: $userEnabled");
    if(!userEnabled){
      return;
    }
    if (!mqttStateLog.shouldAttemptConnect()) {
      return;
    }

    print("MqttObserver: refreshConnectionStatus Re-running init");
    init();
  }

  void setMqttState(MqttState mqttState) {
    if (this.mqttStateLog.mqttState != mqttState) {
      this.mqttStateLog.setMqttState(mqttState);

      // notify widget(s) of change
      globals.globalDerby.mqttStateLogStreamController.add(this.mqttStateLog);
    }
  }

  void dispatchReceivedMessage(Mqtt.MqttReceivedMessage myMessage) {
    //Mqtt.MqttReceivedMessage myMessage = c[0] as Mqtt.MqttReceivedMessage;
    final Mqtt.MqttPublishMessage recMess =
    myMessage.payload as Mqtt.MqttPublishMessage;
    final String pt = Mqtt.MqttPublishPayload.bytesToStringAsString(
        recMess.payload.message);
    print(
        "MqttObserver::Change notification:: topic is <${myMessage.topic}> payload is <$pt> ");
    if (myMessage.topic == TimerConfig.desiredTimerConfigTopic) {
      var jsonMap = jsonDecode(pt);

      StaticShared.getShared().timerConfig = TimerConfig.fromJsonMap(jsonMap);
      print(
          "MqttObserver::Change notification:: installed new TimerConfig: $jsonMap");
      return;
    }
    if (myMessage.topic == ndjsonTopic) {
      var jsonMap = jsonDecode(pt);

      if (priorMessageOffset == jsonMap["beginOffset"]) {
        print(
            "MqttObserver::Change notification suppressing duplicate $priorMessageOffset");
      } else {
        print("MqttObserver::Change notification refreshing.");
        priorMessageOffset = jsonMap["beginOffset"];

        globals.globalDerby.refreshStatus
            .doRefresh(ndjson: jsonMap, caller: "mqtt");
      }
      return;
    }
    //TODO: parse the topic and publish to a map (allow multiple concurrent timer sources)!
    if(myMessage.topic.startsWith("derby/")){
      print("MqttObserver::LaneStatus $pt");
      var jsonMap = jsonDecode(pt);
      var pc=PinChange.fromJson(jsonMap);
      print("MqttObserver::LaneStatus/PinChange ${pc.toString()}");

      globals.globalDerby.timerLaneStatusModel.refreshPinChange(pc);
    }
  }

  void setUserEnabled(bool enableMqtt) {
    this.userEnabled=enableMqtt;
    if (!enableMqtt){
      if(this.mqttStateLog.mqttState == MqttState.CONNECTED){
        client.disconnect();

      }
    }
      // caller should re-instantiate to enable
      //refreshConnectionStatus();
  }
}
