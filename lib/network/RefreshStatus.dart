import 'dart:async';

import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/models.dart';
import 'package:flutter0322/network/GetS3Object.dart';
import 'package:flutter0322/shared/RaceConfig.dart';
import 'package:semaphore/semaphore.dart';
class RefreshStatus{
  static final int  maxCount = 1;
  var running = 0;
  final int  simultaneous = 0;
  final LocalSemaphore sem;
  RefreshStatus():sem= new LocalSemaphore(maxCount)
  {

  }


  Future<Null> doRefresh({RaceConfig raceConfig, dynamic ndjson, String caller:"unknown"})async{
    print("doRefresh0 $caller raceConfig: $raceConfig");
    int t0=DateTime.now().millisecondsSinceEpoch;


    await sem.acquire();

    int t1=DateTime.now().millisecondsSinceEpoch;
    int waitTime1=t1-t0;
    print("doRefresh1 $waitTime1  BEGIN");
    running++;
    RefreshLog refreshLog;
    try {
      refreshLog=await new RefreshData().doRefresh(raceConfig: raceConfig, ndjsonMap: ndjson);
    }
    catch( e){
      print("error ${e}, proceeding with semaphore unlock.");
      refreshLog.appendMsg("error: ${e}");
    }

    try {
      await globals.globalDerby.derbyDb.addNewModel(refreshLog, defer: false);
    }
    catch(e){
      print("error ${e}, failed to insert refreshLog.");

    }
    running--;
    sem.release();
    int t2=DateTime.now().millisecondsSinceEpoch;
    int waitTime2=t2-t0;

    print("doRefresh2: $waitTime2");

    globals.globalDerby.mqttObserver.refreshConnectionStatus();

    return;
  }
  bool isRefreshInProgress(){
    return( running>0);
  }
}