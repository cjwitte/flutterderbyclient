import 'dart:io';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter0322/globals.dart';
import 'package:flutter0322/network/GetS3Object.dart';
import 'package:flutter0322/shared/StaticShared.dart';

class MyColors{

  static List<MaterialColor> allowedColors  =[];

  MyColors(this.primarySwatch){
    purgeUnwantedColors();  //TODO: would prefer this to be static!
  }

  IconData listIcon=Icons.brightness_1;
  Color errorBg=Colors.red[800];
  MaterialColor primarySwatch;
  //Color primarySwatch=Colors.indigo;
  Color stripeColor(int index, BuildContext context) {
    return index % 2 == 1 ? Theme.of(context).backgroundColor: null;

  }

  /*
  switchBrightness(BuildContext context){
    DynamicTheme.of(context).setBrightness(Theme.of(context).brightness == Brightness.dark? Brightness.light: Brightness.dark);

  }
  */
  switchTheme(BuildContext context){
    Color current=Theme.of(context).primaryColor;

    int currentIdx=allowedColors.indexWhere((c) => c==current);

    int nextIdx=currentIdx+1;
    if(nextIdx >= allowedColors.length){
      nextIdx=0;
    }
    print("Theme change colors from: $current [$currentIdx] to [$nextIdx]");


    applyColorTheme(nextIdx, context);

    saveCurrentTheme(primaryColor: Theme.of(context).primaryColor);

  }


  /*
  void restoreSavedTheme(BuildContext context) async {
    int savedIndex= await loadCurrentTheme();
    print ("restoreSavedTheme: $savedIndex");
    applyColorTheme(savedIndex, context);
  }
  */
  void applyColorTheme(int nextIdx, BuildContext context, ){
    new MyColors(allowedColors[nextIdx]).applyColorThemeFromMyColor( context);

  }

  void applyColorThemeFromMyColor( BuildContext context){
    globalDerby.myColors=this;
    ThemeData ntd=new ThemeData(
      primaryColor: globalDerby.myColors.primarySwatch,
      backgroundColor: globalDerby.myColors.primarySwatch[200],
    );
    DynamicTheme.of(context).setThemeData(ntd);
    print("Theme background: ${ntd.backgroundColor}");
    saveCurrentTheme(primaryColor:this.primarySwatch);
  }

  void saveCurrentTheme({MaterialColor primaryColor}) async {

    File tpFile=await getThemePreferenceFile();
    if(primaryColor==null){

    }
    await tpFile.writeAsString(primaryColor.value.toString());
    print( "SaveCurrentTheme: saved ${primaryColor.value.toRadixString(16)} to $tpFile");

    //loadSavedTheme(dryRun: true);
  }
  Future<MyColors>  loadSavedTheme({dryRun: false}) async{
    File tpFile=await getThemePreferenceFile();
    int currentIdx=0;
    try {
      String savedPref = await tpFile.readAsString();
      print("loadCurrentTheme: got  ${savedPref}");
      int savePrefInt = int.tryParse(savedPref) ?? -1;

      currentIdx = getIndexFromRGB(savePrefInt);
      print("loadCurrentTheme: got  ${currentIdx}");
    }
    catch(e){
      print("loadCurrentTheme: failed:  ${e}");
      currentIdx=getIndexFromRGB(Colors.blueGrey.value);
      print("loadCurrentTheme: defaults to :  ${currentIdx}");

    }
    if(currentIdx < 0){
      currentIdx=0;
    }
    if(! dryRun) {
      globalDerby.myColors = new MyColors(allowedColors[currentIdx]);
    }
    return globalDerby.myColors;
  }

  int getIndexFromRGB(int rgbInt){
    return allowedColors.indexWhere((c) => c.value == rgbInt);
  }
  // ignore: missing_method_parameters
  Future<File> getThemePreferenceFile()  async {
    final path=await StaticShared.getShared().localPersistenceProvider.getTopPersistenceDir();

    return  File('$path/theme.txt');


  }

  void purgeUnwantedColors() {
    if(allowedColors.length>0) return;

    Colors.primaries.forEach((f){allowedColors.add(f);});
    allowedColors.remove(Colors.green);
    allowedColors.remove(Colors.lightGreen);
    allowedColors.remove(Colors.lime);
    allowedColors.remove(Colors.deepOrange);
    allowedColors.remove(Colors.red);

  }
  void buildAllowedColors() {
    Map<String, MaterialColor> rc={};
    rc["Green"]=Colors.green;

    Colors.primaries.forEach((f){allowedColors.add(f);});
    allowedColors.remove(Colors.green);
    allowedColors.remove(Colors.lightGreen);
    allowedColors.remove(Colors.lime);
    allowedColors.remove(Colors.deepOrange);
    allowedColors.remove(Colors.red);

  }
}