import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'PrefDefs.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class PrefList {
  List<PrefDef> prefDefs=[];
  PrefList({this.prefDefs});

  Map<String, dynamic> toJson() => _$PrefListToJson(this);
  factory PrefList.fromJson(Map<String, dynamic> json) {
    return _$PrefListFromJson(json);
  }
}
@JsonSerializable(nullable: true, includeIfNull: false)
class PrefDef {
  final String prefName;
  bool prefValue;
  bool persist;


  Map<String, dynamic> toJson() => _$PrefDefToJson(this);
  factory PrefDef.fromJson(Map<String, dynamic> json) {
    return _$PrefDefFromJson(json);
  }
  //WidgetProvider prefWidgetProvider;
  @JsonKey(ignore: true)
  ValueChanged<bool> vc;

  PrefDef({@required this.prefName, this.prefValue=false,persist=false}){
    /*if(prefWidgetProvider==null){
      prefWidgetProvider=this.foo;
    }*/
  }

  Widget foo(ValueChanged v){
    ValueChanged wrapped=(dynamic ok){
      toggleFoo(ok);
      v(ok);
    };
    return new CheckboxListTile(value: prefValue, onChanged: wrapped, title: new Text(prefName),);
  }

  void toggleFoo(bool newValue){
    print("togglePref: $newValue pref: $prefName");
    prefValue=!prefValue;
  }


}
