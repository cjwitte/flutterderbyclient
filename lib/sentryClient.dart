import 'package:flutter0322/autoTimer/models/Constants.dart';
import 'package:sentry/sentry.dart';
// This file does not exist in the repository and is .gitignored.
import 'dsn.dart';

final SentryClient _sentry = new SentryClient(dsn: dsn, environmentAttributes:
const Event(
  serverName: 'test.server.com',
  release: buildVersion,
  environment: 'staging',
),);


/// Whether the VM is running in debug mode.
///
/// This is useful to decide whether a report should be sent to sentry. Usually
/// reports from dev mode are not very useful, as these happen on developers'
/// workspaces rather than on users' devices in production.
bool get isInDebugMode {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}

/// Reports [error] along with its [stackTrace] to Sentry.io.
Future<Null> sentryReportError(dynamic error, dynamic stackTrace) async {
  print('sentryReportError Caught error: $error');

  // Errors thrown in development mode are unlikely to be interesting. You can
  // check if you are running in dev mode using an assertion and omit sending
  // the report.
  if (isInDebugMode) {
    print(stackTrace);
    print('sentryReportError In dev mode. TEST sending report to Sentry.io.');
    //return;
  }

  print('Reporting to Sentry.io...');

  final SentryResponse response = await _sentry.captureException(
    exception: error,
    stackTrace: stackTrace,
  );

  if (response.isSuccessful) {
    print('Success! Event ID: ${response.eventId}');
  } else {
    print('Failed to report to Sentry.io: ${response.error}');
  }
}