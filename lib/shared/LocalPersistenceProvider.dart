import 'dart:io';

import 'dart:math';

typedef Future<String> LpProviderType();

class LocalPersistenceProvider{
  final LpProviderType getTopPersistenceDir;
  final LpProviderType getRacePersistenceDir;
  //TODO: ideally RacePersistence is in a subdir.  Since this is server use only right now, it isnt' really relevant.
   LocalPersistenceProvider({this.getTopPersistenceDir: tmpProvider, this.getRacePersistenceDir: tmpProvider});
   Future<File> localFile({String fileName = ""}) async {
     if (fileName.length == 0) {
       fileName = "tmp." + new Random().nextInt(100000000).toString();
     }
     final path=await this.getRacePersistenceDir();

     return new File('${path}/$fileName');
   }
}
Future<String> tmpProvider() async{
  return ("/tmp");
}
