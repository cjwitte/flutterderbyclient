import 'dart:convert';
import 'dart:io';

import 'package:flutter0322/shared/StaticShared.dart';
import 'package:json_annotation/json_annotation.dart';
part 'LoginCredentials.g.dart';

enum LoginRole { None, Timer, Starter, Admin }
@JsonSerializable(nullable: true, includeIfNull: false)
class LoginCredentials {
  String user;
  String password; //TODO: hack to propagate user to mqtt.  deleteME
  final LoginRole loginRole;
  final String sessionId;
  LoginCredentials({this.loginRole: LoginRole.None, this.sessionId, this.user, this.password});
  Map<String, dynamic> toJson() => _$LoginCredentialsToJson(this);
  factory LoginCredentials.fromJson(Map<String, dynamic> json) {
    return _$LoginCredentialsFromJson(json);
  }

  static LoginCredentials fromJsonMap(Map<String, dynamic> jsonMap,
      {String sessionId}) {
    print("fromJsonMappy: $jsonMap");
    // var json = jsonDecode(jsonString);

    LoginRole assignedRole = LoginRole.None;
    if (jsonMap["roleStarter"]) {
      assignedRole = LoginRole.Starter;
    }
    if (jsonMap["roleTimer"]) {
      assignedRole = LoginRole.Timer;
    }
    if (jsonMap["roleAdmin"]) {
      assignedRole = LoginRole.Admin;
    }

    print("fromJsonMappy: role: $assignedRole");

    return LoginCredentials(loginRole: assignedRole, sessionId: sessionId);
  }
  static Future<File> getLcFile() async {
    //return _localFile(fileName: "raceConfig.ndjson");
    String dirName= await StaticShared.getShared().localPersistenceProvider.getTopPersistenceDir();
    File rcFile= new File("$dirName/login.ndjson");

    print("LoginCredentials using file: "+rcFile.absolute.toString());
    return rcFile;

  }

  Future persistToFile() async {
    File lcFile = await getLcFile();
    var json = new JsonCodec();
    String jsonString = json.encode(this);
    print("persistLoginCredentialsToFile:  lcFile: ${lcFile.toString()} $jsonString");

    var oStream = await lcFile.openWrite();
    oStream.write(jsonString);
    await oStream.close();
  }
  static Future<LoginCredentials> restoreLoginCredentials() async {
    File lcFile = await getLcFile();
    if (!lcFile.existsSync()) {
      print("restoreLoginCredentials: no lcFile: ${lcFile.toString()}");

      return new LoginCredentials();
    }
    Stream<List<int>> inputStream = await lcFile.openRead();

    String line = await inputStream
        .transform(new Utf8Decoder())
        .transform(new LineSplitter())
        .first;
    if (line == null) {
      print("restoreLoginCredentials: unable to restore lc");
      return new LoginCredentials();
    }
    var json = jsonDecode(line);

    print("restoreLoginCredentials: found lcFile: $json");

    return  LoginCredentials.fromJson(json);
  }



  //TODO: we can apparently use built_value for equals check..
  //https://github.com/google/built_value.dart
  bool operator ==(o) =>
      o is LoginCredentials &&
          o.loginRole == loginRole &&
          o.sessionId == sessionId;

  bool canAddRacePhase() {
    return (this.loginRole == LoginRole.Starter ||
        this.loginRole == LoginRole.Admin ||
        this.loginRole == LoginRole.Timer);
  }

  bool canAddPendingRace() {
    return (this.loginRole == LoginRole.Admin ||
        this.loginRole == LoginRole.Timer);
  }

  bool isRoleTimer() {
    return (this.loginRole == LoginRole.Admin ||
        this.loginRole == LoginRole.Timer);
  }

  bool canChangeBracketName() {
    return (this.loginRole == LoginRole.Admin ||
        this.loginRole == LoginRole.Timer);
  }

  String toString() {
    return "LoginCredentials $loginRole session: $sessionId user: $user" ;
  }

  bool canEditRacers() {
    //return true;
    return (this.loginRole == LoginRole.Admin ||
        this.loginRole == LoginRole.Timer);
  }
}