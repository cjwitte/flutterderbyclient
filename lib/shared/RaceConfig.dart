
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter0322/shared/StaticShared.dart';
import 'package:xml/src/xml/nodes/document.dart';
import 'package:xml/xml.dart' as xml;
//import 'package:flutter0322/network/GetS3Object.dart';


class RaceConfig {
  final String raceName;
  final String applicationUrl;
  final String s3BucketUrlPrefix;
  RaceConfig({this.applicationUrl, this.s3BucketUrlPrefix, this.raceName}) {}
  RaceConfig.fromJson(Map<String, dynamic> json)
      : raceName = json["raceName"],
        applicationUrl = json["applicationUrl"],
        s3BucketUrlPrefix = json["s3BucketUrlPrefix"];
  Map<String, dynamic> toJson() => {
    'raceName': raceName,
    'applicationUrl': applicationUrl,
    's3BucketUrlPrefix': s3BucketUrlPrefix,
  };
  static RaceConfig fromXml({String xmlString, String raceName}) {
    var document = xml.parse(xmlString);
    print("RaceConfig fromXml: " + document.toString());
    String applicationUrl = getTextForTag(document, "applicationUrl");
    String s3BucketUrlPrefix = getTextForTag(document, "s3BucketUrlPrefix");
    return new RaceConfig(
        applicationUrl: applicationUrl,
        s3BucketUrlPrefix: s3BucketUrlPrefix,
        raceName: raceName);
  }

  String getBaseUrl() {
    return applicationUrl.replaceAll("root@", ""); //wtf is this in here for???
  }

  String getMqttHostname() {
    String rc = applicationUrl;
    print("getMqttHostname: $rc");
    //TODO: build uri/url and get hostname.  did not find a relevant constructor :-(
    rc = rc.replaceAll("http://", "");
    rc = rc.replaceAll("root@", "");
    List<String> rcSplit = rc.split(new RegExp(":"));
    rc = rcSplit[0];
    print("getMqttHostname: gave: $rc");

    return rc;
  }
  static Future<File> raceConfigFile() async {
    //return _localFile(fileName: "raceConfig.ndjson");
    String dirName= await StaticShared.getShared().localPersistenceProvider.getTopPersistenceDir();
    File rcFile= new File("$dirName/raceConfig.ndjson");

    print("RaceConfigFile using file: "+rcFile.absolute.toString());
    return rcFile;

  }

  Future persistToFile() async {
    File rcFile = await raceConfigFile();
    var json = new JsonCodec();
    String jsonString = json.encode(this);
    print("persistToFile:  rcFile: ${rcFile.toString()} $jsonString");

    var oStream = await rcFile.openWrite();
    oStream.write(jsonString);
    await oStream.close();
  }

  static Future<RaceConfig> restoreRaceConfig() async {
    File rcFile = await raceConfigFile();
    if (!rcFile.existsSync()) {
      print("restoreRaceConfig: no rcFile: ${rcFile.toString()}");

      return null;
    }
    Stream<List<int>> inputStream = await rcFile.openRead();

    String line = await inputStream
        .transform(new Utf8Decoder())
        .transform(new LineSplitter())
        .first;
    if (line == null) {
      print("restoreRaceConfig: unable to restore raceConfig");
      return null;
    }
    var json = jsonDecode(line);

    // var json = JSON.decode(line);
    print("restoreRaceConfig: found rcFile: $json");

    return new RaceConfig.fromJson(json);
  }

  static String getTextForTag(XmlDocument doc, String tag) {
    String rc = "";
    for (var child in doc.findAllElements(tag)) {
      print("getTextForTag child {$tag}: " + child.toString());
      rc = child.text;
    }
    return rc;
  }
}
