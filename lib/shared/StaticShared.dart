import 'dart:async';

import 'package:flutter0322/shared/LocalPersistenceProvider.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/RaceConfig.dart';
import 'package:flutter0322/shared/TImerConfig.dart';

class StaticShared {
  static StaticShared _staticShared = new StaticShared();

  StreamController<StaticShared> staticSharedStreamController;
  Stream<StaticShared> staticSharedStream;

  StreamController<LoginCredentials> loginCredentialsStreamController;
  Stream<LoginCredentials> loginCredentialsStream;

  LoginCredentials _loginCredentials =
      new LoginCredentials(loginRole: LoginRole.None);
  TimerConfig _timerConfig = new TimerConfig(
      minTime: new Duration(milliseconds: 222), //
      maxTime: new Duration(milliseconds: 555), //
      maxPerforationCount: 1, //
      maxPerforationTime: new Duration(milliseconds: 15));
  RaceConfig raceConfig;
  LocalPersistenceProvider localPersistenceProvider;

  static StaticShared getShared() {
    return _staticShared;
  }

  set timerConfig(TimerConfig timerConfig){
    this._timerConfig=timerConfig;
    staticSharedStreamController.add(this);
  }
  get timerConfig{
    return this._timerConfig;
  }

  set loginCredentials(LoginCredentials loginCredentials){
    this._loginCredentials=loginCredentials;
    loginCredentialsStreamController.add(loginCredentials);
    staticSharedStreamController.add(this);
  }
  get loginCredentials{
    return this._loginCredentials;
  }
  StaticShared({this.localPersistenceProvider:null}) {
    print("StaticShared constructor\n");
    if(localPersistenceProvider==null){
      localPersistenceProvider=new LocalPersistenceProvider();
    }
    staticSharedStreamController =
    new StreamController<StaticShared>.broadcast();
    staticSharedStream = staticSharedStreamController.stream;

    loginCredentialsStreamController =
    new StreamController<LoginCredentials>.broadcast();
    loginCredentialsStream = loginCredentialsStreamController.stream;
  }
}
