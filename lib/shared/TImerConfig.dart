import 'dart:convert';

import 'package:meta/meta.dart';

class TimerConfig {
  static const desiredTimerConfigTopic="timer/desiredTimerConfig";
  static const activeTimerConfigTopic="timer/activeTimerConfig";
  Duration minTime;
  Duration maxTime;
  int maxPerforationCount;
  Duration maxPerforationTime;
  TimerConfig(
      {@required this.minTime,
        @required this.maxTime,
        @required this.maxPerforationCount,
        @required this.maxPerforationTime});
  Map toJson(){
    return {"minMs":minTime.inMilliseconds, "maxMs":maxTime.inMilliseconds, "maxPerforationCount":maxPerforationCount, "maxPerforationMs": maxPerforationTime.inMilliseconds};
  }
  String toString(){
    var json = new JsonCodec();
    return json.encode(this);
  }
  TimerConfig clone(){
    return TimerConfig.fromJsonMap(this.toJson());
  }
  TimerConfig.fromJsonMap(Map jsonMap) {
    initFromMap(jsonMap);
  }
  TimerConfig.fromJsonString(String jsonString) {
    var json = new JsonCodec();
    initFromMap(json.decode(jsonString));
  }
  void initFromMap(Map jsonMap) {
    this.minTime=new Duration( milliseconds: jsonMap["minMs"]);
    this.maxTime=new Duration( milliseconds:jsonMap["maxMs"]);
    this.maxPerforationCount=jsonMap["maxPerforationCount"];
    this.maxPerforationTime=new Duration( milliseconds:jsonMap["maxPerforationMs"]);
  }

}