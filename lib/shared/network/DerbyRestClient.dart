import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter0322/shared/HistoryType.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/RaceConfig.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/shared/network/RestDio.dart';

typedef Future<Response> _DoHttp(Map<String, String> dataMap, String urlSuffix,
    {Dio dio});

class Login {
  final String user;
  final String password;

  Login({this.user, this.password});

  Future<LoginCredentials> doLogin({RaceConfig raceConfig}) async {
    if (raceConfig == null) {
      raceConfig = StaticShared.getShared().raceConfig;
    }
    String urlString =
        raceConfig.getBaseUrl() + "/resources/j_spring_security_check";
    print("Login with url: $urlString");
    print("Login with user: [$user] password: [$password]");

    Dio dio = await getPersistentDio();

    //j_username=timer&j_password

    var userPasswordMap = {
      "j_username": user, // Other Field
      "j_password": password,
    };

    print("login encoded");
    try {
      var res = await dio.post(urlString,
          data: userPasswordMap,
          options: new Options(
              contentType:
                  ContentType.parse("application/x-www-form-urlencoded")));
      print("Login response: $res");
    } catch (e) {
      print("Login error: $e");
    }
    print("prepare to ajax");

    LoginAjax loginAjax =
        new LoginAjax(username: this.user, password: this.password);
    //LoginCredentials lc = await loginAjax. getAjax(raceConfig:raceConfig, dio:dio);
    LoginCredentials lc = await loginAjax.getAjax();
    if (lc != null) {
      print("Login A promoting credentials from ajax: ${lc}");
      // getAjax published the result...
    }
    return lc;
  }
}

class PostAddBlocks {
  String getBlockUrl(HistoryType historyType) {
    if(historyType==null){
      return "/ILLOGIC_NULL";
    }
    switch (historyType) {
      case HistoryType.Phase:
        return "/checkBlocks";
      case HistoryType.Pending:
        return "/addManualPending";
      case HistoryType.Standing:
        return "/ILLOGIC";
      default:
        return "/ILLOGIC2";
    }
  }

  Future postAddBlocks(Map<String, String> postData, HistoryType historyType,
      {Dio dio}) async {
    Response response=await new PostAddBlocks().postForm(postData, getBlockUrl(historyType));
    print("postAddBlocks response: ${response.data}");
  }

  Future postRmBlocks(int id, {Dio dio}) async {
    Map<String, String> dataMap = {
      "id": id.toString(),
      "block1": "",
      "block2": ""
    };
    Response response=await new PostAddBlocks().postForm(dataMap, "/rmBlocks");
    print("postAddBlocks response: ${response.data}");
  }

  Future postRmRaceBracket(int raceBracketID, {Dio dio}) async {

    Map<String, String> dataMap = {
      "raceBracketID": raceBracketID.toString(),
    };
    Response response=await new PostAddBlocks().postForm(dataMap, "/rmRaceBracket");

    print("postAddBlocks response: ${response.data}");
  }

  Future<Response> getUrl(Map<String, String> dataMap, String urlSuffix,
      {Dio dio}) async {
    return await _xxxxxUrl(_getUrl, dataMap, urlSuffix, dio: dio);
  }

  Future<Response> _xxxxxUrl(
      _DoHttp _doHttp, Map<String, String> dataMap, String urlSuffix,
      {Dio dio}) async {
    print("_xxxxxUrl: begin: $urlSuffix.");

    try {
      return await _doHttp(dataMap, urlSuffix, dio: dio);
    } on DioError catch (e) {
      print("_xxxxxUrl: caught DioError.");

      // should only happen on first time thru or session expired.
      if (StaticShared.getShared().loginCredentials.user != null) {
        print("_xxxxxUrl: handling 302. attempting to (re)login.");
        Login login = new Login(
            user: StaticShared.getShared().loginCredentials.user,
            password: StaticShared.getShared().loginCredentials.password);
        await login.doLogin();
        LoginRole role = StaticShared.getShared().loginCredentials.loginRole;
        if (role != null && role != LoginRole.None) {
          return await _doHttp(dataMap, urlSuffix, dio: dio);
        }
      } else {
        print("_xxxxxUrl no credential info available to invoke retry.");
      }
      rethrow;
    }
  }

  Future<Response> _getUrl(Map<String, String> dataMap, String urlSuffix,
      {Dio dio}) async {
    if (dio == null) {
      dio = await getPersistentDio();
    }
    var raceConfig = StaticShared.getShared().raceConfig;

    String urlString = raceConfig.getBaseUrl() + urlSuffix;

    print("getting url: [$dataMap] from [$urlString]\n");

    Response response = await dio.get(
      urlString,
      data: dataMap,
    );
    print("_getUrl [$urlString] response: ${response.data}");

    return response;
  }

  // postForm wrapper will handle the redirect to login page, and attempt a login (once).
  Future<Response> postForm(Map<String, String> dataMap, String urlSuffix,
      {Dio dio}) async {
    return await _xxxxxUrl(_postForm, dataMap, urlSuffix, dio: dio);
  }

  Future<Response> _postForm(Map<String, String> dataMap, String urlSuffix,
      {Dio dio}) async {
    if (dio == null) {
      dio = await getPersistentDio();
    }
    var raceConfig = StaticShared.getShared().raceConfig;

    //String urlString = raceConfig.getBaseUrl() + "/rmRaceBracket";
    String urlString = raceConfig.getBaseUrl() + urlSuffix;

    print("posting form: [$dataMap] to [$urlString]\n");

    Response response = await dio.post(urlString,
        data: dataMap,
        options: new Options(
            contentType:
                ContentType.parse("application/x-www-form-urlencoded")));
    print("postJson [$urlString] response: ${response.data}");

    return response;
  }

  Future<Response> postJson(String json, String urlSuffix, {Dio dio}) async {
    Map<String, String> dataMap = {
      "json": json,
    };

    return postForm(dataMap, urlSuffix, dio: dio);
  }

  Future postApplyWinner(int phaseTime, String racePhaseId, {Dio dio}) async {
    print("posting applyWinner: [$phaseTime]\n");
    Map<String, String> dataMap = {
      "block1": "",
      "block2": "",
      "phaseTime": phaseTime.toString(),
      "racePhaseId": racePhaseId,
    };
    Response response=await new PostAddBlocks().postForm(dataMap, "/applyWinner");


    print("applyWinner  response: ${response.data}");
  }
}

class LoginAjax {
  String username;
  String password;
  LoginAjax({this.username, this.password});
  Future<LoginCredentials> getAjax({RaceConfig raceConfig, Dio dio}) async {
    if (dio == null) {
      dio = await getPersistentDio();
    }
    if (raceConfig == null) {
      raceConfig = StaticShared.getShared().raceConfig;
    }

    String urlString = raceConfig.getBaseUrl() + "/login/login.json";
    print("getAjax with url: $urlString");

    //var dio = new Dio();
    //dio.cookieJar =cookieJar;

    //curl --cookie JSESSIONID=B3EA5777032C3C8B16D54BA73392EC08  http://207.148.14.78:8888/derbytimer-0.1.5.BUILD-SNAPSHOT/login/login.json
    print("getAjax encoded");
    try {
      Map<String, String> hdrMap = {};
      //hdrMap["Cookie"]="JSESSIONID=FC5811AC07134C513BE26FE7EE542DBC";
      //hdrMap["Authorization"]="Bearer none";
      Options options = new Options(headers: hdrMap);
      Response response = await dio.get(urlString, options: options);
      print("ajax credentials: ${response.data}");

      LoginCredentials loginCredentials = LoginCredentials.fromJsonMap(
          response.data,
          sessionId: "TODO: pull from session");
      if (loginCredentials.isRoleTimer()) {
        loginCredentials.user = this.username;
        loginCredentials.password = this.password;
      }
      // globals.globalDerby.setLoginCredentials(loginCredentials);
      StaticShared.getShared().loginCredentials = loginCredentials;

      return loginCredentials;
    } catch (e) {
      print("getAjax error: $e");
      return null;
    }
  }
}
