import 'dart:async';
import 'dart:io';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:flutter0322/shared/StaticShared.dart';
//import 'package:path_provider/path_provider.dart';
Future deleteCookies() async{

  try {
    await new File(await getPersistentCookiePath()).delete(recursive: true);
  }
  catch(e){
    print("Delete cookie failed: $e");
  }
}
Future<Dio> getPersistentDio() async {
  var dio = new Dio();

  if (true) {

    var cj = new PersistCookieJar(dir:await getPersistentCookiePath());

    //cj.deleteAll(); // TEST to cleanup old cookies...
    //var httpClient = new HttpClient();
    dio.cookieJar = cj;
  }
  return dio;

}
Future<String> getPersistentCookiePath() async{
  print("getPersistentCookiePath.");

  final directory=await StaticShared.getShared().localPersistenceProvider.getRacePersistenceDir();
  //final directory = (await getApplicationDocumentsDirectory()).path;
  final String rc= "$directory/.cookiesF21";
  /*
  Directory cf=new Directory(rc);
  Stream<FileSystemEntity> flen=await cf.list();
  await flen.listen((FileSystemEntity entity) {
    print("cookie file?: ${entity.path}");
  });


  print("cookiePath: $rc [$flen]");
  */
  print("cookiePath: $rc");

  return rc;
}
