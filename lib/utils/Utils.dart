import 'package:flutter/material.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/models.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
typedef Widget  GetSliderContent(BuildContext context);
typedef void HandleClick(BuildContext context);

class Utils {

  static Widget getSlidable(BuildContext context, GetSliderContent gscFuntion,
      {HandleClick onDelete, HandleClick onEdit}) {
    List<Widget> sliderActions = [];
    if (onDelete!=null) {
      sliderActions.add(new IconSlideAction(
          caption: 'Delete',
          color: Colors.blue,
          icon: Icons.delete,
          onTap: () => onDelete(context)
      ));
    }
    if (onEdit!=null) {
      sliderActions.add(new IconSlideAction(
        caption: 'Edit',
        color: Colors.indigo,
        icon: Icons.edit,
        onTap: () => onEdit(context)

        ,
      ));
    }
    return new Slidable(

      delegate: new SlidableDrawerDelegate(),
      actionExtentRatio: 0.2,
      child: gscFuntion(context),

      actions: sliderActions,
    );
  }
  static Future<Racer> lookupRacerFromDb(String carNumber)async{
     var sqlResultList=await globals.globalDerby.derbyDb?.database
        ?.rawQuery(Racer.getSelectSql(carFilter: carNumber, exactMatch: true));

     var racer;
      sqlResultList.forEach((map) {
         racer = Racer.fromJson(map);
      });

    return racer;
  }
  static Future<RaceBracket> getRaceBracket(int id) async {
    List<Map> maps = await globals.globalDerby.derbyDb?.database?.query(
        "RaceBracket", where: 'id = ?', whereArgs: [id]);
    return RaceBracket.fromJsonMap(maps.first);
  }
  static Future<Map<String,RaceStanding>> getRaceStandingMapForBracket(int raceBracketId)async{
    Map<String,RaceStanding> rsMap={};

    List<Map> maps = await globals.globalDerby.derbyDb?.database
        ?.rawQuery(RaceStanding.getSelectSql(
        getPending: true,raceBracketID: raceBracketId));

    maps.forEach((map){
        var rs= RaceStanding.fromSqlMap(map);
        rsMap[rs.chartPosition]=rs;
      });

      print("getRaceStandingMapForBracket: $rsMap");

    return rsMap;
  }
}