import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter0322/dialogs/LoginDialog.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/globals.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/StaticShared.dart';
//import 'package:qrcode_reader/qrcode_reader.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';


class FilterRowWidget extends StatefulWidget {
  final Type triggerTable;
  FilterRowWidget({this.triggerTable});
  @override
  State<StatefulWidget> createState() {
    return new FilterRowWidgetState(triggerTable: this.triggerTable);
  }
}

class FilterRowWidgetState extends State<FilterRowWidget> {
  TextEditingController _controller;
  final Type triggerTable;

  final List<FilterSegue> fsList = [
    new FilterSegueLogin(),
    new FilterSegueQrScan(),
    new FilterExperimental(),
    new FilterBrightness(),
    new FilterSegueCrash(),
    //new FilterFakeLoginUI(),
  ];
  FilterRowWidgetState({this.triggerTable});
  @override
  void initState() {
    super.initState();

    _controller =
        new TextEditingController(text: globals.globalDerby.sqlCarNumberFilter);
  }

  @override
  Widget build(BuildContext context) {
    String filter = globals.globalDerby.sqlCarNumberFilter;

    return new Row(
      children: <Widget>[
        new Icon(Icons.filter_list, size: 48.0),
        new Container(width: 30.0),
        new Container(
            width: 75.0,
            child: new TextField(
              autofocus: false,
              maxLength: 3,
              controller: _controller,
              onSubmitted: applyCarFilter,
              onChanged: applyCarFilter,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                  //labelText: 'CarNumber',
                  hintText: 'Car Filter'),
            )),
      ],
    );
  }

  void applyCarFilter(String carFilter) {
    //skip already processed changes.
    if (carFilter == globals.globalDerby.sqlCarNumberFilter) return;



    fsList.forEach((f)  {
      if(f.considerFilterForSegue(context, carFilter)){
        _controller.clear();
        carFilter="";
      }
    });
    globals.globalDerby.sqlCarNumberFilter = carFilter;

    // cause the screen to repaint...
    globals.globalDerby.derbyDb.recentChangesController
        .add(triggerTable.toString());
  }
}

typedef void FilterSegueCallback(BuildContext context);

abstract class FilterSegue {
  final FilterSegueCallback callback = null;
  final Map<String, int> segueCriteriaMap = null;

  // returning true will cause filter to be cleared.
  bool considerFilterForSegue(BuildContext context, String carNumber) {
    bool rc=false;
    if (!segueCriteriaMap.containsKey(carNumber)) {
      return rc;
    }
    int now = new DateTime.now().millisecondsSinceEpoch;
    segueCriteriaMap[carNumber] = now;
    int recentCount = 0;
    int recentThreshold = now - 30000;

    segueCriteriaMap.forEach((key, value) {
      if (value > recentThreshold) {
        recentCount++;
        rc=true;
      }
    });
    if (recentCount == segueCriteriaMap.length) {
      callback(context);
      //LoginDialog.showLoginDialog(context);
    }
    return rc;
  }
}
class FilterBrightness extends FilterSegue {
  final Map<String, int> themeMap = {
    "998": 0,
  };
  @override
  FilterSegueCallback get callback => (BuildContext context) {
    //globalDerby.myColors.switchBrightness(context);

  };
  @override
  Map<String, int> get segueCriteriaMap => themeMap;
}

class FilterExperimental extends FilterSegue {
  final Map<String, int> experimentalMap = {
    "042": 0,
  };
  @override
  FilterSegueCallback get callback => (BuildContext context) {
    globals.globalDerby.setExperimental(true);
  };
  @override
  Map<String, int> get segueCriteriaMap => experimentalMap;
}
class FilterFakeLoginUI extends FilterSegue {
  final Map<String, int> experimentalMap = {
    "505": 0,
    "606": 0,
  };
  @override
  FilterSegueCallback get callback => (BuildContext context) {
    StaticShared.getShared().loginCredentials = new LoginCredentials(loginRole: LoginRole.Admin);
  };
  @override
  Map<String, int> get segueCriteriaMap => experimentalMap;
}
class FilterSegueLogin extends FilterSegue {
  final Map<String, int> loginMap = {
    "888": 0,
  };
  @override
  FilterSegueCallback get callback => (BuildContext context) {
        LoginDialog.showLoginDialog(context);
      };
  @override
  Map<String, int> get segueCriteriaMap => loginMap;
}
class FilterSegueCrash extends FilterSegue {
  final Map<String, int> loginMap = {
    "899": 0,
  };
  @override
  FilterSegueCallback get callback => (BuildContext context) {
    throw new StateError('Exception Test 889.');
  };
  @override
  Map<String, int> get segueCriteriaMap => loginMap;
}

class FilterSegueQrScan extends FilterSegue {
  final Map<String, int> qrMap = {
    "889": 0,
  };
  @override
  FilterSegueCallback get callback => (BuildContext context) {
        print("qr scan");

        Future<String> futureString=scan();
        //Future<String> futureString = new QRCodeReader().scan();

        futureString.then((qrString) =>
            LoginDialog.showLoginDialog(context, qrString: qrString));
      };
  @override
  Map<String, int> get segueCriteriaMap => qrMap;
}
Future <String>scan() async {
  try {
    String barcode = await BarcodeScanner.scan();
    return barcode;
  } on PlatformException catch (e) {
    if (e.code == BarcodeScanner.CameraAccessDenied) {
        return 'The user did not grant the camera permission!';
    } else {
      return 'Unknown error: $e';
    }
  } on FormatException{
    return 'null (User returned using the "back"-button before scanning anything. Result)';
  } catch (e) {
    return 'Unknown error: $e';
  }
}
