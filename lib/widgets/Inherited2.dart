import 'package:flutter/material.dart';

class MoginCredentialsInherited extends StatefulWidget {
  Widget child;

  MoginCredentialsInherited({this.child, MoginPing moginPing});

  @override
  MoginCredentialsState createState() => new MoginCredentialsState();

}

class MoginCredentialsState extends State<MoginCredentialsInherited> {
  MoginPing _myField=new MoginPing();// default is no perms
  // only expose a getter to prevent bad usage
  MoginPing get myField => _myField;

  void onMyFieldChange(MoginPing newValue) {
    print ("onMyFieldChange: $newValue");
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return new _MoginInherited(
      data: this,
      child: widget.child,
    );
  }
  static MoginCredentialsState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(_MoginInherited) as _MoginInherited).data;
  }
}

/// Only has MyInheritedState as field.
class _MoginInherited extends InheritedWidget {
  final MoginCredentialsState data;

  _MoginInherited({Key key, this.data, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_MoginInherited old) {
    print ("updateShouldNotify: $old");
    print ("updateShouldNotify: $data");

    return true;
  }
}
class MoginPing{

}