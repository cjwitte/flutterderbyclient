import 'package:flutter/material.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';

class LoginCredentialsInherited extends StatefulWidget {
  Widget child;

  LoginCredentialsInherited({this.child, LoginCredentials loginCredentials});

  @override
  LoginCredentialsState createState() => new LoginCredentialsState();


}

class LoginCredentialsState extends State<LoginCredentialsInherited> {
  LoginCredentials _myField=new LoginCredentials();// default is no perms
  // only expose a getter to prevent bad usage
  LoginCredentials get myField => _myField;

  void onMyFieldChange(LoginCredentials newValue) {
    print ("onMyFieldChange: $newValue");
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return new _LoginInherited(
      data: this,
      child: widget.child,
    );
  }
  static LoginCredentialsState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(_LoginInherited) as _LoginInherited).data;
  }
}

/// Only has MyInheritedState as field.
class _LoginInherited extends InheritedWidget {
  final LoginCredentialsState data;

  _LoginInherited({Key key, this.data, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_LoginInherited old) {
    print ("updateShouldNotify: $old");
    print ("updateShouldNotify: $data");

    return true;
  }
}