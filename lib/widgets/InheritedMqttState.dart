import 'package:flutter/material.dart';
import 'package:flutter0322/globals.dart' as globals;

class MqttStateInherited extends StatefulWidget {
  Widget child;

  MqttStateInherited({this.child, MqttPing mqttPing});

  @override
  MqttState createState() => new MqttState();

}

class MqttState extends State<MqttStateInherited> with WidgetsBindingObserver{
  MqttPing _myField=new MqttPing();// default is no perms
  // only expose a getter to prevent bad usage
  MqttPing get myField => _myField;

  void onMyFieldChange(MqttPing newValue) {
    print ("onMyFieldChange: $newValue");
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return new _MqttInherited(
      data: this,
      child: widget.child,
    );
  }
  static MqttState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(_MqttInherited) as _MqttInherited).data;
  }



  // BINDING OBSERVER
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }
  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("didChangeState: "+state.toString());
    if(state==AppLifecycleState.resumed) {
      print("didChangeState: checking mqtt state on resume! "+state.toString());

      globals.globalDerby.mqttObserver.refreshConnectionStatus();
    }
    //setState(() { _notification = state; });
  }
}

/// Only has MyInheritedState as field.
class _MqttInherited extends InheritedWidget {
  final MqttState data;

  _MqttInherited({Key key, this.data, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_MqttInherited old) {
    print ("updateShouldNotify: $old");
    print ("updateShouldNotify: $data");

    return true;
  }
}

class MqttPing{
  int heartbeat=0;
}