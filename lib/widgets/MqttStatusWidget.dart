import 'package:flutter/material.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/network/MqttOberver.dart';

class MqttStatusWidget extends StatelessWidget{
  //final StreamController<MqttPing> _streamController = new StreamController<MqttPing>();
/*
  Stream<MqttStateLog> timedCounter(Duration interval) async* {
    int i = 0;
    while (true) {
      await Future.delayed(interval);
      yield new MqttStateLog();
    }
  }
  */
  @override
  Widget build(BuildContext context) {


    var sb=new StreamBuilder<MqttStateLog>(
        stream: globals.globalDerby.mqttStateLogStream ,
        initialData: globals.globalDerby.mqttObserver.getMqttStateLog(),
        builder: (BuildContext context, AsyncSnapshot<MqttStateLog> asyncSnapshot) {

          if (asyncSnapshot.hasError) {
            return new Text("Error!");
          } else if (asyncSnapshot.data == null) {
            return new  Text("Null Data");
          }else {
            final mqttStateLog = asyncSnapshot.data;
          return buildUi(context, mqttStateLog);
        }});
    print ("sb returning $sb");
    return sb;

  }

  Widget buildUi(BuildContext context, MqttStateLog msl) {
    //return null;
    return new Text(getMqttRaceStatus(context,msl));
  }
  static String getMqttRaceStatus(BuildContext context, MqttStateLog msl) {
    if (globals.globalDerby.mqttObserver == null) {
      return "...";
    }

    print("NavDrawerStatus: ${globals.globalDerby.mqttObserver.getMqttStateLog().toString()}");
    //String mqttPing="mp: "+msl.toString();
    String stateLog=globals.globalDerby.mqttObserver.getMqttStateLog().toString();
    //return "${buildVersion}\n${stateLog}\n$mqttPing";
    return "\n${stateLog}\n";
    //return globals.globalDerby.mqttObserver.getMqttState().toString();
  }
}