import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter0322/appPages/BracketDetailTabs.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/modelUi.dart';
import 'package:flutter0322/models.dart';
import 'package:flutter0322/shared/LoginCredentials.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:flutter0322/utils/Utils.dart';
import 'package:flutter0322/widgets/RacerWidget.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class RaceBracketWidget extends StatelessWidget {
  final double f1 = 1.0;
  final RaceBracket raceBracket;
  final Color bgColor;
  RaceBracketWidget({Key, key, this.raceBracket, this.bgColor})
      : assert(raceBracket != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    if (globals.globalDerby.loginCredentials.canEditRacers()) {
      return Utils.getSlidable(context, getSliderContent, onDelete: (context){_promptForDelete(context);});
    } else {
      return getSliderContent(context);
    }
  }

  void handleTapAndSelectBracket(BuildContext context) async {
    print("Tapped: ${raceBracket.raceName}");
    print("json: ${raceBracket.jsonDetail}");
    globals.globalDerby.derbyDb?.database
        ?.rawQuery(RaceStanding.getSelectSql(
        getPending: true,raceBracketID: this.raceBracket.id))
        ?.then((sqlResultList) {
          Map<String,RaceStanding> rsMap={};
          sqlResultList.forEach((map){
           var rs= RaceStanding.fromSqlMap(map);
            rsMap[rs.chartPosition]=rs;
          });

          print("handleTapAndSelectBracket: $rsMap");
          var arg=new RaceBracketDetailUi(raceBracket,rsMap);
          Navigator.of(context).pushNamed('/BracketDetailTabsPage', arguments: arg);

          return;

    });


  }

  void _promptForDelete(BuildContext context) {
    print("LongPress: RBW");
    LoginCredentials loginCredentials=globals.globalDerby.loginCredentials;
    if(loginCredentials.canChangeBracketName()) {

      Navigator.of(context).push(new MaterialPageRoute<Null>(
          builder: (BuildContext context) {
            return new DeleteRaceBracketDialog(raceBracket);
          },
          fullscreenDialog: true));
    }
  }

  Widget getSliderContent(BuildContext context) {

    return new GestureDetector(
        onTap: () {
          handleTapAndSelectBracket(context);
        },
        /*
        onLongPress: () {
          _promptForDelete(context);
        },
        */
        child: new Container(
            color: bgColor,
            child: new Row(
              children: <Widget>[
                new Padding(
                    padding: new EdgeInsets.all(18.0),
                    child: new Text(raceBracket.raceName,
                        textAlign: TextAlign.left,
                        textScaleFactor: f1,
                        style: new TextStyle(fontWeight: FontWeight.bold))),
                new Padding(
                    padding: new EdgeInsets.all(18.0),
                    child: new Text(raceBracket.raceStatus,
                        textAlign: TextAlign.left, textScaleFactor: f1)),
              ],
            )));

  }
}

class DeleteRaceBracketDialog extends StatefulWidget {
  final RaceBracket raceBracket;
  DeleteRaceBracketDialog(this.raceBracket);
  @override
  DeleteRaceBracketDialogState createState() => new DeleteRaceBracketDialogState();
}

class DeleteRaceBracketDialogState extends State<DeleteRaceBracketDialog> {
  TextEditingController __rbtController;

  var _raceBracketConfirm;

  @override
  void initState() {
    //_raceBracketConfirm = widget.raceBracket.raceName;
    _raceBracketConfirm = "";
    __rbtController = new TextEditingController(text: "");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var raceName=this.widget.raceBracket.raceName;

    return new Scaffold(
      appBar: new AppBar(
        title: const Text('Delete Race Bracket'),
        actions: [
          new FlatButton(
              onPressed: () {
                if(raceName==_raceBracketConfirm) {
                  new PostAddBlocks().postRmRaceBracket(
                      this.widget.raceBracket.id);
                  Navigator.of(context).pop();

                }
                else{
                  // this seems to not work (but doesn't break anything).
                  RacerWidget.showSnackBar("Failed to confirm race name", context);
                }
              },
              child: new Text('Delete',
                  style: Theme
                      .of(context)
                      .textTheme
                      .subhead
                      .copyWith(color: Colors.white))),
        ],
      ),
      body:
      new Column(children:[
        new Text("[$raceName]"),
        new ListTile(
          leading: new Icon(Icons.brightness_1),
          title: new TextField(
            decoration: new InputDecoration(
              hintText: 'Confirm Bracket Name',
            ),
            controller: __rbtController,
            onChanged: (value) => _raceBracketConfirm = value,
          ),
        ),
      ])

    );
  }
}
