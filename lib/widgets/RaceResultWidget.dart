import 'package:flutter/material.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/modelUi.dart';
import 'package:flutter0322/models.dart';
import 'package:flutter0322/utils/Utils.dart';
//import 'package:color/color.dart';


class RaceResultWidget extends StatelessWidget {
  final double f1 = 1.8;
  final DisplayableRace displayableRace;
  final TextStyle timeStyle = new TextStyle(color: Colors.blueGrey);

  final int zebraIndex;
  RaceResultWidget({Key, key, this.displayableRace, this.zebraIndex: 0})
      : assert(displayableRace != null),
        super(key: key) {}

  @override
  Widget build(BuildContext context) {
    var tcw = new Map<int, TableColumnWidth>();
    tcw[0] = new FractionColumnWidth(0.25);

    // default is zebra stripe (null or complete)
    Color bgColor=       globals.globalDerby.myColors.stripeColor(zebraIndex, context);

    switch (displayableRace.getRaceMetaData().phaseStatus) {
      case PhaseStatus.complete:
        //default
        break;
      case PhaseStatus.error:
        bgColor = globals.globalDerby.myColors.errorBg;
        break;
      case PhaseStatus.pending:
        bgColor = const Color(0xFF7cfc00);
        break;
    }
    print("rrw: status: ${displayableRace.getRaceMetaData().phaseStatus} bg: $bgColor");

    Widget tableWidget = new Table(
        columnWidths: tcw,
        //border: new TableBorder.all(),
        children: getChildTableRows(context,displayableRace));

    if (displayableRace.getDeleteAction()==null && displayableRace.getEditAction()==null) {
      return new Card(child: tableWidget, color: bgColor);
    } else {
      return Utils.getSlidable(context, (context)=>new Card(child: tableWidget, color: bgColor), //
        onDelete: displayableRace.getDeleteAction(),
        onEdit: displayableRace.getEditAction(),
      );
    }
  }

  static Widget getFinishFlagWidget() {
    if (true) {
      return _getFixedFFWidget();
    }
    return new DecoratedBox(
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("images/finish_flag.png"))));
  }

  static Widget _getFixedFFWidget() {
    return const Image(
        width: 60.0,
        height: 60.0,
        fit: BoxFit.scaleDown,
        image: const AssetImage("images/finish_flag.png"));
  }
  static Widget getFixedAWidget() {

    return const Image(
        width: 60.0,
        height: 60.0,
        fit: BoxFit.scaleDown,
        image: const AssetImage("images/letter_a_lg_icon.png"));
  }
  static Widget getFixedBWidget() {

    return const Image(
        width: 60.0,
        height: 60.0,
        fit: BoxFit.scaleDown,
        image: const AssetImage("images/letter_b_lg_icon.png"));
  }
  List<TableRow> getChildTableRows(BuildContext context,DisplayableRace displayableRace) {
    Map<int, Racer> driverMap = globals.globalDerby.getRacerCache();
    var rc = new List<TableRow>();

    if (displayableRace
        .getRaceMetaData()
        .chartPosition != null) {
      String cp = displayableRace
          .getRaceMetaData()
          .chartPosition;
      String rbn = displayableRace
          .getRaceMetaData()
          .raceBracketName;
      String updateTime = displayableRace
          .getRaceMetaData()
          .raceUpdateTime;
      updateTime = (updateTime == null) ? "" : updateTime;
      rbn = (rbn == null) ? "" : rbn;
      rc.add(new TableRow(children: <Widget>[
        new Text(
          updateTime,
          style: timeStyle,
        ),
        new Text("Heat: $cp $rbn"),
      ]));
    }
    var resultsSummary = new ResultsSummary();
    displayableRace.getResultsSummary(resultsSummary);

    // race level messages (as opposed to driver level)
    for (String raceMessage in resultsSummary.getMessages(null)) {
      rc.add(new TableRow(children: <Widget>[
        new Text(""),
        new Text("$raceMessage"),
      ]));
    }
    List<String> alternateText = ["", ""];
    List<int> carNumbers =
    displayableRace.getCarNumbers(alternateText: alternateText);
    List<ContextClickHandler> clickHandlers = displayableRace
        .getCarNumberClickHandlers();
    print("AlternateText: $alternateText");
    for (int x = 0; x < carNumbers.length; x++) {
      var driverName = "";
      if (alternateText[x] != null && alternateText[x].length > 0) {
        driverName = alternateText[x];
      } else {
        driverName = driverMap[carNumbers[x]]?.racerName;
      }

      var iconWidget = resultsSummary.getIcon(carNumbers[x]);
      if (iconWidget == null) {
        iconWidget = new Text("");
      }

      Widget plainDriverResultWidget = new DriverResultWidget(
          driverName: driverName,
          carNumber: carNumbers[x],
          supplementalText: resultsSummary.getMessages(carNumbers[x]));

      Widget clickableDriverResultWidget = plainDriverResultWidget;
      if (clickHandlers != null && clickHandlers[x] != null) {
        clickableDriverResultWidget = GestureDetector(
            onTap: () {
              clickHandlers[x](context, displayableRace);
            },
            onLongPress: () {
              //handleLongPress(context);
            },
            child: plainDriverResultWidget
        );
      }
      //print("raceEntry car:"+raceEntry.carNumber.toString());
      //print("raceEntry driver:"+driverMap.values.toString());
      //print("raceEntry driver:"+driverName);
      //resultsSummary[raceEntry].add("foo");
      rc.add(new TableRow(children: <Widget>[
        //new Text("foo"),
        iconWidget,

        clickableDriverResultWidget,
      ]));
    }


    return rc;
  }

}

class DriverResultWidget extends StatelessWidget {
  final String driverName;
  final int carNumber;
  final List<String> supplementalText;
  const DriverResultWidget(
      {Key, key, this.carNumber, this.driverName, this.supplementalText})
      : assert(carNumber != null),
        super(key: key);
/*
  DriverResultWidget.fromRacePhase(RacePhase rp): this.carNumber{

  }
  */
  @override
  Widget build(BuildContext context) {
    double f1 = 1.8;
    var safeDriverName = driverName;

    if (safeDriverName == null) {
      safeDriverName = "";
    }

    String safeCarNumber = carNumber.toString();
    if (carNumber >= 9000 && carNumber <= 10000) {
      safeCarNumber = "Bye";
    }
    if (carNumber == null || carNumber < 0) {
      safeCarNumber = "";
    }

    //var rowWidgets=[];  //This used to work?!?!?
    List<Widget> rowWidgets = [];
    rowWidgets.add(new Row(children: <Widget>[
      new Text(
        safeCarNumber,
        textAlign: TextAlign.left,
        style: new TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 3.0,
      ),
      new Padding(
          padding: new EdgeInsets.all(18.0),
          child: new Text(safeDriverName,
              textAlign: TextAlign.left, textScaleFactor: f1)),
    ]));
    for (String supText in this.supplementalText) {
      rowWidgets.add(new Row(children: <Widget>[
        new Text(
          supText,
          textAlign: TextAlign.left,
          textScaleFactor: f1,
        ),
      ]));
    }

    return new Column(
      children: rowWidgets,
    );
  }
}
