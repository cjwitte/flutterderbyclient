import 'package:flutter/material.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/models.dart';
import 'package:flutter0322/shared/network/DerbyRestClient.dart';
import 'package:flutter0322/utils/Utils.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class RacerWidget extends StatelessWidget {
  final double f1 = 1.8;
  final Racer racer;
  final Color bgColor;

  RacerWidget({Key, key, this.racer, this.bgColor})
      : assert(racer != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    if (globals.globalDerby.loginCredentials.canEditRacers()) {
      return Utils.getSlidable(
        context, getSliderContent, //
        onDelete: (context) {
          _showDeleteDialog(context, "message");
        }, //
        onEdit: (context) {
          Navigator.of(context)
              .pushNamed("/RacerEditWidget", arguments: this.racer);
        }, //
      );

    } else {
      return getSliderContent(context);
    }
  }

  static showSnackBar(String msg, BuildContext context) {
    print("msg from snackbar: $msg");
    final snackBar = SnackBar(content: Text(msg));
    Scaffold.of(context).removeCurrentSnackBar();

    Scaffold.of(context).showSnackBar(snackBar);
    // _showDialog(context,msg);
  }

// user defined function
  void _showDeleteDialog(BuildContext context, String msg) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Delete Racer?"),
          content: new Text("Delete ${racer.carNumber}: ${racer.racerName}"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Delete"),
              onPressed: () {
                new PostAddBlocks()
                    .postForm({"id": racer.id.toString()}, "/rmRacer");

                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Keep"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget getSliderContent(BuildContext context) {
    return new Container(
        color: bgColor,
        child: new Row(
          children: <Widget>[
            new Padding(
                padding: new EdgeInsets.all(18.0),
                child: new Text(racer.carNumber.toString(),
                    textAlign: TextAlign.left,
                    textScaleFactor: 3.0,
                    style: new TextStyle(fontWeight: FontWeight.bold))),
            new Padding(
                padding: new EdgeInsets.all(18.0),
                child: new Text(racer.racerName,
                    textAlign: TextAlign.left, textScaleFactor: f1)),
          ],
        ));
  }

}
// user defined function
