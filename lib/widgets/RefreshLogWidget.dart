import 'package:flutter/material.dart';
import 'package:flutter0322/models.dart';

class RefreshLogWidget extends StatelessWidget {
  final RefreshLog refreshLog;
  final Color bgColor;
  RefreshLogWidget({Key, key, this.refreshLog,this.bgColor})
      : assert(refreshLog != null),
        super(key: key) ;
  @override
  Widget build(BuildContext context) {
    //print("RacerWidget: ${racer.toJson()}");
    return new Container(
        color: bgColor,
        child: new Row(
          children: <Widget>[
            new Padding(
                padding: new EdgeInsets.all(18.0),
                child: new Text(refreshLog.httpStatus.toString(),
                    textAlign: TextAlign.left,
                    textScaleFactor: 1.0,
                    style: new TextStyle(fontWeight: FontWeight.bold))),
            new Padding(
                padding: new EdgeInsets.all(18.0),
                child:
                new Column(
                  children: <Widget>[
    new Text(refreshLog.toStringData(),
    textAlign: TextAlign.left, textScaleFactor: 1.0),
                    new Text(refreshLog.msg)
                  ],
                )),
          ],
        ));
  }
}