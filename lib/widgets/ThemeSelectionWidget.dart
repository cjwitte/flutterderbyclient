import 'package:flutter/material.dart';
import 'package:flutter0322/research/MyColors.dart';

class ThemeSelectionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('Select Theme'),
      ),
      body: new ListView(children: getThemeButtons(context)),
    );
  }
 // }

  List<Widget> getThemeButtons(BuildContext context) {
    List<Widget> rc = [];

    MyColors.allowedColors.forEach((mc) {
      print( "allowedColorFound: $mc");
      /*
      ThemeData ntd = new ThemeData(
        primaryColor: mc,
        backgroundColor: mc[200],
      );

      rc.add(new Theme(
          data: ntd,
          child: new FlatButton(
              onPressed: () {

              },
              child: new Text(
                'FooButton',
              ))));
       */
      rc.add(new FlatButton(
          onPressed: () {
            print ("picked color $mc");
            new MyColors(mc).applyColorThemeFromMyColor( context);
            //Navigator.of(context).pop();

          },
          child: new Text('Use This Color',
              style: Theme.of(context)
                  .textTheme
                  .subhead
                  .copyWith(color: mc))));

    });



    return rc;
  }

  static showThemeDialog(BuildContext context, {String qrString}) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new ThemeSelectionWidget();
        },
        fullscreenDialog: false));
  }

}
