/// Example of a stacked area chart.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class StackedAreaLineChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  StackedAreaLineChart(this.seriesList, {this.animate});

  /// Creates a [LineChart] with sample data and no transition.
  factory StackedAreaLineChart.withSampleData() {
    return new StackedAreaLineChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }


  @override
  Widget build(BuildContext context) {
    new Column();
    var cc= new charts.LineChart(seriesList,
        defaultRenderer:
        new charts.LineRendererConfig(areaOpacity:1.0,includeArea: true, stacked: false, includePoints: true),
        animate: animate);
    if (false)return new Column(
      children: <Widget>[new Text("foo"), cc],
    );
    return cc;
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<LinearSales, int>> _createSampleData() {
    final myFakeDesktopData = [
      new LinearSales(0, 0),
      new LinearSales(320, 0),
      new LinearSales(321, 1),
      new LinearSales(620, 1),
      new LinearSales(621, 0),
      new LinearSales(1100, 0),
      new LinearSales(1101, 1),
      new LinearSales(1102, 0),

      new LinearSales(19000, 0),
    ];

    var myFakeTabletData = [
      new LinearSales(0, 0),
      new LinearSales(311, 0),

      new LinearSales(312, -1),
      new LinearSales(620, -1),
      new LinearSales(621, 0),
      new LinearSales(19000, 0),
    ];


    return [
      new charts.Series<LinearSales, int>(
        id: 'Lane1',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: myFakeDesktopData,
      ),
      new charts.Series<LinearSales, int>(
        id: 'Lane2',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: myFakeTabletData,
      ),

    ];
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}