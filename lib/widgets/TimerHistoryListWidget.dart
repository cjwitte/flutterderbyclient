import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/FinishResult.dart';
import 'package:flutter0322/autoTimer/SimplePinSummary.dart';
import 'package:flutter0322/autoTimer/models/Constants.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/models.dart';
import 'package:flutter0322/network/GetS3Object.dart';
import 'package:flutter0322/shared/StaticShared.dart';
import 'package:intl/intl.dart';
class TimerHistoryListWidget{
  final List<FinishDetailMap> finishDetailMapList;
  final StreamController<List<FinishDetailMap>> repaintWidget;
   TimerHistoryListWidget(this.repaintWidget,this.finishDetailMapList){

    //FinishDetailMap rc0 = new FinishDetailMap.fromJson(json.decode(fmCars0));
    //FinishDetailMap rc1 = new FinishDetailMap.fromJson(json.decode(fmCars1));
/*
    for(int x=0;x<3;x++){
      finishDetailMapList.add(rc0);
      finishDetailMapList.add(rc1);
    }
    */

     _getFdmData(); // checking for updates..
  }
  List<FinishDetailMap> _getHardCoded(){
    final String fm5='{"finishDetailMap":{"24":{"pinNumber":"24","carSegmentList":[{"noseTime":50000500,"tailTime":50003020},{"noseTime":50003030,"tailTime":50003031}]},"23":{"pinNumber":"23","carSegmentList":[{"noseTime":50000606,"tailTime":50002106},{"noseTime":50002116,"tailTime":50002216}]}},"historyBuffer":{"pinHistoryMap":{"24":{"pinList":[{"micros":20350152,"pinNumber":"24","pinState":false},{"micros":50000500,"pinNumber":"24","pinState":true},{"micros":50003020,"pinNumber":"24","pinState":false},{"micros":50003030,"pinNumber":"24","pinState":true},{"micros":50003031,"pinNumber":"24","pinState":false}],"pinNumber":"24"},"23":{"pinList":[{"micros":20351560,"pinNumber":"23","pinState":false},{"micros":50000606,"pinNumber":"23","pinState":true},{"micros":50002106,"pinNumber":"23","pinState":false},{"micros":50002116,"pinNumber":"23","pinState":true},{"micros":50002216,"pinNumber":"23","pinState":false}],"pinNumber":"23"},"clock":{"pinList":[{"micros":45000001,"pinNumber":"clock","pinState":true},{"micros":46000001,"pinNumber":"clock","pinState":false},{"micros":47000001,"pinNumber":"clock","pinState":true},{"micros":48000001,"pinNumber":"clock","pinState":false},{"micros":49000001,"pinNumber":"clock","pinState":true},{"micros":50000001,"pinNumber":"clock","pinState":false}],"pinNumber":"clock"}}}}';
    final String fmCars0='{"finishDetailMap":{"23":{"pinNumber":"23","carSegmentList":[{"noseTime":1553877370007339,"tailTime":1553877370358839}],"carNumber":"881"},"24":{"pinNumber":"24","carSegmentList":[{"noseTime":1553877370007329,"tailTime":1553877370357431}],"carNumber":"882"}},"historyBuffer":{"pinHistoryMap":{"23":{"pinList":[{"micros":1,"pinNumber":"23","pinState":true},{"micros":1553877370007339,"pinNumber":"23","pinState":true},{"micros":1553877370358839,"pinNumber":"23","pinState":false}],"pinNumber":"23"},"24":{"pinList":[{"micros":1,"pinNumber":"24","pinState":true},{"micros":1553877370007329,"pinNumber":"24","pinState":true},{"micros":1553877370357431,"pinNumber":"24","pinState":false}],"pinNumber":"24"}}}}';
    final String fmCars1='{"finishDetailMap":{"23":{"pinNumber":"23","carSegmentList":[{"noseTime":1553877370007339,"tailTime":1553877370358839}]},"24":{"pinNumber":"24","carSegmentList":[{"noseTime":1553877370007329,"tailTime":1553877370357431}],"carNumber":"886"}},"historyBuffer":{"pinHistoryMap":{"23":{"pinList":[{"micros":1,"pinNumber":"23","pinState":true},{"micros":1553877370007339,"pinNumber":"23","pinState":true},{"micros":1553877370358839,"pinNumber":"23","pinState":false}],"pinNumber":"23"},"24":{"pinList":[{"micros":1,"pinNumber":"24","pinState":true},{"micros":1553877370007329,"pinNumber":"24","pinState":true},{"micros":1553877370357431,"pinNumber":"24","pinState":false}],"pinNumber":"24"}}}}';
    final List<String> fj=[];
    fj.add('{"finishDetailMap":{"23":{"pinNumber":"23","carSegmentList":[{"noseTime":1553985130019144,"tailTime":1553985130370644}]},"24":{"pinNumber":"24","carSegmentList":[{"noseTime":1553985130019134,"tailTime":1553985130369236}]}},"finishEpoch":1553985130102,"historyBuffer":{"pinHistoryMap":{"23":{"pinList":[{"micros":1,"pinNumber":"23","pinState":true},{"micros":1553985130019144,"pinNumber":"23","pinState":true},{"micros":1553985130370644,"pinNumber":"23","pinState":false}],"pinNumber":"23"},"24":{"pinList":[{"micros":1,"pinNumber":"24","pinState":true},{"micros":1553985130019134,"pinNumber":"24","pinState":true},{"micros":1553985130369236,"pinNumber":"24","pinState":false}],"pinNumber":"24"}}}}');
    fj.add('{"finishDetailMap":{"23":{"pinNumber":"23","carSegmentList":[{"noseTime":1553985159977976,"tailTime":1553985160328078}]},"24":{"pinNumber":"24","carSegmentList":[{"noseTime":1553985159977986,"tailTime":1553985160329486}]}},"finishEpoch":1553985160061,"historyBuffer":{"pinHistoryMap":{"23":{"pinList":[{"micros":1,"pinNumber":"23","pinState":true},{"micros":1553985159977976,"pinNumber":"23","pinState":true},{"micros":1553985160328078,"pinNumber":"23","pinState":false}],"pinNumber":"23"},"24":{"pinList":[{"micros":1,"pinNumber":"24","pinState":true},{"micros":1553985159977986,"pinNumber":"24","pinState":true},{"micros":1553985160329486,"pinNumber":"24","pinState":false}],"pinNumber":"24"}}}}');
    fj.add('{"finishDetailMap":{"23":{"pinNumber":"23","carSegmentList":[{"noseTime":1553985189950250,"tailTime":1553985190301750}]},"24":{"pinNumber":"24","carSegmentList":[{"noseTime":1553985189950240,"tailTime":1553985190300342}]}},"finishEpoch":1553985190022,"historyBuffer":{"pinHistoryMap":{"23":{"pinList":[{"micros":1,"pinNumber":"23","pinState":true},{"micros":1553985189950250,"pinNumber":"23","pinState":true},{"micros":1553985190301750,"pinNumber":"23","pinState":false}],"pinNumber":"23"},"24":{"pinList":[{"micros":1,"pinNumber":"24","pinState":true},{"micros":1553985189950240,"pinNumber":"24","pinState":true},{"micros":1553985190300342,"pinNumber":"24","pinState":false}],"pinNumber":"24"}}}}');
    fj.add('{"finishDetailMap":{"23":{"pinNumber":"23","carSegmentList":[{"noseTime":1553985219911945,"tailTime":1553985220262047}]},"24":{"pinNumber":"24","carSegmentList":[{"noseTime":1553985219911955,"tailTime":1553985220263455}]}},"finishEpoch":1553985219997,"historyBuffer":{"pinHistoryMap":{"23":{"pinList":[{"micros":1,"pinNumber":"23","pinState":true},{"micros":1553985219911945,"pinNumber":"23","pinState":true},{"micros":1553985220262047,"pinNumber":"23","pinState":false}],"pinNumber":"23"},"24":{"pinList":[{"micros":1,"pinNumber":"24","pinState":true},{"micros":1553985219911955,"pinNumber":"24","pinState":true},{"micros":1553985220263455,"pinNumber":"24","pinState":false}],"pinNumber":"24"}}}}');
    finishDetailMapList.clear();

    fj.forEach((j){
      finishDetailMapList.add(new FinishDetailMap.fromJson(json.decode(j)));
    });
  }
  Future<void> _getFdmData() async{
    print ("_getFdmData begin");

    SyncS3TimerLog syncS3TimerLog=new SyncS3TimerLog();

    final RefreshLog refreshLog=new RefreshLog();
    refreshLog.fileName=syncS3TimerLog.baseFileName;

    File ndjsonFile2 = await syncS3TimerLog.resumeS3ObjectAsFile(refreshLog);
    print ("_getFdmData await1 complete");

    // re-parse the whole file!
    await syncS3TimerLog.parseTimerFile(await syncS3TimerLog.ndJsonFile());
    print ("_getFdmData await2 complete");
    print ("_getFdmData new fdm size: ${syncS3TimerLog.finishDetailMapList.length} old: ${this.finishDetailMapList.length} ");

    if(this.finishDetailMapList.length == syncS3TimerLog.finishDetailMapList.length){
      print ("_getFdmData new fdm size: NO CHANGE");
    }
    else {
      repaintWidget.add(List.unmodifiable(syncS3TimerLog.finishDetailMapList.reversed));
    }
  }
  Widget getTimerListBody(BuildContext context) {
    print ("dlist source: ${finishDetailMapList}\n");
    print ("dlist source length: ${finishDetailMapList.length}\n");

     List<dynamic>dlist=List<dynamic>.from(finishDetailMapList, growable: true);
     dlist.insert(0, "header");
     print ("dlist length: ${dlist.length}\n");
    return RefreshIndicator(
        onRefresh: _getFdmData,
        child: new ListView.builder(
          itemCount: dlist.length,

          itemBuilder: (context, index) {
            Color bg=globals.globalDerby.myColors.stripeColor(index, context);
            print ("dlist index: ${index}\n");

            if(dlist[index] is FinishDetailMap) {
              print ("dlist item.\n");

              return new TimerHistoryWidget(
                finishDetailMap: dlist[index],
                bgColor: bg,
              );
            }
            else{
              print ("dlist header: ${index}\n");

              //return new Text("header");
              final BoxDecoration _boxDeco = new BoxDecoration(
                  border: new Border(bottom: Divider.createBorderSide(context, width: 1.0)),
                  color: bg

              );
              return new Table(
                children: [new TableRow(
                  // decoration stacktrace on refresh
               //   decoration: _boxDeco,
                  children: [new Text(""),new Text("Winning\nMS"), new Text ("Lane[Car]"), new Text("Elapsed\nMS")])],);
            }
          },
        ));
  }
}
class TimerHistoryWidget extends StatelessWidget {
  final double f1 = 1.0;
  final FinishDetailMap finishDetailMap;
  final Color bgColor;

  TimerHistoryWidget({Key, key, this.finishDetailMap, this.bgColor})
      : assert(finishDetailMap != null),
        super(key: key){
  }
  @override
  Widget build(BuildContext context) {


    return new GestureDetector(
        onTap: () {
          //handleTap(context);
        },
        onLongPress: () {
          //handleLongPress(context);
        },
        child: new Table(
              children: getFinishRowWidgetCells(context,finishDetailMap, bgColor)
            ));
  }

  List<TableRow> getFinishRowWidgetCells(BuildContext context,FinishDetailMap finishDetailMap, Color bgColor){
    print("getFinishRowWidgetCells");
    List<TableRow> rc=[];
    List<FinishResult> finishResultList=finishDetailMap.getFinishResults();

    final BoxDecoration _boxDeco = new BoxDecoration(
        border: new Border(bottom: Divider.createBorderSide(context, width: 1.0)),
        color: bgColor

    );

    String finishEpochHms="hh:mm:ss";
    if(finishDetailMap.finishEpoch!=null){
      var dt=DateTime.fromMillisecondsSinceEpoch(finishDetailMap.finishEpoch);
      finishEpochHms=new DateFormat.Hms().format(dt);
    }
    rc.add(new TableRow(
        decoration:_boxDeco,children: [new Text(finishEpochHms),new Container(),new Container(),new Container() ]));
    finishResultList.forEach((finishResult){
      rc.add(getFinishRowWidget(context,finishResult, _boxDeco));
    });
    return rc;

  }
  TableRow getFinishRowWidget(BuildContext context,FinishResult fr,BoxDecoration _boxDeco){
    String winningTime="   ";
    var fw=FontWeight.normal;

    if(fr.victoryMarginMicros!=null){
      winningTime=SimplePinSummary.microsToMillisString(fr.victoryMarginMicros);
      fw=FontWeight.bold;
    }
    String carNumber="---";
    if(fr.finishDetail.carNumber!=null){
      carNumber=fr.finishDetail.carNumber;
    }
    String carAndLane="${lanesByPinMap[fr.pinNumber]} [$carNumber]";
    //String carAndLane="Lane:${fr.pinNumber} [$carNumber]";
    String elapsedMsString=SimplePinSummary.microsToMillisString(fr.elapsedMicros);

    String validSuffix=fr.validDerbyRace?":-)":"!";
    return new TableRow(
      decoration: _boxDeco,
      children: <Widget>[
        new Container(),
        new Padding(
            padding: new EdgeInsets.all(1.0),
            child: new Text( winningTime,
                textAlign: TextAlign.left,
                textScaleFactor: f1,
                style: new TextStyle(fontWeight: fw))),
        /*
        new Padding(
            padding: new EdgeInsets.all(1.0),
            child: new Text("Lane:${fr.pinNumber}",
                textAlign: TextAlign.left, textScaleFactor: f1)),
                */
        new Text(carAndLane,style: new TextStyle(fontWeight: fw)),
        new Padding(
            padding: new EdgeInsets.all(1.0),
            child: new Text("$elapsedMsString $validSuffix",
                textAlign: TextAlign.left, textScaleFactor: f1)),
      ],
    );

  }


}
