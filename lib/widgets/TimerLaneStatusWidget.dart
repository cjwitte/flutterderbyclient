import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter0322/DerbyNavDrawer.dart';
import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:flutter0322/widgets/TimerHistoryListWidget.dart';
import 'package:flutter0322/globals.dart' as globals;

/// Example of a stacked area chart.

class TimerLaneStatusWidget extends StatelessWidget {
  final style = new TextStyle(fontWeight: FontWeight.bold, color: Colors.green);
  StreamController<TimerLaneStatusModel> tlsStreamController;
  Stream<TimerLaneStatusModel> tlsStream;

  Timer refreshTimer;
  Random rnd2 = new Random(new DateTime.now().millisecondsSinceEpoch);

  TimerLaneStatusModel lastKnownModel=null;
  TimerLaneStatusWidget() {
    refreshTimer=Timer.periodic(new Duration(milliseconds: 100),refreshTimerBody);
    tlsStreamController =
        new StreamController<TimerLaneStatusModel>(
            onPause: stopTimer,
            onCancel: stopTimer
        );
    tlsStream = tlsStreamController.stream;
  }

  void refreshTimerBody(Timer x){
    int now=new DateTime.now().millisecondsSinceEpoch;
    if(getCurrentTls().uiShouldRepaint(now)) {
      print("TimerLaneStatusWidget refreshTimerBody isStale: $now");

      tlsStreamController.add(getCurrentTls());
    }
  }
  void stopTimer() {
    print("STOP Timer!");
    if (refreshTimer != null) {
      refreshTimer.cancel();
      refreshTimer = null;
    }
  }
  @override
  Widget build(BuildContext context) {
    return getStatusWidget(context);
  }



  Widget getStatusWidget(BuildContext conext) {
    return new StreamBuilder<TimerLaneStatusModel>(
        stream: tlsStream,
        initialData: getCurrentTls(),
        builder: (BuildContext context,
            AsyncSnapshot<TimerLaneStatusModel> asyncSnapshot) {
          if (asyncSnapshot.hasError) {
            return new Text("TimerLaneStatus Error!");
          } else if (asyncSnapshot.data == null) {
            return new Text("TimerLaneStatus Null Data");
          } else {
            return buildStatus(context, asyncSnapshot.data);
          }
        });
  }
  TimerLaneStatusModel getCurrentTls() {

    return globals.globalDerby.timerLaneStatusModel;

  }

  TimerLaneStatusModel getRandomTls() {
    TimerLaneStatusModel tls = new TimerLaneStatusModel();

    tls.recentRefreshMs = 0;

    for (int x = 0; x < 2; x++) {
      tls.laneStates.add(getRandState(rnd2));
    }

    return tls;
  }

  Widget buildStatus(BuildContext context, TimerLaneStatusModel data) {
    List<Widget> buttons = [];

    int laneNumber = 1;
    bool isError = false;
    final numStyle = new TextStyle(fontWeight: FontWeight.bold, fontSize: 42);

    data.checkExpired();
    data.laneStates.forEach((laneState) {
      buttons.add(new Container(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 2.0),
          child: new RaisedButton.icon(
              onPressed: () {},
              color: getButtonColor(laneState),
              label: new Text(laneNumber.toString(), style: numStyle),
              icon: new Text(""))));

      if (laneState != LaneState.Ready) {
        isError = true;
      }
      laneNumber++;
    });
    List<Widget> cKids = [

      new Row(children: buttons)
    ];
    if (isError) {
      cKids.add(getHalt2());
    }
    lastKnownModel=data;

    return new Column(
      children: cKids,
    );
  }

  static Widget getHalt2() {
    return Container(
        decoration: BoxDecoration(
          color: Colors.blueGrey,
        ),
        child: getHaltWidget());
  }

  static Widget getHaltWidget() {
    //const String iFile="images/finish_flag.png";
    const String iFile = "images/halt.png";
    return const Image(
        width: 200.0,
        height: 200.0,
        fit: BoxFit.scaleDown,
        image: const AssetImage(iFile));
  }

  Color getButtonColor(LaneState ls) {
    switch (ls) {
      case LaneState.NoSignal:
        return Colors.blueGrey[700];
      case LaneState.Blocked:
        return Colors.redAccent[700];
      case LaneState.Ready:
        return Colors.green[500];
      case LaneState.Rejuvenating:
        return Colors.yellow[700];
    }
    return Colors.black; // shouldn't happen
  }

  LaneState getRandState(Random rnd2) {
    int x = rnd2.nextInt(10);
    if (x == 0) return LaneState.NoSignal;
    if (x == 1) return LaneState.Blocked;
    if (x == 2) return LaneState.Rejuvenating;
    return LaneState.Ready;
  }
}

enum LaneState { NoSignal, Ready, Blocked, Rejuvenating }

class TimerLaneStatusModel {
  //TODO: hardcoded lanecount of 2!?
  List<LaneState> laneStates = [LaneState.NoSignal,LaneState.NoSignal];
  Map<String,PinChange>pcMap={};
  int recentRefreshMs=0;
  int lastUiPoll=0;
  int recentPcMicros=0;  // micros received when recentRefreshMs is updated.

  void checkExpired(){
    if(_isExpired()){
      for(int x=0;x<laneStates.length;x++){
        laneStates[x]=LaneState.NoSignal;
      }
    }
  }
  bool uiShouldRepaint(int pollMs){
    bool rc=(lastUiPoll< recentRefreshMs);
    lastUiPoll=pollMs;
    return rc;
  }
  bool _isExpired(){
    int nowMs=new DateTime.now().millisecondsSinceEpoch;
    return((nowMs-recentRefreshMs )> 20000);
  }
  void refreshPinChange(PinChange pc) {
    recentRefreshMs=new DateTime.now().millisecondsSinceEpoch;
    recentPcMicros=pc.micros;


    // experimental: if timer epoch is trustworthy, this is a reasonable filter.
    //    old/cached data from retained mq messages shouldn't be rendered as viable.
    int age=recentRefreshMs - pc.pubTime;
    if(age >60000){
      print("refreshPinChange forceExpired: $age");
      recentRefreshMs=-1; // forceExpired
    }
    else{
      print("refreshPinChange forceExpired: OK $age");
    }
    pcMap[pc.pinName]=pc;


    _recalcLaneStatus();
  }
  void _recalcLaneStatus(){
    pcMap.forEach((pinName,pinChange){
      int laneInt=-1;
      if(pinName.startsWith("lane") && pinName.endsWith("1")){ laneInt=0;}
      if(pinName.startsWith("lane") && pinName.endsWith("2")){ laneInt=1;}
      if(pinName.startsWith("lane") && pinName.endsWith("3")){ laneInt=2;}
      if(laneInt>=0){
        if((!pinChange.pinState) && (pinChange.micros +5000000 )>recentPcMicros){
          laneStates[laneInt]=LaneState.Rejuvenating;
          return;
        }
        laneStates[laneInt]=pinChange.pinState?LaneState.Blocked:LaneState.Ready;
      }
    });
  }
}
class OneLaneStatusModel {
  int laneNumber;
  bool laneState;
}
