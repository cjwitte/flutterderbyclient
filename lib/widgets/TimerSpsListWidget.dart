import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:flutter0322/autoTimer/SimplePinSummary.dart';
import 'package:flutter0322/globals.dart' as globals;
import 'package:flutter0322/models.dart';
import 'package:flutter0322/network/GetS3Object.dart';
import 'package:intl/intl.dart';
class TimerSpsListWidget{
  final List<SimplePinSummary> spsList;
  final StreamController<List<SimplePinSummary>> repaintWidget;
  TimerSpsListWidget(this.repaintWidget,this.spsList){



    _getPinChangeData(); // checking for updates..
  }
  Future<void> _getPinChangeData() async{
    print ("_getPinChangeData begin");

    SyncS3PinChangeLog syncS3PinChangeLog=new SyncS3PinChangeLog();

    final RefreshLog refreshLog=new RefreshLog();
    refreshLog.fileName=syncS3PinChangeLog.baseFileName;

    File ndjsonFile2 = await syncS3PinChangeLog.resumeS3ObjectAsFile(refreshLog);
    print ("_getPinChangeData await1 complete");

    // re-parse the whole file!
    await syncS3PinChangeLog.parsePinChangeFile(await syncS3PinChangeLog.ndJsonFile());
    print ("_getPinChangeData await2 complete");
    print ("_getPinChangeData new pcl size: ${syncS3PinChangeLog.pinChangeList.length} old: ${this.spsList.length} ");

    if(this.spsList.length == syncS3PinChangeLog.spsList.length){
      print ("_getPinChangeData new pcl size: NO CHANGE");
    }
    else {
      List<SimplePinSummary>spsList=syncS3PinChangeLog.spsList;
      repaintWidget.add(List.unmodifiable(spsList.reversed));
      //repaintWidget.add(spsList);

    }
  }
  Widget getTimerSpsListBody(BuildContext context) {
    print ("dlist source: ${spsList}\n");
    print ("dlist source length: ${spsList.length}\n");

    List<dynamic>dlist=List<dynamic>.from(spsList, growable: true);
    dlist.insert(0, "header");
    print ("dlist length: ${dlist.length}\n");
    return RefreshIndicator(
        onRefresh: _getPinChangeData,
        child: new ListView.builder(
          itemCount: dlist.length,

          itemBuilder: (context, index) {
            Color bg=globals.globalDerby.myColors.stripeColor(index, context);
            print ("dlist index: ${index}\n");

            if(dlist[index] is SimplePinSummary) {
              print ("dlist item.\n");

              return new TimerSpsWidget(
                simplePinSummary: dlist[index],
                bgColor: bg,
              );
            }
            else{
              print ("dlist header: ${index}\n");

              //return new Text("header");
              final BoxDecoration _boxDeco = new BoxDecoration(
                  border: new Border(bottom: Divider.createBorderSide(context, width: 1.0)),
                  color: bg

              );
              return new Table(
                children: [new TableRow(
                  // decoration stacktrace on refresh
                  //   decoration: _boxDeco,
                    children: [new Text(""),new Text("Winning\nMS"), new Text ("Lane[Car]"), new Text("Elapsed\nMS")])],);
            }
          },
        ));
  }
}
class TimerSpsWidget extends StatelessWidget {
  final double f1 = 1.0;
  final SimplePinSummary simplePinSummary;
  final Color bgColor;

  TimerSpsWidget({Key, key, this.simplePinSummary, this.bgColor})
      : assert(simplePinSummary != null),
        super(key: key){
  }
  @override
  Widget build(BuildContext context) {


    return new GestureDetector(
        onTap: () {
          //handleTap(context);
        },
        onLongPress: () {
          //handleLongPress(context);
        },
        child: new Table(
            children: getFinishRowWidgetCells(context,simplePinSummary, bgColor)
        ));
  }

  List<TableRow> getFinishRowWidgetCells(BuildContext context,SimplePinSummary simplePinSummary, Color bgColor){
    print("getFinishRowWidgetCells");
    List<TableRow> rc=[];

    final BoxDecoration _boxDeco = new BoxDecoration(
        border: new Border(bottom: Divider.createBorderSide(context, width: 1.0)),
        color: bgColor

    );

    String finishEpochHms="hh:mm:ss";
    if(simplePinSummary.latestPubTime!=null){
      var dt=DateTime.fromMillisecondsSinceEpoch(simplePinSummary.latestPubTime);
      finishEpochHms=new DateFormat.Hms().format(dt);
    }
    rc.add(new TableRow(
        decoration:_boxDeco,children: [new Text(finishEpochHms),new Container(),new Container(),new Container() ]));
    rc.add(getSpsRowWidget(context,simplePinSummary,"lane1", _boxDeco));
    rc.add(getSpsRowWidget(context,simplePinSummary,"lane2", _boxDeco));

    return rc;

  }
  final  threeDigits = new NumberFormat("000", "en_US");

  final ReTrailingZero=new RegExp(r'000$');
  final ReLeadingZero=new RegExp(r'^0:00:');
  TableRow getSpsRowWidget(BuildContext context,SimplePinSummary simplePinSummary,String laneString,BoxDecoration _boxDeco){
    String winningTime="   ";
    var fw=FontWeight.normal;

    int carlenMs=null;
    Duration winDuration=null;
    if(laneString=="lane1"){
      carlenMs=simplePinSummary.l1lengthMs;
    }
    if(laneString=="lane2"){
      carlenMs=simplePinSummary.l2lengthMs;
    }
    if(simplePinSummary.winLane==laneString){
      winDuration=new Duration(milliseconds: simplePinSummary.winMs);
    }

    // don't bother to display time for duration over a minute long.
    if(winDuration!=null && winDuration.inMinutes<1){
      //winningTime=threeDigits.format(winMs)+"MS";
      winningTime=winDuration.toString();
      winningTime=winningTime.replaceAll(ReTrailingZero, "");
      winningTime=winningTime.replaceAll(ReLeadingZero, "");
      fw=FontWeight.bold;
    }
    return new TableRow(
      decoration: _boxDeco,
      children: <Widget>[
        new Container(),
        new Padding(
            padding: new EdgeInsets.all(1.0),
            child: new Text( winningTime,
                textAlign: TextAlign.left,
                textScaleFactor: f1,
                style: new TextStyle(fontWeight: fw))),
        /*
        new Padding(
            padding: new EdgeInsets.all(1.0),
            child: new Text("Lane:${fr.pinNumber}",
                textAlign: TextAlign.left, textScaleFactor: f1)),
                */
        new Text(laneString,style: new TextStyle(fontWeight: fw)),
        new Padding(
            padding: new EdgeInsets.all(1.0),
            child: new Text(threeDigits.format(carlenMs)+"MS",
                textAlign: TextAlign.left, textScaleFactor: f1)),
      ],
    );

  }




}
