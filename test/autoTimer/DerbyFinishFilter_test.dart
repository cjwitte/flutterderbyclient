import 'package:flutter0322/autoTimer/DerbyFinishFilter.dart';
import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/FinishResult.dart';
import 'package:flutter0322/autoTimer/HistoryBuffer.dart';
import 'package:test/test.dart';

import 'TestFixtures.dart';

void main() {
  {

    TestFixtures.testStaticModel();
    test('invalid car (short)', () {
      var cut = new DerbyFinishFilter();
      HistoryBuffer historyBuffer = new HistoryBuffer();
      FinishDetail fd = new FinishDetail(pinNumber: "777");
      fd.addNoseTime(1);
      fd.addTailTime(5);
      FinishResult f1 = new FinishResult(finishDetail: fd);
      //FinishResult f1 = (new FinishResultBuilder()..finishDetail=fd).build();
      expect(f1.validDerbyRace, false);
      var frList = [f1, f1];
      //FinishDetailMap cut = new FinishDetailMap(historyBuffer);
      cut.populateValidDerbyRace(frList);
      expect(f1.validDerbyRace, false);
    });

    test('valid non peforated car ', () {
      var cut = new DerbyFinishFilter();
      HistoryBuffer historyBuffer = new HistoryBuffer();
      FinishDetail fd = new FinishDetail(pinNumber: "777");
      fd.addNoseTime(1);
      fd.addTailTime(1+350000);
      FinishResult f1 = new FinishResult(finishDetail: fd);
      expect(f1.validDerbyRace, false);
      var frList = [f1, f1];
      //FinishDetailMap cut = new FinishDetailMap(historyBuffer);
      cut.populateValidDerbyRace(frList);
      expect(f1.validDerbyRace, true);
    });
    test('valid perforated car ', () {
      var cut = new DerbyFinishFilter();
      HistoryBuffer historyBuffer = new HistoryBuffer();
      FinishDetail fd = new FinishDetail(pinNumber: "777");
      fd.addNoseTime(1);
      fd.addTailTime(10000);
      fd.addNoseTime(11000);
      fd.addTailTime(350002);

      expect(fd.getPerforations(), [1000]);
      expect(new DerbyCarConstraints().isTolerablePerforationList(fd.getPerforations()),true);

      FinishResult f1 = new FinishResult(finishDetail: fd);
      expect(f1.validDerbyRace, false);
      var frList = [f1, f1];
      //FinishDetailMap cut = new FinishDetailMap(historyBuffer);
      cut.populateValidDerbyRace(frList);
      expect(f1.validDerbyRace, true);
    });
  }
}
