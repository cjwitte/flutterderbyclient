import 'dart:convert';

import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/HistoryBuffer.dart';
import 'package:test/test.dart';
//class MockHistory extends Mock implements HistoryBuffer {}

void main() {
  // Mock class

// mock creation
  test('json map empty', () {
    HistoryBuffer historyBuffer=new HistoryBuffer();
    FinishDetailMap cut=new FinishDetailMap(historyBuffer);

    String emptyp=json.encode(cut.toJson());
    print("empty: $emptyp\n");
    //expect({"finishDetailMap":{}, "historyBuffer":new HistoryBuffer()},cut.toJson());

    cut.finishDetailMap["foo"]=new FinishDetail(pinNumber: "77");
    //expect(cut.toJson(), '{"foo":{"pinName":"77","noseTime":null,"tailTime":null,"elapsed":null}}');
    //expect(cut.toJson(), '{"foo":{"pinNumber":"77","carSegmentList":[]}}');
    print( cut.toJson());
  });
  test('json finishDetail', () {
    FinishDetail cut=new FinishDetail(pinNumber: "77");
    cut.addNoseTime(5);
    cut.addTailTime(7);
    cut.addNoseTime(11);
    cut.addTailTime(31);

    expect(cut.toJsonSummary(), {'pinName': '77',  'tailTime': 31, 'elapsed': 26,'noseTime': 5});
   // expect(cut.toJson(), {'pinNumber': '77',  'carSegmentList': [{"noseTime":5,"tailTime":7},{"noseTime":11,"tailTime":31}]});

  });
  test('json finishDetail no tail', () {
    FinishDetail cut=new FinishDetail(pinNumber: "77");
    cut.addNoseTime(5);

    expect(cut.toJsonSummary(),{'pinName': '77', 'noseTime': 5, 'tailTime': null, 'elapsed': null});

  });
  test('json finishDetail no data', () {
    FinishDetail cut=new FinishDetail(pinNumber: "77");

    expect(cut.toJsonSummary(), {'pinName': '77', 'noseTime': null, 'tailTime': null, 'elapsed': null});

  });
}
