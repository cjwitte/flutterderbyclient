import 'dart:convert';
import 'dart:io';

import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/FinishResult.dart';
import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:flutter0322/autoTimer/RawDataBuffer.dart';
import 'package:flutter0322/autoTimer/utils/CompactMap.dart';
import 'package:test/test.dart';

import 'TestFixtures.dart';

void main() {
  TestFixtures.testStaticModel();

  int uint32Max = 0xffffffff;
  test('uint32 normal', () {
    expect(uint32Delta(500, 100), 400);
  });
  test('uint32 wrap', () {
    expect(uint32Delta(2, uint32Max - 9), 12);
  });
  test('uint32 wrap', () {
    expect(uint32Delta(0, uint32Max), 1);
  });
  test('uint32 wrap', () {
    expect(uint32Delta(3, uint32Max - 3), 7);
  });
  test('lane is flickering', () {
    RawDataBuffer cut = new RawDataBuffer(new ExpectedHistory(null));

    bool ps = true;
    for (int x = 0; x < 300; x++) {
      int now = x * 100 * 1000; // 100k
      PinChange pc = new PinChange(micros: now, pinState: ps, pinNumber: "555");
      ps = !ps;
      cut.addJsonData(pc.toJsonString());
      expect(cut.historyBuffer.getNewestTime(), now);
      ;
      expect(true,cut.historyBuffer.pinHistoryMap["555"].isLaneFlickering(newestTime:cut.historyBuffer.getNewestTime()));
      print("newest $now");
    }
  });

  test('lane is stable', () {
    RawDataBuffer cut = new RawDataBuffer(new ExpectedHistory(null));

    bool ps = true;
    for (int x = 0; x < 300; x++) {
      int now = x * 10 * 1000 * 1000; // 10M
      PinChange pc = new PinChange(micros: now, pinState: ps, pinNumber: "555");
      ps = !ps;
      cut.addJsonData(pc.toJsonString());
      expect(cut.historyBuffer.getNewestTime(), now);
      expect(cut.historyBuffer.pinHistoryMap["555"].isLaneReady(newestTime:cut.historyBuffer.getNewestTime()), false);

      if (x > 0) {
        expect(
            cut.historyBuffer.pinHistoryMap["555"].isLaneFlickering(newestTime:cut.historyBuffer.getNewestTime()), false);
        expect(cut.historyBuffer.pinHistoryMap["555"].pinQ.length, 2);
      } else {
        // needs to be primed.  first time thru it s/b flickering.
        expect(cut.historyBuffer.pinHistoryMap["555"].isLaneFlickering(newestTime:cut.historyBuffer.getNewestTime()), true);
        expect(cut.historyBuffer.pinHistoryMap["555"].pinQ.length, 1);
      }
      print("newest $now");
    }
  });

  test('happy race', () {
    TestRace testRace = new TestRace();
    g1(testRace);
    g2(testRace);

    generateClock(testRace);

    ExpectedHistory expectedHistory = new ExpectedHistory(testRace);
    RawDataBuffer cut = new RawDataBuffer(expectedHistory);

    int expectedFinishCount = testRace.expectedFinishResultList.length;
    int finishCount = 0;
    for (PinChange x in testRace.inputPinChangeList) {
      print("happy json: $x");
      String json = x.toJsonString();
      if (cut.addJsonData(json)) {
        finishCount++;
      }
    }
    assert(testRace.expectedFinishResultList.isEmpty,
        "TestRace results were not depleted!");

    FinishDetailMap finishDetailMap = new FinishDetailMap(cut.historyBuffer);
    print("finishCount: $finishCount\n");
    expect(finishCount, expectedFinishCount);
    expect(expectedHistory.pulse,testRace.expectedPulse )
    ;
    //expect(finishDetailMap.getWinTime(0), 10);
  });

  // stupid async file activity is aborted w/o sleep!
  for( int seconds = 0; seconds < 3; seconds++ ) {
    test('Waiting 1 second...', () {
      sleep(const Duration(seconds:1));
    } );
  }

}

class ExpectedHistory implements PropagateStatusCallbacks {
  final TestRace testRace;
  ExpectedHistory(this.testRace);
  @override
  void onFinish(FinishDetailMap finishDetailMap) {
    //print("mqCallback: history: $topic ");
    print("mqCallback: finishDetailMapJson ${finishDetailMap.toJson()}");
    if (testRace.expectedFinishResultList != null) {
      assert(testRace.expectedFinishResultList.isNotEmpty,
          "Expected Race received more results than configured for.");
      ExpectedFinishResult expectedFR =
          testRace.expectedFinishResultList.removeAt(0);
      List<FinishResult> actualResults = finishDetailMap.getFinishResults();
      expect(actualResults[0].pinNumber, expectedFR.pinNumber);
      expect(actualResults[0].validDerbyRace, expectedFR.validDerbyCars);
      expect(actualResults[0].finishDetail.laneBlockedTime(),
          expectedFR.carLengthMicros);
      expect(
          actualResults[0].victoryMarginMicros, expectedFR.victoryMarginMicros);
      expect(finishDetailMap.getValidFinishLaneCount(), 2);
      print("mqCallback: Completed ExpectedFinish:  ${jsonEncode(expectedFR)}");

    }
  }

  int pulse=0;
  @override
  void onHeartbeat(List<String> errorList) {
    pulse++;
    print ("onHeartbeat: $pulse errors: $errorList");
  }

  @override
  void onPinChange(PinChange pinChange) {
    // noop
  }

}

class TestRace {
  List<PinChange> inputPinChangeList = [];

  List<ExpectedFinishResult> expectedFinishResultList = [];
  int expectedPulse=0;
}

TestRace generateClock(TestRace testRace) {
  testRace.inputPinChangeList.sort((a, b) => a.micros.compareTo(b.micros));
  //if (false )return;
  int loMicro = testRace.inputPinChangeList.first.micros;
  int hiMicro = testRace.inputPinChangeList.last.micros;
  hiMicro+=(10*1000*1000);  // flush the clock and finish with stable lanes.
  bool ps = false;
  for (int x = loMicro; x < hiMicro; x = x + 1000000) {
    PinChange pc = new PinChange(micros: x, pinState: ps, pinNumber: "clock");
    if(ps){
      testRace.expectedPulse++;
    }
    ps = !ps;
    testRace.inputPinChangeList.add(pc);

  }
  testRace.inputPinChangeList.sort((a, b) => a.micros.compareTo(b.micros));
}

TestRace g1(TestRace testRace) {
  List<Map<String, String>> raw = [];
  String base20 = (20 * CompactMap.million).toString();
  raw.add({"base": base20, "pin": "23", "state": "1", "times": "60:351500"});
  raw.add({"base": base20, "pin": "24", "state": "1", "times": "50:350102"});

  buildTestRaceFromCompactMapList(raw, testRace);

  testRace.expectedFinishResultList.add(new ExpectedFinishResult(
      pinNumber: "24", victoryMarginMicros: 10, carLengthMicros: 350102, validDerbyCars: true));
  return testRace;
}

TestRace g2(TestRace testRace) {
  List<Map<String, String>> raw = [];
  String base50 = (50 *CompactMap.million).toString();
  raw.add(
      {"base": base50, "pin": "23", "state": "1", "times": "606:1500:10:100"});
  raw.add(
      {"base": base50, "pin": "24", "state": "1", "times": "500:2520:10:1"});

  buildTestRaceFromCompactMapList(raw, testRace);

  testRace.expectedFinishResultList.add(new ExpectedFinishResult(
      pinNumber: "24", victoryMarginMicros: 106, carLengthMicros: 2520));
  testRace.expectedFinishResultList.add(new ExpectedFinishResult(
      pinNumber: "24", victoryMarginMicros: 106, carLengthMicros: 2531));
  return testRace;
}

void buildTestRaceFromCompactMapList(
    final List<Map<String, String>> raw, TestRace testRace) {
  final CompactMap compactMap=new CompactMap();

  List<PinChange> pcList = compactMap.buildPinChangeListFromCompactMapList(raw);

  for (PinChange pc in pcList) {
    print(pc.toJsonString() + "\n");
    testRace.inputPinChangeList.add(pc);
  }
}



class ExpectedFinishResult {
  int victoryMarginMicros;
  String pinNumber;
  int carLengthMicros;
  final bool validDerbyCars;
  ExpectedFinishResult(
      {this.pinNumber, this.carLengthMicros, this.victoryMarginMicros, this.validDerbyCars=false});
  Map<String,Object> toJson(){
    return {"victoryMarginMicros":victoryMarginMicros, "pinNumber":pinNumber,"carLengthMicros":carLengthMicros};
  }
}
