import 'package:flutter0322/shared/StaticShared.dart';
import 'package:flutter0322/shared/TImerConfig.dart';

class TestFixtures {
  static void testStaticModel() {
    StaticShared.getShared().timerConfig = new TimerConfig(
        maxTime: new Duration(milliseconds: 500), //
        minTime: new Duration(milliseconds: 300), //
        maxPerforationCount: 1, //
        maxPerforationTime: new Duration(milliseconds: 15));
  }
}
