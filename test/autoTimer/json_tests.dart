import 'dart:convert';

import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:test/test.dart';
//class MockHistory extends Mock implements HistoryBuffer {}

void main() {
  // Mock class

// mock creation
  test('json PinChange', () {
    PinChange pc = new PinChange(
        micros: 2000, pinState: true, pinNumber: "555");
    String jstring=json.encode(pc.toJson());


    print("json string: $jstring");
    PinChange pc2=PinChange.fromJson(json.decode(jstring));

    expect(pc.toJson(),pc2.toJson());
    //expect(pc.toJson(), "{}");

    //expect(cut.toJson(), '{"foo":{"pinName":"77","noseTime":null,"tailTime":null,"elapsed":null}}');
    //expect(cut.toJson(), '{"foo":{"pinNumber":"77","carSegmentList":[]}}');
  });
}