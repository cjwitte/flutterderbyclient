import 'dart:convert';

import 'package:flutter0322/autoTimer/PinChange.dart';
import 'package:flutter0322/autoTimer/models/data_model_plain.dart';
import 'package:test/test.dart';

void main() {
  test('Pc python json superflous data', () {
    String jsonString = '{"pinNumber": "25", "seq": 17, "pinState": 1, "micros": 4212730917, "pinName": "fakeHz", "pubTime": 1555893960999, "pinType": "clock"}';
    var rpc = PinChange.fromJson(json.decode(jsonString));
    print("rpc:" + json.encode(rpc.toJson()));

    expect(rpc.pinState, equals(true));
    expect(rpc.pinNumber, equals("25"));
    expect(rpc.micros, equals(4212730917));
  });
  test('Pc python json true', () {
    String jsonString = '{"micros":5,"pinNumber":"22","pinState":1}';
    var rpc = PinChange.fromJson(json.decode(jsonString));
    print("rpc:" + json.encode(rpc.toJson()));

    expect(rpc.pinState, equals(true));
  });
  test('Pc python json false', () {
    String jsonString = '{"micros":5,"pinNumber":"22","pinState":0}';
    var rpc = PinChange.fromJson(json.decode(jsonString));
    print("rpc:" + json.encode(rpc.toJson()));

    expect(rpc.pinState, equals(false));
  });
  test('PinChange json', () {
    var pc = new PinChange(micros: 5, pinNumber: "22", pinState: true);
    String jsonS2 = json.encode(pc.toJson());
    print(jsonS2);

    print("as map");
    print(json.decode(jsonS2));
    var rpc = PinChange.fromJson(json.decode(jsonS2));

    //RacePhase x=rp.build();
    //var jsonS2=x.toJson();
    print("rpc:" + json.encode(rpc.toJson()));
    print(rpc);

    // print(RacePhase.fromJson(jsonS2));
  });
}
