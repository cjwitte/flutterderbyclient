import 'dart:async';
import 'dart:io';

import 'package:flutter0322/models.dart';
import 'package:flutter0322/network/GetS3Object.dart';
import 'package:flutter0322/shared/RaceConfig.dart';

/*
void main(List<String> arguments){
  print("iotest\n");
}
*/
String awsBucket="https://s3.amazonaws.com/chicagomay2018-s3derbyracedata-12xlchops4tcu";
String awsObject="/dataLog.ndjson";
void main()async{
  print("begin io_tests");
  /*
  await new HttpClient().getUrl(Uri.parse(testUrl))
      .then((HttpClientRequest request) => request.close())
      .then((HttpClientResponse response) =>
      response.pipe(new File('main0.txt').openWrite()));

  await main2();
  await main3();
  */
  await testDoRefreshRacePhase();
  await testDoRefreshRacer();
}
void main2 ()async{
  print("iotest\n");
  String  response=await GetS3Utils.getS3ObjectAsString(awsBucket+awsObject);
  print("iotest String length: "+response.length.toString());

}
void main3 ()async{
  print("iotest\n");
  File file=new File("iootest.0");
  File  fResponse=await new SyncS3DataLog().getS3ObjectAsFile(awsBucket+awsObject,file);

  FileStat fstat = await FileStat.stat(fResponse.path);

  print("iotest File length: "+fstat.size.toString());

}


Future<RefreshLog>  testDoRefreshRacePhase ()  async{
  RaceConfig raceConfig=new RaceConfig(applicationUrl:null,s3BucketUrlPrefix: awsBucket);
  RefreshLog r=await new RefreshData().doRefresh( raceConfig: raceConfig);
  return r;
}
Future<RefreshLog> testDoRefreshRacer ()async{
  RaceConfig raceConfig=new RaceConfig(applicationUrl:null,s3BucketUrlPrefix: awsBucket);
  RefreshLog r=await new RefreshData().doRefresh( raceConfig: raceConfig);
  return r;
}
