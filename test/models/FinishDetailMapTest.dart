import 'dart:convert';

import 'package:flutter0322/autoTimer/FinishDetail.dart';
import 'package:flutter0322/autoTimer/FinishResult.dart';
import "package:test/test.dart";

void main() {


  test("finishDetailMap JSON", () {
    //final String fm4='{"finishDetailMap":{"24":{"pinNumber":"24","carSegmentList":[{"noseTime":50000500,"tailTime":50003020},{"noseTime":50003030,"tailTime":50003031}]},"23":{"pinNumber":"23","carSegmentList":[{"noseTime":50000606,"tailTime":50002106},{"noseTime":50002116,"tailTime":50002216}]}},"historyBuffer":{"pinHistoryMap":{"24":{"pinNumber":"24"},"23":{"pinNumber":"23"},"clock":{"pinNumber":"clock"}}}}';
    final String fm5='{"finishDetailMap":{"24":{"pinNumber":"24","carSegmentList":[{"noseTime":50000500,"tailTime":50003020},{"noseTime":50003030,"tailTime":50003031}]},"23":{"pinNumber":"23","carSegmentList":[{"noseTime":50000606,"tailTime":50002106},{"noseTime":50002116,"tailTime":50002216}]}},"historyBuffer":{"pinHistoryMap":{"24":{"pinList":[{"micros":20350152,"pinNumber":"24","pinState":false},{"micros":50000500,"pinNumber":"24","pinState":true},{"micros":50003020,"pinNumber":"24","pinState":false},{"micros":50003030,"pinNumber":"24","pinState":true},{"micros":50003031,"pinNumber":"24","pinState":false}],"pinNumber":"24"},"23":{"pinList":[{"micros":20351560,"pinNumber":"23","pinState":false},{"micros":50000606,"pinNumber":"23","pinState":true},{"micros":50002106,"pinNumber":"23","pinState":false},{"micros":50002116,"pinNumber":"23","pinState":true},{"micros":50002216,"pinNumber":"23","pinState":false}],"pinNumber":"23"},"clock":{"pinList":[{"micros":45000001,"pinNumber":"clock","pinState":true},{"micros":46000001,"pinNumber":"clock","pinState":false},{"micros":47000001,"pinNumber":"clock","pinState":true},{"micros":48000001,"pinNumber":"clock","pinState":false},{"micros":49000001,"pinNumber":"clock","pinState":true},{"micros":50000001,"pinNumber":"clock","pinState":false}],"pinNumber":"clock"}}}}';
    var fdMap = json.decode(fm5);
    FinishDetailMap rc = new FinishDetailMap.fromJson(fdMap);
    print("asString: ${rc.toString()}");
    print("length: ${fm5.length.toString()}");
    print("Json2: ${rc.toJson2()}");

    List<FinishResult> fdr=rc.getFinishResults();

    if(fdr!=null) fdr.forEach((finishResult){
      print ("got fr: ${finishResult.toString}");
    });


  });
}