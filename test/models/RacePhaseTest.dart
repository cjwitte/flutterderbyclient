import 'dart:convert';

import 'package:flutter0322/models.dart';
import "package:test/test.dart";

void main() {


  test("racePhase Json", () {
    final String lane2Json='{"carNumber1": 104, "carNumber2": 103, "loadMS": 1535988131733, "resultMS": -100, "phaseNumber": 2, "lastUpdateMS": 1535988144600, "raceStandingID": 2, "id": 4, "version": 2}';
    var jsonMap = jsonDecode(lane2Json);
    RacePhase rc = new RacePhase.fromJsonMap(jsonMap);
    print("REL: ${rc.raceEntryList}");
    expect(rc.getCompatResultMs(),equals(-100));

  });
}